/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

//--------------------------------
/* cmp( i_str )
	case sensitive compare with another str.

	NOTE: same as == operator.
*/
D_INLINE
int
Str::cmp
(
	Str const& i_str
) const
{
	assert( i_str.size() > 0 );
	assert( m_size > 0 );
	assert( m_array != nullptr );
	return strncmp( m_array, i_str.c_str(), m_size );
}

//--------------------------------
/* cmp( i_cStr )
	case sensitive compare with a C string.

	NOTE: same as == operator.
*/
D_INLINE
int
Str::cmp
(
	char const* i_cStr
) const
{
	assert( i_cStr != nullptr );
	assert( m_size > 0 );
	assert( m_array != nullptr );
	return strncmp( m_array, i_cStr, m_size );
}

//--------------------------------
/* cmp( i_str, i_len )
	case sensitive compare with a Str that
	stops at i_len number of characters.
*/
D_INLINE
int
Str::cmpn
(
	Str const& i_str,
	size_t const i_len
) const
{
	assert( i_str.size() > 0 );
	assert( i_len > 0 && i_len <= m_size );
	assert( m_size > 0 );
	assert( m_array != nullptr );
	return strncmp( m_array, i_str.c_str(), i_len );
}

//--------------------------------
/* cmpn( i_cStr, i_len )
	case sensitive compare with a C String that
	stops at i_len number of characters.
*/
D_INLINE
int
Str::cmpn
(
	char const* i_cStr,
	size_t const i_len
) const
{
	assert( i_cStr != nullptr );
	assert( i_len > 0 && i_len <= m_size );
	assert( m_size > 0 );
	assert( m_array != nullptr );
	return strncmp( m_array, i_cStr, i_len );
}

//--------------------------------
/* icmp( i_str )
	case insensitive compare with another str.
*/
D_INLINE
int
Str::icmp
(
	Str const& i_str
) const
{
	assert( i_str.size() > 0 );
	assert( m_size > 0 );
	assert( m_array != nullptr );
	return strincmp( m_array, i_str.c_str(), m_size );
}

//--------------------------------
/* icmp( i_cStr )
	case insensitive compare with a C string.
*/
D_INLINE
int
Str::icmp
(
	char const* i_cStr
) const
{
	assert( i_cStr != nullptr );
	assert( m_size > 0 );
	assert( m_array != nullptr );
	return strincmp( m_array, i_cStr, m_size );
}

//--------------------------------
/* icmpn( i_str, i_len )
	case insensitive compare with a Str that
	stops at i_len number of characters.
*/
D_INLINE
int
Str::icmpn
(
	Str const& i_str,
	size_t const i_len
) const
{
	assert( i_str.size() > 0 );
	assert( i_len > 0 && i_len <= m_size );
	assert( m_size > 0 );
	assert( m_array != nullptr );
	return strincmp( m_array, i_str.c_str(), i_len );
}

//--------------------------------
/* icmpn( i_cStr, i_len )
	case sensitive compare with a C String that
	stops at i_len number of characters.
*/
D_INLINE
int
Str::icmpn
(
	char const* i_cStr,
	size_t const i_len
) const
{
	assert( i_cStr != nullptr );
	assert( i_len > 0 && i_len <= m_size );
	assert( m_size > 0 );
	assert( m_array != nullptr );
	return strincmp( m_array, i_cStr, i_len );
}