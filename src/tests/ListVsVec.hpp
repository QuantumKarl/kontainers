/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_LIST_VS_VEC_HPP )
#define D_LIST_VS_VEC_HPP

class ListVsVec
{
public:
	ListVsVec();

	static void RunListTests();
	static void RunVecTests();

	LoopCount const& GetLoopCount() const { return m_loopCount; }
private:

	// Kontainer tests
	void ListTest1( size_t const i_num );
	void ListTest2( size_t const i_num, size_t const i_mul );

	// STL tests
	void VecTest1( size_t const i_num );
	void VecTest2( size_t const i_num, size_t const i_mul );

	void ListRunA();
	void VecRunA();

	LoopCount m_loopCount;
};

#include "ListVsVec.inl"

#endif