/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_BINSEARCH_HPP )
#define D_BINSEARCH_HPP

#include "..\sys\sys_defines.hpp"
#include "..\sys\sys_includes.hpp"
#include "..\sys\sys_assert.hpp"
#include "..\sys\sys_types.hpp"
#include "..\sys\sys_global.hpp"

namespace Algorithm
{
	//----------------------------------------------------------------
	/* Binary Search templates

		The array elements have to be ordered in increasing order.
	*/

	//--------------------------------
	/* BinSearch_GreaterEqual( arrayIn, arrayInSize, value )
		Finds the last array element which is smaller than the given value.
	*/
	template< class type >
	D_INLINE
	size_t
	BinSearch_Less
	(
		type const*		i_array,
		size_t const	i_arraySize,
		type const&		i_value
	)
	{
		assert( i_array != nullptr && i_arraySize > 0 );
		assert( i_value >= i_array[0] && i_value <= i_array[i_arraySize-1] );

		size_t len		= i_arraySize;
		size_t mid		= len;
		size_t offset	= 0;
		while( mid > 0 )
		{
			mid = len >> 1;
			if ( i_array[offset+mid] < i_value )
			{
				offset += mid;
			}
			len -= mid;
		}
		return offset;
	}

	//--------------------------------
	/* BinSearch_GreaterEqual( arrayIn, arrayInSize, value )
		Finds the last array element which is smaller than or equal to the given value.
	*/
	template< class type >
	D_INLINE
	size_t
	BinSearch_LessEqual
	(
		type const*		i_array,
		size_t const	i_arraySize,
		type const&		i_value
	)
	{
		assert( i_array != nullptr && i_arraySize > 0 );
		assert( i_value >= i_array[0] && i_value <= i_array[i_arraySize-1] );

		size_t len		= i_arraySize;
		size_t mid		= len;
		size_t offset	= 0;
		while( mid > 0 )
		{
			mid = len >> 1;
			if ( i_array[offset+mid] <= i_value )
			{
				offset += mid;
			}
			len -= mid;
		}
		return offset;
	}

	//--------------------------------
	/* BinSearch_Greater( arrayIn, arrayInSize, value )
		Finds the first array element which is greater than the given value.
	*/
	template< class type >
	D_INLINE
	size_t
	BinSearch_Greater
	(
		type const*		i_array,
		size_t const	i_arraySize,
		type const&		i_value
	)
	{
		assert( i_array != nullptr && i_arraySize > 0 );
		assert( i_value >= i_array[0] && i_value <= i_array[i_arraySize-1] );

		size_t len		= i_arraySize;
		size_t mid		= len;
		size_t offset	= 0;
		size_t res		= 0;
		while( mid > 0 )
		{
			mid = len >> 1;
			if ( i_array[offset+mid] > i_value )
			{
				res = 0;
			}
			else
			{
				offset += mid;
				res = 1;
			}
			len -= mid;
		}
		return offset+res;
	}

	//--------------------------------
	/* BinSearch_GreaterEqual( arrayIn, arraInSize, value )
		Finds the first array element which is greater than or equal to the given value.
	*/
	template< class type >
	D_INLINE
	size_t
	BinSearch_GreaterEqual
	(
		type const*		i_array,
		size_t const	i_arraySize,
		type const&		i_value
	)
	{
		assert( i_array != nullptr && i_arraySize > 0 );
		assert( i_value >= i_array[0] && i_value <= i_array[i_arraySize-1] );

		size_t len		= i_arraySize;
		size_t mid		= len;
		size_t offset	= 0;
		size_t res		= 0;
		while( mid > 0 )
		{
			mid = len >> 1;
			if ( i_array[offset+mid] >= i_value )
			{
				res = 0;
			}
			else
			{
				offset += mid;
				res = 1;
			}
			len -= mid;
		}
		return offset+res;
	}

}// end of namespace
#endif // end of D_BINSEARCH_HPP