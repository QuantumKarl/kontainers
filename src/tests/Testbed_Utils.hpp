/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_TESTBED_HPP )
#define D_TESTED_HPP

#include <time.h>
using namespace std;

namespace TestUtils
{

#if defined(D_PC_WIN)
#include <intrin.h>
#pragma intrinsic(__rdtsc)

//--------------------------------
// cpu clock based timer
uint64 startTimeA(0), endTimeA(0);
void startTimerA()
{
	startTimeA = static_cast<uint64>(__rdtsc());
}

uint64 endTimerA()
{
	endTimeA = static_cast<uint64>(__rdtsc());
	return endTimeA - startTimeA;
}

//--------------------------------
// windows based timer
DWORD startTimeB;
void startTimerB()
{
	startTimeB = timeGetTime();
}
DWORD endTimerB()
{
	return timeGetTime() - startTimeB;
}
#elif defined( D_GCC )
// TODO: linux version
http://stackoverflow.com/questions/9887839/clock-cycle-count-wth-gcc
#endif

//--------------------------------

//----------------------------------------------------------------
/* write info
	writes out info about the program to the results file
*/
void
writeInfo()
{
	Str output;
#if defined(D_KARL_TEST)
	output.push_backf( "Starting Test %s %s", D_CPUSTRING, "Kontainers" );
#else
	output.push_backf( "Starting Test %s %s", D_CPUSTRING, "STL" );
#endif

#if defined( D_ASMLIB_HPP )
	output.push_backf("\nInstructionSet = %i", InstructionSet());

	// cpuid_abcd
	int abcd[4];
	char s[16];
	cpuid_abcd(abcd, 0);
	*(int*)(s+0) = abcd[1];	//-V112 // ebx
	*(int*)(s+4) = abcd[3];	//-V112 // edx
	*(int*)(s+8) = abcd[2];	//-V112 // ecx
	s[12] = 0;				// terminate string
	output.push_backf("\nVendor string = %s", s); //-V111

	// ProcessorName()
	output.push_backf("\nProcessorName = %s", ProcessorName()); //-V111

	// CpuType
	int vendor, family, model;
	CpuType(&vendor, &family, &model);
	output.push_backf("\nCpuType: vendor %i, family 0x%X, model 0x%X", vendor, family, model);

	// DataCacheSize
	output.push_backf("\nData cache size: L1 %ikb, L2 %ikb, L3 %ikb",
		(int)DataCacheSize(1)/1024,	//L1
		(int)DataCacheSize(2)/1024,	//L2
		(int)DataCacheSize(3)/1024);//L3

	// ReadTSC time
	ReadTSC();
	int tsc = (int)ReadTSC();
	tsc = (int)ReadTSC() - tsc;
	output.push_backf( "\nReadTSC takes %i clocks", tsc);

#if defined( D_PC_WIN )
	// read win timer
	startTimerB();
	output.push_backf( "\nRead win timer takes %i ms", static_cast<int>( endTimerB() ) );
#else
	compile_time_assert( false );
#endif
#endif

	time_t currentTime;
	time( &currentTime );
#if defined( D_PC_WIN )
	{
		char buf[1024];
		memset(buf,0,1024);
		ctime_s(buf,1024,&currentTime);
		output.push_backf( "\nStart Time %s\n\n", buf ); //-V111
	}
#elif defined( D_GCC )
	compile_time_assert( false );
#endif

	// output to screen
	cout << output << endl;

#if defined( D_WRITE_TO_FILE )
	ofstream results("results.txt", fstream::out | fstream::app);
	if(results.is_open() )
	{
		results << "CPU info:\n" << output << endl;
		results.close();
	}
#endif
}

//----------------------------------------------------------------
/* testCode
*/
template<typename Func>
void
testCode
(
	const string& testTitle,
	Func testMethod,
	uint64 numOfRuns,
	uint64 numOfCycles
)
{
	// check valid in input
	if(numOfRuns > 0 && numOfCycles > 0 )
	{
		size_t const displayPowNumber = 9;
		stack<uint64>timeAresults;
		stack<uint64>timeBresults;

		cout << "\nStarted Test: " + testTitle  << endl;
		for(uint64 c = numOfCycles; c != 0; --c)
		{
			startTimerA();
			startTimerB();

			for(uint64 i = numOfRuns; i != 0; --i)
			{
				testMethod();
			}

			timeAresults.push( static_cast<uint64>(endTimerA()) );
			timeBresults.push( static_cast<uint64>(endTimerB()) );

			cout << "Finished Cycle: " << c << " with:\n"
				<< "CPU ticks: " << (double)timeAresults.top() / pow(10.0, static_cast<int>(displayPowNumber) )
				<< " x 10^" << displayPowNumber << "\n"
				<< "Milliseconds: " << timeBresults.top() << "\n" << endl;
		}

		// calculate stats
		uint64 averageA(0);
		uint64 averageB(0);

		assert( timeAresults.size() == timeBresults.size() );
		while( timeAresults.empty() == false )
		{
			// double precision devision, then round back into a uint64
			averageA += static_cast<uint64>( static_cast<double>(timeAresults.top()) / static_cast<double>(numOfCycles) + 0.5 );
			averageB += static_cast<uint64>( static_cast<double>(timeBresults.top()) / static_cast<double>(numOfCycles) + 0.5 );

			timeAresults.pop();
			timeBresults.pop();
		}

		cout << testTitle + " Results:\n"<< "Average CPU ticks: " << averageA / pow(10.0, static_cast<int>(displayPowNumber) )
			<< " x 10^" << displayPowNumber << "\n" << "Average seconds: " << (double)averageB / 1000.0 << endl;

#if defined( D_WRITE_TO_FILE )
		ofstream results("results.txt", fstream::out | fstream::app);
		if(results.is_open())
		{
			results << testTitle + " Results:\n"<< "Average CPU ticks: " << averageA / pow(10.0, static_cast<int>(displayPowNumber) )
				<< " x 10^" << displayPowNumber << "\n" << "Average seconds: " << (double)averageB / 1000.0 << "\n" <<  endl;
			results.flush();
			results.close();
		}
#endif
	}// end of if valid input
	else
	{
		cout << "error! number of runs is negative; its probably wrapped around" << endl;

#if defined( D_WRITE_TO_FILE )
		ofstream results("results.txt", fstream::out | fstream::app);
		if(results.is_open() )
		{
			results << "test: " << testTitle << "Failed to run" << "\n";
			results.close();
		}
#endif
	}
}

//----------------------------------------------------------------
void
writeEndtime
(
)
{
	Str output;
	time_t currentTime;
	time( &currentTime );
#if defined( D_PC_WIN )
	{
		char buf[1024];
		memset(buf,0,1024);
		ctime_s(buf,1024,&currentTime);
		output.push_backf( "\nEnd Time %s\n", buf ); //-V111
	}
#elif defined( D_GCC )
	compile_time_assert( false );
#endif

	// output to screen
	cout << output << endl;

#if defined( D_WRITE_TO_FILE )
	ofstream results("results.txt", fstream::out | fstream::app);
	if(results.is_open() )
	{
		results << output << endl;
		results.close();
	}
#endif
}

//--------------------------------------------------------------------------------------------------------------------------------
}
#endif