/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_PRIMES_HPP )
#define D_PRIMES_HPP
#include "..\containers\List.hpp"

namespace Primes
{
	// Initalises the static internal list of primes
	void InitStatic( const size_t primesLessThan = 1 << 16 ); //65k
	void FiniStatic();

	// Use this to estimate the number of primes that are under value ( often used for reserving space with vectors )
	size_t EstimateNumberOfPrimesLessThan( const size_t value );

	// Finds the next prime that is greater than or equal to value
	size_t findGE( const size_t value );

	// Finds the next prime that is less than or equal to value
	size_t findLE( const size_t value );

	size_t PrimeAt( const size_t index );
	size_t MaxPrime();
	size_t NumPrimes();

	// Generates primes less that or equal to limit and outputs them into outputPrimes
	template<typename T>
	void segmented_sieve( const size_t limit, List<T>& outputPrimes );
}

#endif // end of D_PRIMES_HPP