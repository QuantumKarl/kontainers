/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_STR_VS_STRING )
#define D_STR_VS_STRING

class StrVSstring
{
public:
	StrVSstring();

	LoopCount const& GetLoopCount() const { return m_loopCount; }

	static void RunStrTests();
	static void RunstringTests();

private:

	void StrTest1( size_t const i_size );
	void StrTest2( size_t const i_size );
	void StrTest3( size_t const i_size );

	void stringTest1( size_t const i_size );
	void stringTest2( size_t const i_size );
	void stringTest3( size_t const i_size );

	void StrRunA();
	void StrRunB();

	void stringRunA();
	void stringRunB();

	LoopCount m_loopCount;
};

#include "StrVSstring.inl"
#endif