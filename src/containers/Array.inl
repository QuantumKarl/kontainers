/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

//--------------------------------
/* Array()
	default ctor, minimal cost
*/
template< typename _type_, size_t m_num >
Array<_type_,m_num>::Array
(
)
{
}

//--------------------------------
/* Array( elements )
	ctor, creates the list of size newListSize and sets their value to elementsValue
*/
template< typename _type_, size_t m_num >
Array<_type_,m_num>::Array
(
	_type_ const& i_elementsValue
)
{
	if( i_elementsValue == nullptr )
	{
		memset( &m_list[0], 0, m_num * sizeof( _type_ ) );
	}
	else
	{
		// Set values
		for( size_t i = 0; i < m_num; ++i )
		{
			m_list[i] = i_elementsValue;
		}
	}
}

//--------------------------------
/* Array( start, end )
	ctor, iterate style init. copies elements from start to end into the list
*/
template< typename _type_, size_t m_num >
Array<_type_,m_num>::Array
(
	_type_ const* i_start,
	_type_ const* i_end
)
{
	assert( i_start != nullptr	&&
			i_end != nullptr );

	size_t const count = ptr_range( i_start, i_end );

	assert( count <= m_num );

	// Add each element to our list
	for(size_t i = 0; i < count; ++i )
	{
		m_list[i] = i_start[i];
	}
}

//--------------------------------
/* Array( other )
	ctor, copy constructor
*/
template< typename _type_, size_t m_num >
D_FORCE_INLINE
Array<_type_,m_num>::Array
(
	Array const& other
)
{
	*this = other;
}

//--------------------------------
/* ~Array
	dtor, clean up
*/
template< typename _type_, size_t m_num >
D_FORCE_INLINE
Array<_type_,m_num>::~Array
(
)
{
}

//--------------------------------
/* back
	return a const reference the to element at the back of the list,
	this is the most recently push back element or the last iteration in a loop
*/
template< typename _type_, size_t m_num >
D_FORCE_INLINE
_type_ const&
Array<_type_,m_num>::back
(
) const
{
	return m_list[m_num-1];
}

//--------------------------------
/* begin
	Returns a pointer to the beginning of the array.
	It is the opposite to end()
	It is useful for iterating over a list;
*/
template< typename _type_, size_t m_num >
D_FORCE_INLINE
_type_*
Array<_type_,m_num>::begin
(
)
{
	return &m_list[0];
}

//--------------------------------
/* begin
	Returns a const pointer to the beginning of the array.  Useful for iterating through the m_list in loops.
	It is the opposite to end()
*/
template< typename _type_, size_t m_num >
D_FORCE_INLINE
_type_ const*
Array<_type_,m_num>::begin
(
) const
{
	return &m_list[0];
}

//--------------------------------
/* contains
	Just like C#'s contain function on the List class,
	returns true if i_obj element is in the list.

	NOTE: It is SLOW at O( n ) where n is the size().
	if you use this a lot, you might want to consider a HashTable
*/
template< typename _type_, size_t m_num >
bool
Array<_type_,m_num>::contains
(
	_type_ const& i_obj
) const
{
	for( size_t i = 0; i < m_num; ++i )
	{
		if ( m_list[ i ] == i_obj )
		{
			return true;
		}
	}

	// Not found
	return false;
}

//--------------------------------
/* delete_contents
	Calls the destructor (delete) of each (pointer) element in the list.
	Then sets all the pointers to nullptr
	
	CAUTION:
	NOTE: this only works on lists containing pointers to objects.
	Since the m_list was not responsible for allocating the object, it has
	no information on whether the object still exists or not, so care must be taken to ensure that
	the pointers are still valid when this function is called.
	In addition, if the elements were allocated with new[], it will cause a memory leak.
*/
template< typename _type_, size_t m_num >
void
Array<_type_,m_num>::delete_contents
(
	bool const i_andSetToNULL
)
{
	for(size_t i = 0; i < m_num; ++i )
	{
		if( m_list[i] != nullptr ) //-V809
		{
			delete m_list[i];
		}
	}

	if( i_andSetToNULL == true )
	{
		memset( &m_list[0], 0, m_num * sizeof( _type_ ) );
	}
}

//--------------------------------
/* end
	returns a pointer to list[m_num].
	Dereferencing is undefined behavior.
	It is the opposite to begin().
*/
template< typename _type_, size_t m_num >
D_FORCE_INLINE
_type_ const*
Array<_type_,m_num>::end
(
) const
{
	return &m_list[m_num];
}

//--------------------------------
/* find
	Searches for the specified data in the list and returns it's address.
	Returns nullptr if the data is not found.

	NOTE: SLOW if you use this alot consider using a hashTable
*/
template< typename _type_, size_t m_num >
D_FORCE_INLINE
_type_*
Array<_type_,m_num>::find
(
	_type_ const& i_obj
) const
{
	size_t i = 0;
	if ( find_index( i_obj, i ) == true )
	{
		return &m_list[ i ];
	}

	return nullptr;
}

//--------------------------------
/* find_index( obj, index )
	Searches for the specified data in the m_list and returns it's index.
	Returns false if the data is not found.

	NOTE: SLOW at O( n )
*/
template< typename _type_, size_t m_num >
bool
Array<_type_,m_num>::find_index
(
	_type_ const& i_obj,
	size_t& o_index
) const
{
	for( size_t i = 0; i < m_num; ++i )
	{
		if ( m_list[ i ] == i_obj )
		{
			o_index = i;
			return true;
		}
	}

	// Not found
	o_index = 0;
	return false;
}

//--------------------------------
/* find_null
	Finds the first nullptr in the list.
	Returns true if a nullptr was found.

	NOTE: SLOW at O( n )
*/
template< typename _type_, size_t m_num >
bool
Array<_type_,m_num>::find_null
(
	size_t& i_index
) const
{
	for( size_t i = 0; i < m_num; ++i )
	{
		if ( m_list[ i ] == nullptr )
		{
			i_index = i;
			return true;
		}
	}

	// Not found
	i_index = 0;
	return false;
}

//--------------------------------
/* front
	Returns a const reference to the first element.
	Opposite to back.

	NOTE: any operation on front() such as erase is the SLOWEST possible operation on a list
*/
template< typename _type_, size_t m_num >
D_FORCE_INLINE
_type_ const&
Array<_type_,m_num>::front() const
{
	return m_list[0];
}

//--------------------------------
/* index_of
	Takes a pointer to an element in the m_list and returns the index of the element.
	This is NOT a guarantee that the object is really in the list.
	Function will assert in debug builds if pointer is outside the bounds of the list,
	but remains silent in release builds.
*/
template< typename _type_, size_t m_num >
D_FORCE_INLINE
size_t
Array<_type_,m_num>::index_of
(
	_type_ const* i_ptr
) const
{
	size_t const index = ptr_range(i_ptr, static_cast<_type_ const*>(m_list) );

	assert( index < m_num );
	return index;
}

//--------------------------------
/* memory_set
	Calls memset to set all the values to 0
*/
template< typename _type_, size_t m_num >
D_FORCE_INLINE
void
Array<_type_,m_num>::memory_set
(
)
{
	memset( &m_list[0], 0, m_num * sizeof( _type_ ) );
}

//--------------------------------
/* memory_set( value )
	Calls memset on all whole array
	with value as the value
*/
template< typename _type_, size_t m_num >
D_FORCE_INLINE
void
Array<_type_,m_num>::memory_set
(
	const size_t i_value
)
{
	memset( &m_list[0], static_cast<int>(i_value), m_num * sizeof( _type_ ) );
}

//--------------------------------
/* size
	Returns the number of elements currently contained in the list.

	NOTE: that this is NOT an indication of the memory allocated.
*/
template< typename _type_, size_t m_num >
D_FORCE_INLINE
size_t
Array<_type_,m_num>::size
(
) const
{
	return m_num;
}

//--------------------------------
/* shuffle( ran )
	Shuffles the list. (opposite to sort)
	Pass in the RNG you want the function to draw its random
	numbers from.

	NOTE: SLOW

	NOTE: When the container is not on the main thread,
	a reference to an instance of RNG should be given to it.
*/
template< typename _type_, size_t m_num >
void
Array<_type_,m_num>::shuffle
(
	Random::RNG& ran // = Random::Generator default is global static generator
)
{
	Algorithm::shuffle( m_list, m_num, ran );
}

//--------------------------------
/* sort( templateSort = default )
	Performs a sort on the list using the supplied sort algorithm.
	QuickSort is the default.

	NOTE: The data is moved around the list, so any pointers to data within the list may
	no longer be valid.
*/
template< typename _type_, size_t m_num >
void
Array<_type_,m_num>::sort
(
	Algorithm::SortBase<_type_> const& sort // = idSort_QuickDefault<_type_>()
)
{
	assert( m_list != nullptr );
	sort.Sort( m_list, m_num );
}

//--------------------------------
/* operator =
	Copies one Array to another (deep copy)

	NOTE: SLOW avoid if you can
*/
template< typename _type_, size_t m_num >
Array<_type_,m_num>&
Array<_type_,m_num>::operator=
(
	Array<_type_,m_num> const& other
)
{
	// will not compile if the lists are different sizes

	for( size_t i = 0; i < m_num; ++i )
	{
		m_list[ i ] = other.m_list[ i ];
	}

	return *this;
}

//--------------------------------
/* _memory_used
	Returns size of the used elements in the list.

	ie element size * num used elements
*/
template< typename _type_, size_t m_num >
size_t
Array<_type_,m_num>::_memory_used
(
) const
{
	return m_num * sizeof( _type_ );
}

//--------------------------------
/*	operator[]
	const version.
	Performs bounds checking in debug build.
*/
template< typename _type_, size_t m_num >
D_FORCE_INLINE
_type_ const&
Array<_type_,m_num>::operator[]
(
	size_t i_index
) const
{
	assert( i_index < m_num );

	return m_list[ i_index ];
}

//--------------------------------
/*	operator[]
	None cost version.
	Performs bounds checking in debug build.
*/
template< typename _type_, size_t m_num >
D_FORCE_INLINE
_type_&
Array<_type_,m_num>::operator[]
(
	size_t i_index
)
{
	assert( i_index < m_num );

	return m_list[ i_index ];
}