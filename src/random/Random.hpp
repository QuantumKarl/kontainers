/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_RANDOM_HPP )
#define D_RANDOM_HPP

#include "..\sys\sys_defines.hpp"
#include "..\sys\sys_includes.hpp"
#include "..\sys\sys_assert.hpp"
#include "..\sys\sys_types.hpp"
#include "..\sys\sys_global.hpp"

namespace Random
{
	#include "RNG.hpp"

	// Note: Not thread safe, create one RNG per thread if
	// you need to multi-threaded random number generation
	RNG& Generator();
	
	void Test();
}

#endif // end of D_RANDOM_HPP