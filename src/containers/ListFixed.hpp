/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_LIST_FIXED_HPP )
#define D_LIST_FIXED_HPP

#include "algorithm\Sort.hpp"
#include "random\Random.hpp"

//----------------------------------------------------------------
/* ListFixed
	Very similar to List but can not resize itself.

	Iterating is fast since it is via index / pointer math.
	Adding is fast, as long as no order is needed.
	Removing is slow.
	Searching is slow unless binary search(on a sorted list) is used (see Sort.h)

	NOTE: Different to Array class. This class behaves just like List but can not reallocate.
	i.e it keeps a size which is different to the capacity.
*/
template< typename type_t, size_t m_capacity >
class ListFixed
{
public:
	ListFixed();
	ListFixed( size_t const i_newListSize, type_t const& i_elementsValue );
	ListFixed( type_t const* i_start, type_t const* i_end );
	ListFixed( ListFixed const& i_other );

	~ListFixed();

	// B
	type_t const&	back() const;										// returns a reference to the back
	type_t*			begin();											// returns a pointer to the first element
	type_t const*	begin() const;										// returns a pointer to the first element

	// C
	size_t			capacity() const;									// returns number of elements allocated for
	void			clear();											// clear the list
	bool			contains( type_t const& i_obj ) const;				// find the given element _SLOW_

	// D
	void			delete_contents( bool const i_andSetToNULL );		// delete the contents pointed to by the entries in the list

	// E
	type_t&			emplace_back();										// returns reference to a new data element at the end of the list
	bool			empty() const;										// return true if the list is empty
	type_t const*	end() const;										// returns a pointer to the back of the list
	bool			erase( size_t const i_index );						// remove the element at the given index, and move every element after it down one
	bool			erase( type_t const& i_obj );						// remove the element
	bool			erase_fast( size_t const i_index );					// removes the element at the given index and places the last element into its spot - DOES NOT PRESERVE LIST ORDER

	// F
	type_t const*	find( type_t const& i_obj ) const;							// find pointer to the given element _SLOW_
	bool			find_index( const type_t& i_obj, size_t& o_index ) const;	// find the index for the given element _SLOW_
	bool			find_null( size_t& o_index ) const;							// find the index for the first NULL pointer in the list _SLOW_
	type_t const&	front() const;

	// I
	bool			insert( type_t const& i_obj, size_t const i_index );	// insert the element at the given index _SLOW_
	size_t			index_of( type_t const* i_obj ) const;					// returns the index for the pointer to an element in the list _RISKY_

	// P
	void			pop_back();												// removes the last element from the container
	void			pop_back( size_t const i_numPops );						// removes the n last elements from the container
	void			push_back( type_t const& i_obj );						// append element
	void			push_back( ListFixed const& i_other );					// append list
	void			push_back( type_t const* i_start, type_t const* i_end );// append range

	// S
	void			shuffle( Random::RNG& i_ran );						// apply shuffle to list
	size_t			size() const;										// returns number of elements in list
	void			sort( const Algorithm::SortBase<type_t>& i_sort = Algorithm::Sort_QuickDefault<type_t>() ); // sort the list

	// Misc functions
	size_t			_allocated() const;									// returns total size of allocated memory
	size_t			_size() const;										// returns total size of allocated memory including size of list type_t
	size_t			_memory_used() const;								// returns size of the used elements in the list

	ListFixed<type_t,m_capacity>&		operator=( ListFixed<type_t,m_capacity> const& other ); // _SLOW_
	type_t const&						operator[]( size_t index ) const;
	type_t&								operator[]( size_t index );

private:
	type_t		m_array[m_capacity];
	size_t		m_size;
};

#include "ListFixed.inl"

#endif // D_LISTFIXED_HPP