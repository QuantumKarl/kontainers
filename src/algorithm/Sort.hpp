/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>.

	INFO: This is a modified version of Doom3's sort code see
	https://github.com/id-Software/DOOM-3-BFG/blob/master/neo/idlib/containers/Sort.h
*/

#if !defined( D_SORT_HPP )
#define D_SORT_HPP

//--------------------------------------------------------------------------------------------------------------------------------
/* Doom3 Extract on Sort:
Contains the generic templated sort algorithms for quick-sort, heap-sort and insertion-sort.

The sort algorithms do not use class operators or overloaded functions to compare
objects because it is often desirable to sort the same objects in different ways
based on different keys (not just ascending and descending but sometimes based on
name and other times based on say priority). So instead, for each different sort a
separate class is implemented with a Compare() function.

This class is derived from one of the classes that implements a sort algorithm.
The Compare() member function does not only define how objects are sorted, the class
can also store additional data that can be used by the Compare() function. This, for
instance, allows a list of indices to be sorted where the indices point to objects
in an array. The base pointer of the array with objects can be stored on the class
that implements the Compare() function such that the Compare() function can use keys
that are stored on the objects.

The Compare() function is not virtual because this would incur significant overhead.
Do NOT make the Compare() function virtual on the derived class!
The sort implementations also explicitly call the Compare() function of the derived
class. This is to avoid various compiler bugs with using overloaded compare functions
and the inability of various compilers to find the right overloaded compare function.

To sort an array, an List or an StaticList, a new sort class, typically derived from
Sort_Quick, is implemented as follows:

class Sort_MySort : public Sort_Quick< MyObject, Sort_MySort >
{
public:
	int
	Compare
	(
		const MyObject & a,
		const MyObject & b
	) const
	{
		if ( a should come before b )
		{
			return -1; // or any negative integer
		}
		if ( a should come after b )
		{
			return 1;  // or any positive integer
		}
		else // equal
		{
			return 0;
		}
	}
};

To sort an array:

MyObject array[100];
Sort_MySort().Sort( array, 100 );

To sort an List:

List< MyObject > list;
list.Sort( Sort_MySort() );

The sort implementations never create temporaries of the template type. Only the
'SwapValues' template is used to move data around. This 'SwapValues' template can be
specialized to implement fast swapping of data. For instance, when sorting a list with
objects of some string class it is important to implement a specialized 'SwapValues' for
this string class to avoid excessive re-allocation and copying of strings.

*/

namespace Algorithm
{
	//----------------------------------------------------------------
	/* SortBase
		Sort is an abstract template class for sorting an array of objects of the specified data type.
		The array of objects is sorted such that: Compare( array[i], array[i+1] ) <= 0 for all i
	*/
	template< typename _type_ >
	class SortBase
	{
	public:
		virtual			~SortBase() {}
		virtual void	Sort( _type_* base, size_t num ) const = 0;
	};

	//----------------------------------------------------------------
	/* Sort_Quick
		Sort_Quick is a sort template that implements the
		quick-sort algorithm on an array of objects of the specified data type.
	*/
	template< typename _type_, typename _derived_ >
	class Sort_Quick : public SortBase< _type_ >
	{
	public:
		virtual void
		Sort
		(
			_type_* i_base,
			size_t i_num
		) const
		{
			if( i_num > 0 )
			{
				const int64 MAX_LEVELS = 128;
				int64 lo[MAX_LEVELS], hi[MAX_LEVELS];
				const _derived_* der = static_cast< const _derived_* >( this );

				// 'lo' is the lower index, 'hi' is the upper index
				// of the region of the array that is being sorted.
				lo[0] = 0;
				hi[0] = i_num - 1;

				for ( int64 level = 0; level >= 0; )
				{
					int64 i = lo[level];
					int64 j = hi[level];

					// Only use quick-sort when there are 4 or more elements in this region and we are below MAX_LEVELS.
					// Otherwise fall back to an insertion-sort.
					if ( ( j - i ) >= 4		&& //-V112
						level < ( MAX_LEVELS - 1 ) )
					{
						// Use the center element as the pivot.
						// The median of a multi point sample could be used
						// but simply taking the center works quite well.
						int64 pi = ( i + j ) / 2;

						// Move the pivot element to the end of the region.
						SwapValues( i_base[j], i_base[pi] );

						// Get a reference to the pivot element.
						_type_& pivot = i_base[j--];

						// Partition the region.
						do
						{
							while( der->Compare( i_base[i], pivot ) < 0 )
							{
								if ( ++i >= j ) break;
							}
							while( der->Compare( i_base[j], pivot ) > 0 )
							{
								if ( --j <= i ) break;
							}
							if ( i >= j ) break;
							SwapValues( i_base[i], i_base[j] );
						}
						while( ++i < --j );

						// Without these iterations sorting of arrays with many duplicates may
						// become really slow because the partitioning can be very unbalanced.
						// However, these iterations are unnecessary if all elements are unique.
						while ( der->Compare( i_base[i], pivot ) <= 0 && i < hi[level] )
						{
							++i;
						}
						while ( der->Compare( i_base[j], pivot ) >= 0 && lo[level] < j )
						{
							--j;
						}

						// Move the pivot element in place.
						SwapValues( pivot, i_base[i] );

						assert( level < MAX_LEVELS - 1 );
						lo[level+1] = i;
						hi[level+1] = hi[level];
						hi[level] = j;
						++level;

					}
					else // Insertion-sort of the remaining elements.
					{
						for( ; i < j; --j )
						{
							int64 m = i;
							for ( int64 k = i + 1; k <= j; ++k )
							{
								if ( der->Compare( i_base[k], i_base[m] ) > 0 )
								{
									m = k;
								}
							}
							SwapValues( i_base[m], i_base[j] );
						}
						--level;
					}
				}
			}
			else// i_num is 0
			{
				return;
			}
		}
	};

	//----------------------------------------------------------------
	/* Sort_QuickDefault
		Default quick-sort comparison function that can
		be used to sort scalars from small to large.
	*/
	template< typename _type_ >
	class Sort_QuickDefault : public Sort_Quick< _type_, Sort_QuickDefault< _type_ > >
	{
	public:
		int
		Compare
		(
			_type_ const& i_a,
			_type_ const& i_b
		) const
		{
			if ( i_a < i_b )
			{
				return -1;
			}
			else if ( i_a > i_b )
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	};

	//----------------------------------------------------------------
	/* Sort_Heap
		Sort_Heap is a sort template class that implements the
		heap-sort algorithm on an array of objects of the specified data type.
	*/
	template< typename _type_, typename _derived_ >
	class Sort_Heap : public SortBase< _type_ >
	{
	public:
		virtual void
		Sort
		(
			_type_ * i_base,
			size_t i_num
		) const
		{
			// get all elements in heap order
			const _derived_* der = static_cast< const _derived_* >( this );

			// O( n )
			for ( size_t i = i_num / 2; i > 0; i-- )
			{
				// sift down
				size_t parent = i - 1;
				for ( size_t child = parent * 2 + 1; child < i_num; child = parent * 2 + 1 )
				{
					if ( child + 1 < i_num && der->Compare( i_base[child + 1], i_base[child] ) > 0 )
					{
						++child;
					}
					if ( der->Compare( i_base[child], i_base[parent] ) <= 0 )
					{
						break;
					}
					SwapValues( i_base[parent], i_base[child] );
					parent = child;
				}
			}

			// get sorted elements while maintaining heap order
			for ( size_t i = i_num - 1; i > 0; --i )
			{
				SwapValues( i_base[0], i_base[i] );
				// sift down
				size_t parent = 0;
				for ( size_t child = parent * 2 + 1; child < i; child = parent * 2 + 1 )
				{
					if ( child + 1 < i && der->Compare( i_base[child + 1], i_base[child] ) > 0 )
					{
						++child;
					}
					if ( child->Compare( i_base[child], i_base[parent] ) <= 0 )
					{
						break;
					}
					SwapValues( i_base[parent], i_base[child] );
					parent = child;
				}
			}
		}
	};

	//----------------------------------------------------------------
	/* Sort_HeapDefault
		Default heap-sort comparison function that can
		be used to sort scalars from small to large.
	*/
	template< typename _type_ >
	class Sort_HeapDefault : public Sort_Heap< _type_, Sort_HeapDefault< _type_ > >
	{
	public:
		int
		Compare
		(
			_type_ const& i_a,
			_type_ const& i_b
		) const
		{
			if ( i_a < i_b )
			{
				return -1;
			}
			else if ( i_a > i_b )
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	};

	//----------------------------------------------------------------
	/* Sort_Insertion
		Sort_Insertion is a sort template class that implements the
		insertion-sort algorithm on an array of objects of the specified data type.
	*/
	template< typename _type_, typename _derived_ >
	class Sort_Insertion : public SortBase< _type_ >
	{
	public:
		virtual void
		Sort
		(
			_type_* i_base,
			size_t i_num
		) const
		{
			_type_* lo = i_base;
			_type_* hi = i_base + ( i_num - 1 );
			const _derived_* child = static_cast< const _derived_* >( this );
			while( hi > lo )
			{
				_type_* max = lo;
				for ( _type_ * p = lo + 1; p <= hi; ++p )
				{
					if ( child->Compare( (*p), (*max) ) > 0 )
					{
						max = p;
					}
				}
				SwapValues( *max, *hi );
				--hi;
			}
		}
	};

	//----------------------------------------------------------------
	/* SortInsertionDefault
		Default insertion-sort comparison function that can
		be used to sort scalars from small to large.
	*/
	template< typename _type_ >
	class Sort_InsertionDefault : public Sort_Insertion< _type_, Sort_InsertionDefault< _type_ > >
	{
	public:
		int
		Compare
		(
			const _type_& i_a,
			const _type_& i_b
		) const
		{
			if ( i_a < i_b )
			{
				return -1;
			}
			else if ( i_a > i_b )
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	};
}
#endif // end of D_SORT_HPP