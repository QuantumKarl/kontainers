/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

//--------------------------------
/* Str()
	default ctor, minimal cost
*/
Str::Str
(
):
	m_array( nullptr ),
	m_size( 0 ),
	m_capacity( 0 )
{
	Init();
}

//--------------------------------
/* Str( other )
	ctor, copy constructor
*/
Str::Str
(
	Str const& other
):
	m_array( nullptr ),
	m_size( 0 ),
	m_capacity( 0 )
{
	*this = other;
}

//--------------------------------
/* Str( cStr )
	ctor, from c string
*/
Str::Str
(
	char const* i_cStr
):
	m_array( nullptr ),
	m_size( 0 ),
	m_capacity( 0 )
{
	assert( i_cStr != nullptr );

	size_t const len = strlen( i_cStr );

	// Alloc memory
	reallocate( StrResizePolicyNormal::Round( len ) );

	// Set the new size
	m_size = len;

	// Copy the string
	memcpy(	static_cast<void*>(m_array),
			static_cast<void const*>(i_cStr),
			len);

	m_array[m_size] = '\0';
}

//--------------------------------
/* Str( i_cStr, i_len )
	ctor, from c string
*/
Str::Str
(
	char const* i_cStr,
	size_t const i_len
):
	m_array( nullptr ),
	m_size( 0 ),
	m_capacity( 0 )
{
	assert( i_cStr != nullptr );
	assert( i_len > 0 );

	// Alloc memory
	reallocate( StrResizePolicyNormal::Round( i_len ) );

	// Set the new size
	m_size = i_len;

	// Copy the string
	memcpy(	static_cast<void*>(m_array),
			static_cast<void const*>(i_cStr),
			i_len);

	m_array[m_size] = '\0';
}

//--------------------------------
/* Str( start, end )
	ctor, iterate style init. copies elements from start to end into the string
*/
Str::Str
(
	char const* i_start,
	char const* i_end
):
	m_array( nullptr ),
	m_size( 0 ),
	m_capacity( 0 )
{
	assert( i_start != nullptr	&&
			i_end != nullptr );

	if( i_start != i_end )
	{
		size_t const count = ptr_range( i_start, i_end );
		assert( count <= strlen( i_start ) );

		// Alloc memory
		reallocate( StrResizePolicyNormal::Round( count ) );

		// Set the new size
		m_size = count;

		// Copy the string
		memcpy(	static_cast<void*>(m_array),
			static_cast<void const*>(i_start),
			count);

		m_array[m_size] = '\0';
	}
	// else empty string
}

//--------------------------------
/* ~Str
	dtor, clean up
*/
Str::~Str
(
)
{
	clear( true );
}

//--------------------------------
/* back
	return a const reference the to last char.
	this is the most recently push back char or the last iteration in a loop
*/
D_FORCE_INLINE
char const
Str::back
(
) const
{
	assert(m_array != nullptr);
	return m_array[m_size-1];
}

//--------------------------------
/* begin
	Returns a pointer to the beginning of the array.
	It is the opposite to end()
	It is useful for iterating over a list;
*/
D_FORCE_INLINE
char*
Str::begin
(
)
{
	assert( m_array != nullptr );
	return &m_array[0];
}

//--------------------------------
/* c_str
	Returns a pointer to the beginning of the array.

	Use where you need a const char*
*/
D_FORCE_INLINE
char const*
Str::c_str
(
) const
{
	assert( m_array != nullptr );
	return &m_array[0];
}

//--------------------------------
/* begin
	Returns a const pointer to the begining of the array. Useful for iterating through the m_array in loops.
	It is the opposite to end()
*/
D_FORCE_INLINE
char const*
Str::begin
(
) const
{
	assert( m_array != nullptr );
	return &m_array[0];
}

//--------------------------------
/* capacity
	Returns the number of characters allocated for.

	NOTE: this is the number of chars you can add before it
	will resize.
*/
D_FORCE_INLINE
size_t
Str::capacity
(
) const
{
	return m_capacity;
}

//--------------------------------
/* clear
	Clears the contents of the string, optionally deallocates memory.
*/
D_INLINE
void
Str::clear
(
	bool const i_dealloc
)
{
	if( i_dealloc == true )
	{
		if ( m_array != nullptr )
		{
			Mem_Free( m_array );
			m_array		= nullptr;
		}
		m_capacity	= 0;
		m_size		= 0;
	}
	else
	{
		m_size = 0;
	}
}

//--------------------------------
/* contains
	Just like C#'s contain function on the List class,
	returns true if char is in the string.

	NOTE: It is SLOW at O( n ) where n is the size().
	if you use this a lot, you might want to consider a HashTable
*/
D_INLINE
bool
Str::contains
(
	char const i_obj
) const
{
	for( size_t i = 0; i < m_size; ++i )
	{
		if ( m_array[ i ] == i_obj )
		{
			return true;
		}
	}

	// Not found
	return false;
}

//--------------------------------
/* contains
	Just like C#'s contains function on the List class,
	returns true if char is in the string.

	NOTE: It is SLOW at O( n ) where n is the size().
	if you use this a lot, you might want to consider a HashTable
*/
D_INLINE
bool
Str::contains
(
	char const* i_cStr,
	size_t const i_len
) const
{
	assert( i_len <= strlen( i_cStr ) );

	if( m_array != i_cStr )
	{
		// find first char (make sure there are enough chars after it to contain the sub str)
		char first = i_cStr[0];
		for( size_t i = 0; i < m_size; ++i )
		{
			if ( m_array[i] == first	&&
				(i+i_len) <= m_size )
			{
				// now str cmp from here
				if( strncmp( &m_array[i], i_cStr, i_len ) == 0 )
				{
					return true;
				}
			}
		}
	}
	else
	{
		return false;
	}

	// Not found
	return false;
}

//--------------------------------
/* contains
	Just like C#'s contains function on the List class,
	returns true if char is in the string.

	NOTE: It is SLOW at O( n ) where n is the size().
	if you use this a lot, you might want to consider a HashTable
*/
D_INLINE
bool
Str::contains
(
	Str const& i_str
) const
{
	size_t index;
	return find_index( i_str, index );
}

//--------------------------------
/* empty
	returns true if the container is empty (ie, size() == 0)
*/
D_FORCE_INLINE
bool
Str::empty
(
) const
{
	return m_size == 0;
}

//--------------------------------
/* end
	returns a pointer to m_array[size()].

	Dereferencing is undefined behavior.
	It is the opposite to begin().
*/
D_FORCE_INLINE
char const*
Str::end
(
) const
{
	assert( m_array != nullptr );
	return &m_array[m_size];
}

//--------------------------------
/* erase( index )
	Removes the char at the specified index and moves all data following the element down to fill in the gap.
	The number of elements in the m_array is reduced by one.
	Returns false if the index is outside the bounds of the string.
*/
D_FORCE_INLINE
bool
Str::erase
(
	size_t const i_index
)
{
	assert( m_array != nullptr );
	assert( i_index < m_size );

	if ( i_index < m_size )
	{
		--m_size;
		for(size_t i = i_index; i < m_size; ++i )
		{
			m_array[i] = m_array[i+1];
		}
		m_array[m_size] = '\0';
		return true;
	}
	else
	{
		return false;
	}
}

//--------------------------------
/* erase( char )
	Removes i_char if it is found within the string and moves all data
	following the chars down to fill in the gap.
	The number of chars in the m_array is reduced by one (m_size-1).
	Returns false if the data is not found in the list.
*/
D_FORCE_INLINE
bool
Str::erase
(
	char const i_char
)
{
	size_t index;
	if ( find_index( i_char, index ) == true )
	{
		return erase( index );
	}
	
	return false;
}

//--------------------------------
/* erase( str )
	Removes i_str if it is found within the string and moves all data
	following the chars down to fill in the gap.
	The number of chars in the m_array is reduced by the size of i_str ( m_size-i_str.size() ).
	Returns false if the data is not found in the list.
*/
D_FORCE_INLINE
bool
Str::erase
(
	Str const& i_str
)
{
	// Check the string is present
	size_t index;
	size_t size = i_str.size();
	if ( find_index( i_str, index ) == true )
	{
		if( index+size != m_size )
		{
			// now move the rest of the string down over it
			memmove( static_cast<void*>(&m_array[index]),
					static_cast<const void*>( &m_array[index+size] ),
					m_size - size );
		}
		//else we are erasing to the end and do not need move anything

		// update the size
		m_size -= size;

		// ensure null terminated
		m_array[m_size] = '\0';

		return true;
	}
	
	return false;
}

//--------------------------------
/* erase( start, end )
	Removes all chars in between i_start and i_end.
	following the chars down to fill in the gap.
	The number of chars in the m_array is reduced by the number of chars removed (m_size-end-start).
	Returns false if the data is not found in the list.
*/
D_FORCE_INLINE
bool
Str::erase
(
	size_t const i_start,
	size_t const i_end
)
{
	size_t const range = i_end - i_start;
	if( i_start < i_end &&
		range < m_size )
	{
		if( i_end != m_size )
		{
			// move the rest of the string down
			memmove( static_cast<void*>(&m_array[i_start]),
					static_cast<const void*>( &m_array[i_end] ),
					m_size - range );

			// update the size
			m_size -= range;

			// ensure null terminated
			m_array[m_size] = '\0';

			return true;
		}
		else // we are erasing to the end, so just set a new null and change the size
		{
			// update the size
			m_size -= range;

			// ensure null terminated
			m_array[m_size] = '\0';

			return true;
		}
	}
	return false;
}

//--------------------------------
/* find( char ) // NOTE: this is the same as idStr's first
	Searches for the specified data in the string and returns it's address.
	Returns nullptr if the data is not found.
*/
D_FORCE_INLINE
bool
Str::find
(
	char const	i_char,
	char*&		o_char
) const
{
	size_t i = 0;
	if ( find_index( i_char, i ) == true )
	{
		o_char = &m_array[i];
		return true;
	}
	else
	{
		// Not found
		o_char = nullptr;
		return false;
	}
}

//--------------------------------
/* find( str )
	Searches for the specified string in the string and returns it's address.
	Returns nullptr if the data is not found.
*/
D_FORCE_INLINE
bool
Str::find
(
	Str const& i_str,
	char*& o_char
) const
{
	size_t index;
	if( find_index( i_str, index ) == true )
	{
		o_char = &m_array[index];
		return true;
	}
	else
	{
		// Not found
		o_char = nullptr;
		return false;
	}
}

//--------------------------------
/* find_index( char, index )
	Searches for the specified data in the m_char and returns it's index.
	Returns false if the data is not found.

	NOTE: SLOW at O( n )
*/
D_FORCE_INLINE
bool
Str::find_index
(
	char const i_char,
	size_t& o_index
) const
{
	for( size_t i = 0; i < m_size; ++i )
	{
		if ( m_array[i] == i_char )
		{
			o_index = i;
			return true;
		}
	}

	// Not found
	o_index = 0;
	return false;
}


//--------------------------------
/* find_index( str, index )
	Searches for the specified data in the m_char and returns it's index.
	Returns false if the data is not found.
*/
D_FORCE_INLINE
bool
Str::find_index
(
	Str const& i_str,
	size_t& o_index
) const
{
	assert( m_array != nullptr && m_size > 0 );
	assert( i_str.c_str() != &m_array[0] );

	char const* start = strstr( &m_array[0], i_str.c_str() );
	if( start != nullptr )
	{
		o_index = index_of( start );
		return true;
	}
	else
	{
		// Not found
		o_index = 0;
		return false;
	}
}

//--------------------------------
/* front
	Returns a const reference to the first element.
	Opposite to back.

	NOTE: any operation on front() such as erase is the SLOWEST possible operation on a list
*/
D_FORCE_INLINE
char const&
Str::front
(
) const
{
	assert( m_array != nullptr);
	return m_array[0];
}

//--------------------------------
/* insert( char, index )
	Inserts i_char at i_index in the string.
	Moves all the elements after it up one.
	Returns false is index is out of bounds.
*/
D_FORCE_INLINE
bool
Str::insert
(
	char const	i_char,
	size_t		i_index
)
{
	if ( i_index < m_size )
	{
		CheckResize();

		// Move elements after m_size up one
		memmove(	static_cast<void*>(&m_array[i_index+1]),
					static_cast<void const*>(&m_array[i_index]),
					m_size - i_index );

		// Set the new value of i_index
		m_array[i_index] = i_char;

		++m_size;
		m_array[m_size] = '\0';
		return true;
	}
	else
	{
		return false;
	}
}

//--------------------------------
/* insert( str, index )
	Inserts i_str at i_index in the string.
	Moves all the elements after it up one.
	Returns false is index is out of bounds.
*/
D_FORCE_INLINE
bool
Str::insert
(
	Str const&	i_str,
	size_t		i_index
)
{
	size_t const size = i_str.size();
	if ( i_index < m_size )
	{
		// check resize
		if( m_size+size > m_capacity )
		{
			reallocate( StrResizePolicyNormal::GetNextSize( m_capacity ) );
		}

		// move elements after m_size up by size of i_str
		memmove( static_cast<void*>(&m_array[i_index+size]),
				static_cast<const void*>( &m_array[i_index] ),
				m_size - i_index );

		// copy other string into this one
		memcpy( static_cast<void*>( &m_array[i_index] ),
				static_cast<const void*>( i_str.c_str() ),
				size );

		// update size
		m_size += size;

		// make sure to null terminate
		m_array[m_size] = '\0';
		return true;
	}
	else
	{
		return false;
	}
}

//--------------------------------
/* index_of
	Takes a pointer to an element in the m_array and returns the index of the char.

	This is NOT a guarantee that the char is really in the string.
	Function will assert in debug builds if pointer is outside the bounds of the string,
	but remains silent in release builds.
*/
D_FORCE_INLINE
size_t
Str::index_of
(
	char const* i_ptr
) const
{
	size_t const index = ptr_range(i_ptr, static_cast<char const*>(&m_array[0]) );
	assert( index < m_size );
	return index;
}

//--------------------------------
/* pop_back
	Opposite to push_back(), remove the element at the back of the list

	NOTE: The element is not destroyed, so any memory used by it may not be freed until the destruction of the list.
*/
D_FORCE_INLINE
void
Str::pop_back
(
)
{
	assert( m_array != nullptr && m_size > 0 );
	--m_size;
	m_array[m_size] = '\0';
}

//--------------------------------
/* pop_back( numPops )
	Opposite to push_back(), but instead of just one element remove numPops number of elements

	NOTE: The elements are not destroyed, so any memory used by it may not be freed until the destruction of the list.
*/
D_FORCE_INLINE
void
Str::pop_back
(
	size_t const numPops
)
{
	assert( m_array != nullptr && m_size > 0 );
	assert( m_size - numPops < m_size ); // check that we will not wrap around
	assert( numPops <= m_size );

	// use memset so the side effects are the same as calling pop_back several times
	memset( static_cast<void*>(&m_array[m_size-numPops]), '\0', numPops );
	m_size -= numPops;
}

//--------------------------------
/* push_back( char )
	Appends a new element at the back of the list
*/
D_FORCE_INLINE
void
Str::push_back
(
	char const i_char
)
{
	CheckResize();

	m_array[m_size] = i_char;
	++m_size;
	m_array[m_size] = '\0';
}

//--------------------------------
/* push_back( str )
	Appends all chars from i_str
	Does not include end.
*/
D_FORCE_INLINE
void
Str::push_back
(
	Str const& i_str
)
{
	assert( i_str.size() > 0 );

	// Calc number of elements
	size_t const count = i_str.size();

	// Calc new size
	size_t const newSize = m_size + count;

	// Check we have enough space
	reserve( newSize );

	// copy them into our container
	memcpy(	static_cast<void*>(&m_array[m_size]),
			static_cast<void const*>(i_str.c_str()),
			count);

	// Set new size
	m_size = newSize;

	// Make sure to null terminate
	m_array[m_size] = '\0';
}

//--------------------------------
/* push_back( i_other )
	Append elements from start to end.
	Does not include end.
*/
D_FORCE_INLINE
void
Str::push_back
(
	char const* i_cStr
)
{
	assert( i_cStr != nullptr );

	// Calc number of elements
	size_t const len = strlen( i_cStr );

	// Calc new size
	size_t const newSize = m_size + len;

	// Check we have enough space
	reserve( newSize );

	// Copy them into our container
	memcpy(	static_cast<void*>( &m_array[m_size] ),
			static_cast<void const*>(i_cStr),
			len);

	// Set new size
	m_size = newSize;

	// Make sure to null terminate
	m_array[m_size] = '\0';
}

//--------------------------------
/* push_back( i_cStr, i_len )
	Append elements from start to end.
	Does not include end.
*/
D_FORCE_INLINE
void
Str::push_back
(
	char const* i_cStr,
	size_t const i_len
)
{
	assert( i_cStr != nullptr );
	assert( i_len > 0 );
	assert( i_len <= strlen( i_cStr ) );

	// Calc new size
	size_t const newSize = m_size + i_len;

	// Check we have enough space
	reserve( newSize );

	// Copy them into our container
	memcpy(	static_cast<void*>( &m_array[m_size] ),
			static_cast<void const*>(i_cStr),
			i_len);

	// Set new size
	m_size = newSize;

	// Make sure to null terminate
	m_array[m_size] = '\0';
}

//--------------------------------
/* push_back( start, end )
	Append elements from start to end.
	Does not include end.
*/
D_FORCE_INLINE
void
Str::push_back
(
	char const* i_start,
	char const* i_end
)
{
	assert( i_start != nullptr && i_end != nullptr );

	// Calc number of elements
	size_t const count = ptr_range( i_start, i_end );
	assert( count <= strlen( i_start ) );

	// Calc new size
	size_t const newSize = m_size + count;

	// Check we have enough space
	reserve( newSize );

	// copy them into our container
	memcpy(	static_cast<void*>(&m_array[m_size]),
			static_cast<void const*>(i_start),
			count);

	// Set new size
	m_size = newSize;

	// Make sure to null terminate
	m_array[m_size] = '\0';
}

//--------------------------------
/* push_back( formatCStr, va_list )
	append printf formatted arguments.

	duplicate of format function.
*/
void
Str::push_backf
(
	D_VERIFY_FORMAT_STRING char const* formatCStr,
	...
)
{
	// Set up a buffer to write into
	size_t const bufferSize = static_cast<size_t>( format_buffer_size ); // See header for size
	char buffer[bufferSize];
	buffer[bufferSize-1] = '\0';
	
	// Setup va list
	va_list argptr;
	va_start( argptr, formatCStr ); //-V111

	// Write into buffer
	int l = _vsnprintf_s( buffer, bufferSize-1, formatCStr, argptr );

	// clean up va list
	va_end( argptr );

	assert( l != bufferSize-1 );
	assert( l > 0 );

	// append into the string
	if( l > 0 )
	{
		// make sure to null terminate the c string
		buffer[l] = '\0';
		push_back(buffer, static_cast<size_t>(l) );
	}
}

//--------------------------------
/* reserve( newMinSize )
	Requests the container to resize if newMineSize is large then its current capacity,
	ie reserve capacity in advance.
	if the container is already large enough no allocations will take place.
*/
D_FORCE_INLINE
void
Str::reserve
(
	size_t const newMinSize
)
{
	// if needs to expand
	if( newMinSize >= m_capacity ) // >= because we need +1 for the null terminator
	{
		// get next size
		size_t const nextSize = StrResizePolicyNormal::GetNextSize( m_size );

		// if next size suffices
		if( nextSize > newMinSize )
		{
			reallocate( nextSize );
		}
		else// next size is not big enough, get next size from requested min size
		{
			reallocate( StrResizePolicyNormal::GetNextSize( newMinSize  ) );

			assert( m_capacity > newMinSize );
		}
	}
	// else big enough already
}

//--------------------------------
/* reallocate( newCapacity )
	Allocates memory for the amount of elements requested while keeping the contents intact.
	Contents are copied using their = operator so that data is correctly instantiated.

	eg if you know you are not going to use more that 256 elements,
	it would be efficient to call reallocate( 256 ) before adding any elements.
	However, there is the static sized container you may want to consider.

	NOTE: usually used internally in this class, use it with care.
*/
D_FORCE_INLINE
void
Str::reallocate
(
	size_t const newCapacity
)
{
	if( newCapacity > 0 )
	{
		if ( newCapacity == m_capacity )
		{
			// not changing the size, so just exit
			return;
		}

		// allocate memory
		if ( newCapacity > 0 )
		{
			char* newptr = (char*)Mem_Alloc( sizeof(char) * newCapacity );

			if( m_array != nullptr )
			{
				size_t const overlap = Min( m_capacity, newCapacity );

				// copy into new array
				memcpy(	static_cast<void*>( newptr ),
					static_cast<void const*>( m_array ),
					overlap );

				Mem_Free( m_array );
			}
			m_array = newptr;
		}
		else
		{
			Mem_Free( m_array );
			m_array = nullptr;
		}

		m_capacity = newCapacity;

		// if the container just reduced in size
		if ( m_size > m_capacity)
		{
			m_size = m_capacity;
		}

#if 0
		// memset the last multiple of our memory alignment (probably 16), just in case they used in a compare
		size_t const index = m_capacity - c_Memory_Alignment;

		// if we havent wrapped around
		if( index < m_capacity )
		{
			memset( static_cast<void*>(&m_array[index]), '\0', c_Memory_Alignment );
		}
#endif

		// Add the nullptr to terminate the string
		m_array[m_size] = '\0';
	}
	else// free up the m_list if no data is being reserved
	{
		clear( true );
		return;
	}
}

//--------------------------------
/* size
	Returns the number of chars currently contained in the list.
*/
D_FORCE_INLINE
size_t
Str::size
(
) const
{
	assert( strlen(m_array) == m_size );
	return m_size;
}

//--------------------------------
/* sub_str( start, end )
	Creates a new string using the string as source.

	ie, you will get a string from start to end out of this string.
*/
D_INLINE
Str
Str::sub_str
(
	size_t const i_start,
	size_t const i_end
)
{
	assert( i_end < m_size );
	assert( i_start < i_end );
	assert( i_start != i_end );

	return Str( &m_array[i_start], &m_array[i_end] );
}

//--------------------------------
/* shrink_to_fit
	Resizes the array to exactly the number of elements it contains or frees up memory if empty.

	NOTE: call this when you know there are going to be no more elements added
*/
D_FORCE_INLINE
void
Str::shrink_to_fit
(
)
{
	if ( m_array != nullptr )
	{
		if ( m_size > 0 )
		{
			reallocate( m_size );
		}
		else
		{
			clear(true);
		}
	}
}

//--------------------------------
/* shuffle( ran )
	Shuffles the list. (opposite to sort)
	Pass in the RNG you want the function to draw its random
	numbers from.

	NOTE: SLOW

	NOTE: When the container is not on the main thread,
	a reference to an instance of RNG should be given to it.
*/
D_FORCE_INLINE
void
Str::shuffle
(
	Random::RNG& ran
)
{
	Algorithm::shuffle( m_array, m_size, ran );
}

//--------------------------------
/* sort( 	Performs a sort on the list using the supplied sort algorithm.
	QuickSort is the default.

	NOTE: The data is moved around the list, so any pointers to data within the list may 
	no longer be valid.
*/
D_FORCE_INLINE
void
Str::sort
(
	const Algorithm::SortBase<char>& sort // = Algorithm::Sort_QuickDefault<_type_>()
)
{
	assert( m_array != nullptr );
	sort.Sort( m_array , m_size );
}

//--------------------------------
/* _allocated
	Returns the total memory allocated for the list in bytes,
	but doesn't take into account additional memory allocated by _type_.

	ie allocated size * element size
*/
D_FORCE_INLINE
size_t
Str::_allocated
(
) const
{
	return m_capacity * sizeof( char );
}

//--------------------------------
/* _size
	Returns total size of the list in bytes,
	but doesn't take into account additional memory allocated by _type_

	ie class size + allocation size
*/
D_FORCE_INLINE
size_t
Str::_size
(
) const
{
	return sizeof( Str ) + _allocated();
}

//--------------------------------
/* _memory_used
	Returns size of the used elements in the list.

	ie element size * num used elements
*/
D_FORCE_INLINE
size_t
Str::_memory_used
(
) const
{
	return m_size * sizeof( char );
}

//--------------------------------
/* Init
	Checks if m_list needs to be allocated, and allocates it if necessary.
*/
D_FORCE_INLINE
void
Str::Init
(
)
{
	if ( m_array == nullptr )
	{
		assert( m_size == 0 );
		reallocate( StrResizePolicyNormal::GetNextSize( m_size ) );
	}
}

//--------------------------------
/* CheckResize
	Checks if m_list / m_capacity needs to be increased in size.
*/
D_FORCE_INLINE
void
Str::CheckResize
(
)
{
	if( m_size < m_capacity )
	{
		return;
	}
	else
	{
		reallocate( StrResizePolicyNormal::GetNextSize( m_capacity ) );
	}
}