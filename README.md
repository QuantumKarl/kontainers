### Readme ###

### What is this repository for? ###

This repository contains STL like containers created for performance.
They are not intended to replace or improve upon STL containers, they were created for the enjoyment of creating such structures.

### How do I get set up? ###

Currently the project is configured for Visual studio 2010. Therefore to start compiling:

1. Get a copy of all the files.

2. Open the .sln found in the ide\VS2010 folder.

3. Compile your desired configuration.

### I want to look at X, where should I look? ###
The codebase is structured as follows:

1. src - contains all source code, with sub folders grouping the codebase.

2. ide - contains project files to assist building the codebase.

3. ext/libs - contains libraries required for using Agner Fog's asm library.


For example "I want to look at the hash table and it's tests" you would:

1. open "src" folder.

2. open "containers" folder.

3. open "Map.hpp" and "Map.inl".

4. navigate up one directory (back to "src").

5. open "tests" folder.

6. open "MapVSunorded_map.hpp" and "MapVSunorded_map.inl".

7. look for a function name that ends with "RunA" or "RunB", this will be calling the testing functions in a loop.

### How is it tested? ###
The testing function is found in "Testbed_Utils::testCode", it runs a function given to it a number of times, timing each run using the RDTSC counter and a window's timer. Finally it writes the averaged time to "Results.txt". Each container has it's own tests which has it's own STL version of the test. The STL and kontainers tests are as similar as possible, only the object name differs e.g. Vector<float> becomes List<float>.