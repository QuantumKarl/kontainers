/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_MAP_VS_UNORDERED_MAP )
#define D_MAP_VS_UNORDERED_MAP

class MapVSunordered_map
{
public:
	MapVSunordered_map();

	static void RunMapTests();
	static void Rununordered_mapTests();

	LoopCount const& GetLoopCount() const { return m_loopCount; }
private:

	void MapRunA();
	void unordered_mapRunA();

	// Karl containers
	void TestStringToIndex( size_t const size );
	void TestIndexToString( size_t const size );

	// STL containers
	void Test_unordered_map_StringToIndex( size_t const size );
	void Test_unordered_map_IndexToString( size_t const size );

	// Loop quantities
	LoopCount m_loopCount;
};

#include "MapVSunordered_map.inl"

#endif