/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>.

	Info: This random number generator is a combined(and modified) generator.
	The most part is from Numerical Recipes(third edition) Chapter 7 page 342 the "Ran" generator.
	For the floating point generation Doom3's floating point IEE has been combined with "Ran" see:
	https://github.com/id-Software/DOOM-3-BFG/blob/master/neo/idlib/math/Random.h

	Numerical Recipes extract on "Ran":
	Implementation of the highest quality recommended generator. The constructor is called with
	an integer seed and creates an instance of the generator. The member functions int64, double,
	and int32 return the next values in the random sequence, as a variable type indicated by their
	names. The period of the generator is ~3.128 * 10^57.
*/
#if !defined( D_RNG_HPP )
#define D_RNG_HPP

// Random Number Generator
class RNG
{
public:
	RNG(uint64 j = 0)
	{
		Init(j);
	}

	// Initialises and sets up the seed values
	void Init( uint64 j )
	{
		assert( j != 4101842887655102017LL );// Choose another value
		v = 4101842887655102017LL;
		w = 1;
		u = j ^ v;
		Integer();
		v = u;
		Integer();
		w = v;
		Integer();
		breg = 0;
		bc = 0;
		dd = 0;
		inext = 0;
		inextp = 31;
		for (int k=0; k<55; k++)
		{
			dtab[k] = 5.42101086242752217E-20 * Integer();
		}
		s = static_cast<ulong>( j+1 );
	}

	// Returns a random uint64 between [0,uint64MAX]
	D_FORCE_INLINE uint64 Integer()
	{
		u = u * 2862933555777941757LL + 7046029254386353087LL;
		v ^= v >> 17;
		v ^= v << 31;
		v ^= v >> 8;
		w = 4294957665U*(w & 0xffffffff) + (w >> 32); //-V112
		uint64 x = u ^ (u << 21);
		x ^= x >> 35; x ^= x << 4;
		return (x + v) ^ w;
	}

	// Returns a random uint64 between [min,max]
	D_FORCE_INLINE uint64 Integer(uint64 min, uint64 max)
	{
		return ConvertRange<uint64>(0,uint64MAX,min,max,Integer());
	}

	// Returns a random real between [0.0,1.0]
	D_FORCE_INLINE double Real()
	{
		if (++inext == 55) inext = 0;
		if (++inextp == 55) inextp = 0;
		dd = dtab[inext] - dtab[inextp];
		if (dd < 0) dd += 1.0;
		double r = dtab[inext] = dd;
		s = 1664525L * s + 1013904223L;
		ulong i = 0x3f800000 | ( s & 0x007fffff );
		r += (double)( ( *(float *)&i ) - 1.0f );// intensional use of incompatible binary representation
		if (r >= 1.0) r -= 1.0;
		return r;
	}

	// Returns a random uint32 between [0,uint32MAX]
	D_FORCE_INLINE uint32 Int()
	{
		return (uint32)Integer();
	}

	// Returns a random uint32 between [min,max]
	D_FORCE_INLINE uint32 Int(uint32 min, uint32 max)
	{
		return ConvertRange<uint32>(0,uint32MAX,min,max,(uint32)Integer());
	}

	// Returns a byte containing random bits
	D_FORCE_INLINE byte Byte()
	{
		if ((--bc) != 0)
		{
			return static_cast<byte>(breg >>= 8);
		}
		breg = Integer();
		bc = 7;
		return static_cast<byte>(breg);
	}

private:
	double dtab[55], dd;
	uint64 u,v,w, breg;
	size_t inext, inextp, bc;
	ulong s;
};

#endif // end of D_RAND_HPP