/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

//--------------------------------
/* Set( i_arraySize, i_resizeLoad )
	ctor, initialise members,

	NOTE: does NOT allocate memory, until first insert or reserve
*/
template<typename key_t>
D_FORCE_INLINE
Set<key_t>::Set //-V524
(
	size_t const i_arraySize,	// = 256
	real_t const i_resizeLoad	// = 0.45
):
	m_resizeLoad( i_resizeLoad ),
	m_arraySize( i_arraySize ),
	m_arrayMask( Primes::findLE(i_arraySize) ),// Next lowest prime number
	m_compressionMask( i_arraySize - 1 ),
	m_numEntries( 0 ),
	m_resizeThreshold( static_cast<size_t>( static_cast<real_t>( i_arraySize ) * m_resizeLoad + 0.5 ) ),
	m_array( nullptr ),
	m_defaultKey()
{
	assert( i_resizeLoad <= 0.6 && i_resizeLoad >= 0.1 );
	assert( IsPowerOf2( i_arraySize ) );

#ifdef D_TRACK_PROBING
	m_iNumProbes = 0;
#endif

	Init();
}

//--------------------------------
/* Set( i_other )
	copy ctor, creates a hash table by copying other
*/
template<typename key_t>
D_FORCE_INLINE
Set<key_t>::Set
(
	Set const& i_other
):
	m_resizeLoad( 0 ),
	m_arraySize( 0 ),
	m_arrayMask( 0 ),
	m_compressionMask( 0 ),
	m_numEntries( 0 ),
	m_resizeThreshold( 0 ),
	m_array( nullptr ),
	m_defaultKey()
{
	copy_from( i_other );

#ifdef D_TRACK_PROBING
	m_iNumProbes = 0;
#endif
}

//--------------------------------
/* ~Set
	dtor, tidy up
*/
template<typename key_t>
Set<key_t>::~Set //-V524
(
)
{
	delete[] m_array;
}

//--------------------------------
/* clear( i_dealloc )
	Removes all the added elements from the hash map.

	NOTE: The table is not to be used after clear(true),
	it is purely to deallocate memory early. (ie before dtor)
*/
template<typename key_t>
void
Set<key_t>::clear
(
	bool const i_dealloc // = true
)
{
	assert( m_array != nullptr );
	if( i_dealloc == true )
	{
		delete[] m_array;

		m_array = nullptr;
		m_arraySize			= 0;
		m_arrayMask			= 0;
		m_compressionMask	= 0;
		m_numEntries		= 0;
		m_resizeThreshold	= 0;
	}
	else// set all the inserted entries to their defaults
	{
		for( size_t i = 0; i < m_arraySize; ++i )
		{
			// if there is something to remove (ie its not default)
			if( Entry_t::Equal( m_array[i].key, m_defaultKey ) == false )
			{
				// Set it to defaults
				m_array[i].key = m_defaultKey;
			}
		}
		m_numEntries	= 0;
	}
}

//--------------------------------
/* find( i_key )
	Finds your key in the hash table.
	It returns true if it is in the hash table.
*/
template<typename key_t>
D_INLINE
bool //-V659
Set<key_t>::find 
(
	key_t const& i_key
) const
{
	assert( m_array != nullptr );

	for (size_t idx = Entry_t::GetHash( i_key,m_arrayMask);; ++idx )
	{
		idx &= m_compressionMask;

		key_t probedKey = m_array[idx].key;
		if ( Entry_t::Equal( probedKey, i_key ) )
		{
			return true;
		}
		else if ( Entry_t::Equal( probedKey, m_defaultKey ) )
		{
			return false;
		}
	}
}

//--------------------------------
/* for_all_elements( i_functionsToApply ) const
	Same as above but the function passed must be const

	for example

	Set<string> mySet;
	// ...Add lots of strings

	// print each element
	mySet.for_all_elements( []( string const& val ) { cout << val << endl; } );

	NOTE: If you need to use this often its a sign you should not be using a set / hash table
*/
template<typename key_t>
template<typename lambda>
void
Set<key_t>::for_all_elements
(
	lambda const i_functionToApply
) const
{
	assert( m_array != nullptr );
	for( size_t i = 0; i < m_arraySize; ++i )
	{
		// if its a valid entry
		if( Entry_t::Equal( m_array[i].key, m_defaultKey ) == false )
		{
			// apply the users function
			i_functionToApply( static_cast<_value_ const>( m_array[i].value ) );
		}
	}
}

//--------------------------------
/* insert( i_key )
	Adds a key into the table.
	May cause the table to resize and rehash.

	NOTE: can not have a default constructed key as the key
*/
template<typename key_t>
D_INLINE
bool //-V659
Set<key_t>::insert
(
	key_t const& i_key
)
{
	// You can not insert a key which has the same value as the default constructor of its type
	assert( Entry_t::Equal( i_key, m_defaultKey ) == false );

	// Check if we need to expand
	if( m_numEntries > m_resizeThreshold )
	{
		resize( m_arraySize*2 );
	}

#ifdef D_TRACK_PROBING
	size_t iProbes = 0;
#endif

	for (size_t idx = Entry_t::GetHash( i_key, m_arrayMask );; ++idx )
	{
		idx &= m_compressionMask;

		key_t prevKey = m_array[idx].key;
		if ( Entry_t::Equal( prevKey, m_defaultKey ) )
		{
			m_array[idx].key	= i_key;

			++m_numEntries;
#ifdef D_TRACK_PROBING
			if( iProbes > m_iNumProbes )
			{
				m_iNumProbes = iProbes;
			}
#endif
			return true;
		}
		else if( Entry_t::Equal( prevKey, i_key ) )
		{
			return false;
		}

#ifdef D_TRACK_PROBING
		++iProbes;
#endif
	}
}

//--------------------------------
/* resize( i_newSize )
	Reallocates the table to newTable size,
	rehashes and copies all the entries into the
	newly allocated table.

	NOTE: SLOW! reserve is probably what you are after
*/
template<typename key_t>
void
Set<key_t>::resize
(
	size_t const& i_newSize
)
{
	// assert that the new table size is acceptable
	assert( i_newSize > 0 && i_newSize > m_numEntries * 2 );
	assert( IsPowerOf2( i_newSize ) == true && i_newSize <= Primes::MaxPrime() ) ;

	// Create a new table
	size_t const newCompressionMask	= i_newSize - 1;
	size_t const newTableMask		= Primes::findLE( i_newSize );
	Entry_t* newTable				= new Entry_t[i_newSize];

	if( m_array != nullptr )
	{
		// Insert every element from the current table into the new table
		for( size_t i = 0; i < m_arraySize; ++i )
		{
			// if there is something to insert
			if( Entry_t::Equal( m_array[i].key, m_defaultKey ) == false )
			{
				// Start probing
				for( size_t idx = Entry_t::GetHash( m_array[i].key, newTableMask );; ++idx )
				{
					idx &= newCompressionMask;

					if( Entry_t::Equal( newTable[idx].key, m_defaultKey ) )
					{
						newTable[idx].key	= m_array[i].key;

						break;// Break from probing loop
					}
				}
			}
		}

		// Delete old table
		delete[]	m_array;
	}

	// Swap the tables info
	m_array		= newTable;
	m_arraySize	= i_newSize;
	m_arrayMask	= newTableMask;
	m_compressionMask = newCompressionMask;
	m_resizeThreshold = static_cast<size_t>( static_cast<real_t>( i_newSize ) * m_resizeLoad + 0.5f );
}

//--------------------------------
/* reserve( i_numElements )
	Allocates the minimal amount of memory
	for numElements number of elements.

	NOTE: First call will always allocate memory.
*/
template<typename key_t>
D_FORCE_INLINE
void //-V524
Set<key_t>::reserve
(
	size_t const& i_numElements
)
{
	// Check if we need to resize
	if( static_cast<real_t>( i_numElements ) / m_resizeLoad > m_arraySize )// resizeLoad asserted to be valid in constructors
	{
		// Get next power of two that is greater then requested number of elements
		size_t const newSize = static_cast<size_t>( PowerOf2GT( i_numElements ) );
		assert( newSize > i_numElements );
		assert( newSize >= static_cast<size_t>(default_capacity) ); // If this fires you do not need to reserve

		// re-alloc
		resize( newSize );
	}
}

//--------------------------------
/* size
	Returns the number of entries
	that have been added to the table.

	NOTE: This is NOT the table size,
	this is the number of successful inserts.
*/
template<typename key_t>
D_FORCE_INLINE
size_t
Set<key_t>::size
(
) const
{
	return m_numEntries;
}

//--------------------------------
/* _allocated
	Returns the total memory allocated for the hashTable in bytes,
	but doesn't take into account additional memory allocated by key_t.

	ie allocated tableSize * entry size
*/
template<typename key_t>
size_t //-V524
Set<key_t>::_allocated
(
) const
{
	return sizeof( Entry_t ) * m_arraySize;
}

//--------------------------------
/* _size
	Returns total size of the hashTable in bytes,
	but doesn't take into account additional memory allocated by key_t.

	ie class size + allocation size
*/
template<typename key_t>
size_t
Set<key_t>::_size
(
) const
{
	return sizeof( Set<key_t> ) + _allocated();
}

//--------------------------------
/* _memory_used
	Returns size of the used elements in the hashTable.

	ie entry size * num used entries
*/
template<typename key_t>
size_t //-V524
Set<key_t>::_memory_used
(
) const
{
	return m_numEntries * sizeof( Entry_t );
}

//--------------------------------
/* operator =
	copies another hash table and
	asigns it to this
*/
template<typename key_t>
D_FORCE_INLINE
Set< key_t >&
Set<key_t>::operator=
(
	Set const& i_other
)
{
	copy_from( i_other );
	return *this;
}

//--------------------------------
/* copy_from( i_other )
	copies another hash table and
	asigns it to this
*/
template<typename key_t>
void
Set<key_t>::copy_from
(
	Set const& i_other
)
{
	if( i_other.m_array != nullptr	&&
		i_other.m_arraySize > 0 )
	{
		// Delete our own table
		if( m_array != nullptr )
		{
			// if the table sizes differ then just delete the table
			if( m_arraySize != i_other.m_arraySize )
			{
				delete[] m_array;
				m_array = new Entry_t[i_other.m_arraySize];
			}
			else// avoid the deletion since they match in size
			{
				clear(false);
			}
		}

		// Copy all data members
		m_resizeLoad		= i_other.m_resizeLoad;
		m_arraySize			= i_other.m_arraySize;
		m_arrayMask			= i_other.m_arrayMask;
		m_compressionMask	= i_other.m_compressionMask;
		m_numEntries		= i_other.m_numEntries;
		m_resizeThreshold	= i_other.m_resizeThreshold;

		// Copy edited entries into the table (don't use memcpy in case the key needs to do a deep copy)
		for( size_t i = 0; i < m_arraySize; ++i )
		{
			if( Entry_t::Equal( i_other.m_array[i].key, m_defaultKey ) == false )
			{
				m_array[i].key	= i_other.m_array[i].key;
			}
		}
	}
	else
	{
		// Nothing to copy
		assert( false );
	}
}

//--------------------------------
/* Init
	Checks if m_array needs to be allocated
*/
template<typename key_t>
D_FORCE_INLINE
void //-V524
Set<key_t>::Init
(
)
{
	if( m_array == nullptr )
	{
		assert( m_arraySize > 0 );
		m_array = new Entry_t[m_arraySize];
	}
}