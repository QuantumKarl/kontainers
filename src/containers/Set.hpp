/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_SET_HPP )
#define D_SET_HPP

// Toggles tracking of the number of max probes taken to insert.
// Used for performance monitoring.
#if !defined( D_FINAL )
	#define D_TRACK_PROBING
#endif

//----------------------------------------------------------------
/* Set (Hash Set)
	Uses linear probing and MurMur2 Hash.
	Does not support removing entries.

	NOTE: also known as a "Bloom Filter" and "Set" etc
*/
template< typename key_t >
class Set
{
	#include "SetEntries.hpp"

	enum
	{
		default_capacity = 256
	};
public:
	Set( size_t const i_arraySize = default_capacity, real_t const i_resizeLoad = 0.45 ); // resizeLoad must be less than 0.6
	Set( Set const& i_other );

	~Set();

	// C
	void			clear( bool const i_dealloc = true );		// Removes all added elements, optionally deallocates memory

	// F
	bool			find( key_t const& i_key ) const;			// finds a key in the set
	template <typename lambda > void for_all_elements( lambda const i_functionToApply ) const; // Apply a lambda function to each element added into the set

	// I
	bool			insert( key_t const& i_key );				// inserts / adds a value to the set

	// R
	void			resize( size_t const& i_newSize );			// Resizes and re hashes the internal array, contents are preserved.
	void			reserve( size_t const& i_newSize );			// Requests a resize if necessary (to the appropriate size).

	// S
	size_t			size() const;								// Returns the number of added entries in the set

	// Misc functions
	size_t			_allocated() const;							// returns total size of allocated memory
	size_t			_size() const;								// returns total size of allocated memory including size of Set(class)
	size_t			_memory_used() const;						// returns size of the used entries in the Set

	Set&		operator=( Set const& i_other );

#if defined( D_TRACK_PROBING )
	size_t m_iNumProbes;
#endif
private:
	void			copy_from( Set const& i_other );	// copies other set to this
	void			Init();								// checks if m_array needs to be allocated

	typedef			SetEntry< key_t > Entry_t;	// An entry in the set

	real_t			m_resizeLoad;		// The percentage load at which the array resizes. Must be less than 0.6,
										//	the lower the value the better the performance will be. However, it will use up more memory / resize earlier.

	size_t			m_arraySize;		// The number of entries allocated on the end of m_array
	size_t			m_arrayMask;		// A prime number just under arraySize, used for (improving) hashing
	size_t			m_compressionMask;	// A number used to compress the hash to an index
	size_t			m_numEntries;		// The number of entries in the set
	size_t			m_resizeThreshold;	// The number of entries required to force the set to resize (load factor threshold)

	Entry_t*		m_array;			// Pointer to an array of Entry_t
	
	// For comparison (to avoid temporaries)
	key_t const 	m_defaultKey;
};

#include "Set.inl"

#endif // end of D_SET_HPP