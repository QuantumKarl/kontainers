/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#include <queue>
using std::queue;

//----------------------------------------------------------------
/* Init loop counters and other magic numbers
*/
QueueVSqueue::QueueVSqueue()
{
#if defined( D_FULL_TEST )
	m_loopCount.SetOuterLoopAtIndex(0,3); // RunA first outer loop
	m_loopCount.SetInnerLoopAtIndex(0,300); // RunA first inner loop
	m_loopCount.SetInnerLoopAtIndex(1,1024); // RunA second inner loop

	m_loopCount.SetArgumentAtIndex(0, SourceData::allWords.size() ); // Strings test arg
	m_loopCount.SetArgumentAtIndex(1, Min( Primes::NumPrimes(), SourceData::randomFloats.size() ) ); // Numbers test arg
	m_loopCount.SetCyclesAndRunsArgs(2,4);
	//m_loopCount.SetCyclesAndRunsArgs(1,1);
#else
	m_loopCount.SetOuterLoopAtIndex(0,1); // RunA first outer loop
	m_loopCount.SetInnerLoopAtIndex(0,1); // RunA first inner loop
	m_loopCount.SetInnerLoopAtIndex(1,1); // RunA second inner loop
	m_loopCount.SetArgumentAtIndex(0, 128 ); // Strings test arg
	m_loopCount.SetArgumentAtIndex(1, 128 ); // Numbers test arg

	m_loopCount.SetCyclesAndRunsArgs(1,1);
#endif
}
//----------------------------------------------------------------
// Test KARL Queue

//--------------------------------
/*
*/
void QueueVSqueue::QueueTestStrings( size_t const i_size )
{
	assert( i_size <= SourceData::allWords.size() );

	Queue<string> q1;
	Queue<string> q2;

	// Add two, and pop one
	size_t halfSize = i_size / 2;
	size_t j = 0;
	for(size_t i = 0; i < halfSize; i+=2, ++j )
	{
		q1.push_back( SourceData::allWords[i] );
		q1.push_back( SourceData::allWords[i+1] );

		if( q1.front() != SourceData::allWords[j] )
		{
			cout << "ERROR IN KARL QUEUE" << endl;
			return;
		}

		q2.push_back( q1.front() );
		q1.pop_front();
	}

	// Pop the remaining entries
	while( q1.empty() == false )
	{
		if( q1.front() != SourceData::allWords[j] )
		{
			cout << "ERROR IN KARL QUEUE" << endl;
			return;
		}
		q2.push_back( q1.front() );
		q1.pop_front();
		++j;
	}

	// Check q2 is the correct size and empty it
	assert( halfSize == q2.size() );
	q2.clear(true);
}

//--------------------------------
/*
*/
void QueueVSqueue::QueueTestNumbers( size_t const i_size )
{
	assert( i_size <= Primes::NumPrimes() );
	assert( i_size <= SourceData::randomFloats.size() );

	Queue<real_t> q1;
	Queue<real_t> q2;
	Queue<real_t> q3;

	// Fill Q1 and Q2
	for( size_t i = 0; i < i_size; ++i )
	{
		q1.push_back( static_cast<real_t>(Primes::PrimeAt(i)) );
		q2.push_back( SourceData::randomFloats[i] );
	}

	// Empty Q1 and Q2 while filling Q3
	for(size_t i = 0; i < i_size; ++i )
	{
		q3.push_back( q1.front() * q2.front() );
		q1.pop_front();
		q2.pop_front();
	}

	// Empty q3 to create the sum
	real_t sum = 0.0;
	while( q3.empty() == false )
	{
		sum += q3.front();
		q3.pop_front();
	}
}

//----------------------------------------------------------------

//----------------------------------------------------------------
// Test STL queue
//--------------------------------
/*
*/
void QueueVSqueue::queueTestStrings( size_t const i_size )
{
	assert( i_size <= SourceData::allWords.size() );

	queue<string> q1;
	queue<string> q2;

	// Add two, and pop one
	size_t halfSize = i_size / 2;
	size_t j = 0;
	for(size_t i = 0; i < halfSize; i+=2, ++j )
	{
		q1.push( SourceData::allWords[i] );
		q1.push( SourceData::allWords[i+1] );

		if( q1.front() != SourceData::allWords[j] )
		{
			cout << "ERROR IN KARL QUEUE" << endl;
			return;
		}

		q2.push( q1.front() );
		q1.pop();
	}

	// Pop the remaining entries
	while( q1.empty() == false )
	{
		if( q1.front() != SourceData::allWords[j] )
		{
			cout << "ERROR IN KARL QUEUE" << endl;
			return;
		}
		q2.push( q1.front() );
		q1.pop();
		++j;
	}

	// Check q2 is the correct size and empty it
	assert( halfSize == q2.size() );
	while(q2.empty() == false )
	{
		q2.pop();
	}
}

//--------------------------------
/*
*/
void QueueVSqueue::queueTestNumbers( size_t const i_size )
{
	assert( i_size <= Primes::NumPrimes() );
	assert( i_size <= SourceData::randomFloats.size() );

	queue<real_t> q1;
	queue<real_t> q2;
	queue<real_t> q3;

	// Fill Q1 and Q2
	for( size_t i = 0; i < i_size; ++i )
	{
		q1.push( static_cast<real_t>(Primes::PrimeAt(i)) );
		q2.push( SourceData::randomFloats[i] );
	}

	// Empty Q1 and Q2 while filling Q3
	for(size_t i = 0; i < i_size; ++i )
	{
		q3.push( q1.front() * q2.front() );
		q1.pop();
		q2.pop();
	}

	// Empty q3 to create the sum
	real_t sum = 0.0;
	while( q3.empty() == false )
	{
		sum += q3.front();
		q3.pop();
	}
}
//----------------------------------------------------------------

//--------------------------------
/*
*/
void QueueVSqueue::QueueRunA()
{
	for( size_t j = 0; j < m_loopCount.GetOuterLoopA(); ++j )
	{
		for(size_t i = 0;i < m_loopCount.GetInnerLoopA(); ++i) // approx 6 seconds
		{
			QueueTestStrings( m_loopCount.GetArgumentA() );
		}

		for(size_t i = 0; i < m_loopCount.GetInnerLoopB() ; ++i) // approx 9 seconds
		{
			QueueTestNumbers( m_loopCount.GetArgumentB() );
		}
	}
}

//--------------------------------
/*
*/
void QueueVSqueue::queueRunA()
{
	for( size_t j = 0; j < m_loopCount.GetOuterLoopA(); ++j )
	{
		for(size_t i = 0;i < m_loopCount.GetInnerLoopA(); ++i) // approx 9 seconds
		{
			queueTestStrings( m_loopCount.GetArgumentA() );
		}

		for(size_t i = 0; i < m_loopCount.GetInnerLoopB() ; ++i) // approx 36 seconds
		{
			queueTestNumbers( m_loopCount.GetArgumentB() );
		}
	}
}

//--------------------------------
/*
*/
void QueueVSqueue::RunQueueTests()
{
	QueueVSqueue test;
	TestUtils::testCode("Karl Queue Test",
						[&](){test.QueueRunA();},
						test.GetLoopCount().GetRunsArgument(),
						test.GetLoopCount().GetCyclesArgument() );
}

//--------------------------------
/*
*/
void QueueVSqueue::RunqueueTests()
{
	QueueVSqueue test;
	TestUtils::testCode("STL queue Test",
						[&](){test.queueRunA();},
						test.GetLoopCount().GetRunsArgument(),
						test.GetLoopCount().GetCyclesArgument() );
}