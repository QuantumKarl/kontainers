/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_ARRAY_ALLOCATORS_HPP )
#define D_ARRAY_ALLOCATORS_HPP

//----------------------------------------------------------------
/* Array Allocators
	Some containers use these template functions to handle their
	allocations.
*/

//---------------------------------------------------------------
// Toggle C_STYLE (malloc) allocators else C++ style allocators
#define D_C_STYLE
//----------------------------------------------------------------

#if	!defined( D_C_STYLE )
	#include <new> // for placement new
#else
#endif

//--------------------------------
/* ArrayNew
	Creates a new array.
	
	C_Style : uses place new to init components.
	C++: calls new.
*/
template< typename _type_ >
D_INLINE
_type_*
ArrayNew
(
	size_t const i_num
)
{
#if	defined( D_C_STYLE )
	_type_* ptr = (_type_ *)Mem_Alloc( sizeof(_type_) * i_num );

	for ( size_t i = 0; i < i_num; ++i ) // TODO: make a specialsed function to avoid this for pod types, try duff's device here
	{
		::new ( &ptr[i] ) _type_;
	}
	return ptr;
#else
	return new _type_[i_num];
#endif
}

//--------------------------------
/* ArrayDelete
	Deletes an array.
	
	C_Style: explicitly calls the destructor.
	C++: calls delete[].
*/
template< typename _type_ >
D_INLINE
void
ArrayDelete
(
	_type_* i_ptr,
	size_t const i_num
)
{
#if	defined( D_C_STYLE )
	// Call the destructor on each element
	for ( size_t i = 0; i < i_num; ++i )
	{
		i_ptr[i].~_type_();
	}
	Mem_Free( i_ptr );
#else
	delete[] i_ptr;
#endif
}

//--------------------------------
/* ArrayResize
	Resizes an array.

	C_Style and C++ share the same logic.
*/
template< typename _type_ >
D_INLINE
_type_*
ArrayResize
(
	_type_* i_ptr,
	size_t curNum,
	size_t newNum
)
{
	if ( newNum > 0 )
	{
		_type_* newptr = ArrayNew<_type_>( newNum );

		size_t const overlap = Min( curNum, newNum );

		ArrayCopy<_type_>( newptr, i_ptr, overlap );

		ArrayDelete<_type_>( i_ptr, curNum );
		return newptr;
	}
	else
	{
		ArrayDelete<_type_>( i_ptr, curNum );
		return nullptr;
	}
}

//--------------------------------
/* ArrayCopy
	Copies entries from one array to another

	NOTE: its purpose is to allow us to specialse on pod types
	in order to use memcpy where possible

	TODO: test if these make a difference, try duffs device here
*/
template< typename type_t >
D_FORCE_INLINE
void
ArrayCopy
(
	type_t* o_des,
	type_t const* i_src,
	size_t const i_num
)
{
	for ( size_t i = 0; i < i_num; ++i )
	{
		o_des[i] = i_src[i];
	}
}

// With the MSVS2010 compiler:
// in debug mode the fact that some functions are requested to be inlined can cause
// main.obj : fatal error LNK1179: invalid or corrupt file: duplicate COMDAT '??$ArrayCopy@H@@YAXPEAHPEBH_K@Z'
// (even if inlining is disabled), also tried replacing the D_FORCE_INLINE with blanks so they are not requested to be
// inlined, stil didnt help. So we can not have these functions in debug mode
#if !(defined( D_DEBUG ) && defined( D_PC_WIN ))

// real_t specialisation
template< typename type_t >
D_FORCE_INLINE
void
ArrayCopy<>
(
	real_t* o_des,
	real_t const* i_src,
	size_t const i_num
)
{
	memcpy( o_des, i_src, sizeof(real_t) * i_num );
}

// size_t specialisation (also covers uint32 and unint64)
template< typename type_t >
D_FORCE_INLINE
void
ArrayCopy<>
(
	size_t* o_des,
	size_t const* i_src,
	size_t const i_num
)
{
	memcpy( o_des, i_src, sizeof(size_t) * i_num );
}

// bool specialisation
template< typename type_t >
D_FORCE_INLINE
void
ArrayCopy<>
(
	bool* o_des,
	bool const* i_src,
	size_t const i_num
)
{
	memcpy( o_des, i_src, sizeof(bool) * i_num );
}

// char specialisation
template< typename type_t >
D_FORCE_INLINE
void
ArrayCopy<>
(
	char* o_des,
	char const* i_src,
	size_t const i_num
)
{
	memcpy( o_des, i_src, sizeof(char) * i_num );
}

// byte specialisation (unsigned char)
template< typename type_t >
D_FORCE_INLINE
void
ArrayCopy<>
(
	byte* o_des,
	byte const* i_src,
	size_t const i_num
)
{
	memcpy( o_des, i_src, sizeof(byte) * i_num );
}

// int32 specialisation (unsigned char)
template< typename type_t >
D_FORCE_INLINE
void
ArrayCopy<>
(
	int32* o_des,
	int32 const* i_src,
	size_t const i_num
)
{
	memcpy( o_des, i_src, sizeof(int32) * i_num );
}

// int64 specialisation (unsigned char)
template< typename type_t >
D_FORCE_INLINE
void
ArrayCopy<>
(
	int64* o_des,
	int64 const* i_src,
	size_t const i_num
)
{
	memcpy( o_des, i_src, sizeof(int64) * i_num );
}

#endif

#endif