/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_USING_CSTRING_CMP )
// jazz for avoiding branches when converting + or - to 1 or -1

#define INT8_SIGN_BIT		7
#define INT16_SIGN_BIT		15
#define INT32_SIGN_BIT		31
#define INT64_SIGN_BIT		63

#define INT8_SIGN_MASK		( 1 << INT8_SIGN_BIT )
#define INT16_SIGN_MASK		( 1 << INT16_SIGN_BIT )
#define INT32_SIGN_MASK		( 1UL << INT32_SIGN_BIT )
#define INT64_SIGN_MASK		( 1ULL << INT64_SIGN_BIT )

// If this was ever compiled on a system that had 64 bit unsigned ints,
// it would fail.
compile_time_assert( sizeof( unsigned int ) == 4 );

#define OLD_INT32_SIGNBITSET(i)		(static_cast<const unsigned int>(i) >> INT32_SIGN_BIT)
#define OLD_INT32_SIGNBITNOTSET(i)	((~static_cast<const unsigned int>(i)) >> INT32_SIGN_BIT)

// Unfortunately, /analyze can't figure out that these always return
// either 0 or 1, so this extra wrapper is needed to avoid the static
// alaysis warning.

D_INLINE_EXTERN int INT32_SIGNBITSET( int i )
{
	int	r = OLD_INT32_SIGNBITSET( i );
	assert( r == 0 || r == 1 );
	return r;
}

D_INLINE_EXTERN int INT32_SIGNBITNOTSET( int i )
{
	int	r = OLD_INT32_SIGNBITNOTSET( i );
	assert( r == 0 || r == 1 );
	return r;
}
//----------------------------------------------------------------
#endif

//--------------------------------
/* memcpy( dest, src, count )
	Copy count bytes from src to dest
*/
D_FORCE_INLINE
void*
CString::_memcpy
(
	void* dest,
	void const* src,
	size_t count
)
{
	assert( dest != nullptr );
	assert( src != nullptr );
	assert( dest != nullptr );
	assert( count > 0 );

#ifdef D_MEMCPY_OVERLAP_CHECK
	if( IsBlocksOverlapping( dest, src, count ) == true )
	{
		assert( false );
	}
#endif

#ifdef D_AGNER_MEMCPY
	return A_memcpy( dest, src, count );
#else
	return ::memcpy( dest, src, count );
#endif
}

//--------------------------------
/* memmove( dest, src, count )
	Same as memcpy, but allows overlap between src and dest
*/
D_FORCE_INLINE
void*
CString::_memmove
(
	void* dest,
	void const* src,
	size_t count
)
{
	assert( dest != nullptr );
	assert( src != nullptr );
	assert( dest != src );
	assert( count > 0 );
#ifdef D_AGNER_MEMMOVE
	return A_memmove( dest, src, count );
#else
	return ::memmove( dest, src, count );
#endif
}

//--------------------------------
/* memset
	Set count bytes in dest to (char)c
*/
D_FORCE_INLINE
void*
CString::_memset
(
	void* dest,
	int c,
	size_t count
)
{
	assert( dest != 0 );
	assert( count > 0 );
#ifdef D_AGNER_MEMSET
	return A_memset( dest, c, count );
#else
	return ::memset( dest, c, count );
#endif
}

//--------------------------------
/* memcmp( buf1, buf2, num )
	Compares two blocks of memory
*/
D_FORCE_INLINE
int
CString::_memcmp
(
	void const* buf1,
	void const* buf2,
	size_t num
)
{
	assert( buf1 != nullptr );
	assert( buf2 != nullptr );
	assert( buf1 != buf2 );
	assert( num > 0 );
#ifdef D_AGNER_MEMCMP
	return A_memcmp( buf1, buf2, num );
#else
	return ::memcmp( buf1, buf2, num );
#endif
}

//--------------------------------
/* strcat( dest, src )
	Concatenate strings dest and src. Store result in dest
*/
D_FORCE_INLINE
char*
CString::_strcat
(
	char* dest,
	char const* src
)
{
	assert( dest != nullptr );
	assert( src != nullptr );
	assert( dest != src );
#ifdef D_AGNER_STRCAT
	return A_strcat( dest, src );
#else
	return ::strcat( dest, src );
#endif
}

//--------------------------------
/* strcpy( dest, src )
	Copy string src to dest
*/
D_FORCE_INLINE
char*
CString::_strcpy
(
	char* dest,
	char const* src
)
{
	assert( dest != nullptr );
	assert( src != nullptr );
	assert( dest != nullptr );
#ifdef D_AGNER_STRCPY
	return A_strcpy( dest, src );
#else
	return ::strcpy( dest, src );
#endif
}

//--------------------------------
/* strlen( cStr )
	Get length of zero-terminated string
*/
D_FORCE_INLINE
size_t
CString::_strlen
(
	char const* cStr
)
{
	assert( cStr != nullptr );
#ifdef D_AGNER_STRLEN
	return A_strlen( cStr );
#else
	return ::strlen( cStr );
#endif
}

//--------------------------------
/* strcmp( a, b )
	Compare strings. Case sensitive
*/
D_FORCE_INLINE
int
CString::_strcmp
(
	char const* a,
	char const* b
)
{
	assert( a != nullptr );
	assert( b != nullptr );
	assert( a != b );
#ifdef D_USING_CSTRING_CMP
#ifdef D_AGNER_STRCMP
	return A_strcmp( a, b );
#else
	return ::strcmp( a, b );
#endif
#else
	int c1, c2, d;

	do
	{
		c1 = *a++;
		c2 = *b++;

		d = c1 - c2;
		if ( d != 0 )
		{
			return ( INT32_SIGNBITNOTSET( d ) << 1 ) - 1;
			//return d > 0 ? CMP::STR_MORE ? CMP::STR_LESS;
		}
	}
	while( c1 );

	return 0;		// strings are equal
#endif
}

//--------------------------------
/* strncmp( a, b, len )
	Compare string, with max len cutoff. Case senitive
*/
int
CString::_strncmp
(
	char const* a,
	char const *b,
	size_t len
)
{
	assert( a != nullptr );
	assert( b != nullptr );
	assert( len > 0 );
#ifdef D_USING_CSTRING_CMP
	return ::strncmp( a, b, len );
#else
	int c1, c2, d;
	do
	{
		c1 = *a++;
		c2 = *b++;

		if ( !len-- )
		{
			return 0;		// strings are equal until end point
		}

		d = c1 - c2;
		if ( d )
		{
			return ( INT32_SIGNBITNOTSET( d ) << 1 ) - 1;
		}
	}
	while( c1 );
	return 0;		// strings are equal
#endif
}

//--------------------------------
/* stricmp( cStr1, cStr2 )
	Compare strings. Case insensitive for A-Z only
*/
D_FORCE_INLINE
int
CString::_stricmp
(
	char const* cStr1,
	char const* cStr2
)
{
	assert( cStr1 != nullptr );
	assert( cStr2 != nullptr );
	assert( cStr1 != cStr2 );
#ifdef D_USING_CSTRING_CMP
#ifdef D_AGNER_STRICMP
	return A_stricmp( cStr1, cStr2 );
#else
	return ::stricmp( cStr1, cStr2 );
#endif
#else
	int c1, c2, d;
	do
	{
		c1 = *cStr1++;
		c2 = *cStr2++;

		d = c1 - c2;
		while( d )
		{
			if ( c1 <= 'Z' && c1 >= 'A' )
			{
				d += ('a' - 'A');
				if ( !d )
				{
					break;
				}
			}
			if ( c2 <= 'Z' && c2 >= 'A' )
			{
				d -= ('a' - 'A');
				if ( !d )
				{
					break;
				}
			}
			return ( INT32_SIGNBITNOTSET( d ) << 1 ) - 1;
		}
	} while( c1 );

	return 0;		// strings are equal
#endif
}

//--------------------------------
/* strincmp( s1, s2, n )
	c string compare, insensitive and with length cutoff

	NOTE: there is no c string function for this, so its done in C++
	thus will be slower then other compares
*/
D_FORCE_INLINE
int
CString::_strincmp
(
	char const* s1,
	char const* s2,
	size_t n
)
{
	assert( s1 != nullptr );
	assert( s2 != nullptr );
	assert( n > 0 );

	int c1, c2, d;
	do
	{
		c1 = *s1++;
		c2 = *s2++;

		if ( !n-- )
		{
			return 0;		// strings are equal until end point
		}

		d = c1 - c2;
		while( d )
		{
			if ( c1 <= 'Z' && c1 >= 'A' )
			{
				d += ('a' - 'A');
				if ( !d )
				{
					break;
				}
			}
			if ( c2 <= 'Z' && c2 >= 'A' )
			{
				d -= ('a' - 'A');
				if ( !d )
				{
					break;
				}
			}

			if ( d < 0 )
			{
				return -1;
			}
			else if( d > 0 )
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}
	while( c1 );

	return 0;// strings are equal
}

//--------------------------------
/* strstr( cStr, cSubStr )
	Search for substring in string
*/
D_FORCE_INLINE
char const*
CString::_strstr
(
	char const* cStr,
	char const* cSubStr
)
{
	assert( cStr != nullptr );
	assert( cSubStr != nullptr );
	assert( cStr != cSubStr );
#ifdef D_AGNER_STRSTR
	return A_strstr( cStr, cSubStr );
#else
	return ::strstr( cStr, cSubStr );
#endif
}

//--------------------------------
/* strspn( cStr, cStrSet )
	Find span of characters that belong to set
*/
D_FORCE_INLINE
size_t
CString::_strspn
(
	char const* cStr,
	char const* cStrSet
)
{
	assert( cStr != nullptr );
	assert( cStrSet != nullptr );
	assert( cStr != cStrSet );
#ifdef D_AGNER_STRSPN
	return A_strspn( cStr, cStrSet );
#else
	return ::strspn( cStr, cStrSet );
#endif
}

//--------------------------------
/* strcspn( cSTr, cStrSet )
	Find span of characters that do NOT belong to set
*/
size_t
CString::_strcspn
(
	char const* cStr,
	char const* cStrSet
)
{
	assert( cStr != nullptr );
	assert( cStrSet != nullptr );
	assert( cStr != cStrSet );
#ifdef D_AGNER_STRCSPN
	return A_strcspn( cStr, cStrSet );
#else
	// Not implemented
	assert( false );
	return 0;
#endif
}

//================================================================================================================================
// char functions

/* char_printable( c )
	returns true if c is printable.
*/
D_FORCE_INLINE
bool
CString::char_printable
(
	char c
)
{
	return c >= 0x20 && c <= 0x7E; //-V112
}

//--------------------------------
/* char_lower( c )
	returns true is c is lowercase.
*/
D_FORCE_INLINE
bool
CString::char_lower
(
	char c
)
{
	// test for regular ascii
	return c >= 'a' && c <= 'z';
}

//--------------------------------
/* char_upper( c )
	returns true if c is uppercase.
*/
D_FORCE_INLINE
bool
CString::char_upper
(
	char c
)
{
	// test for regular ascii
	return c >= 'A' && c <= 'Z';
}

//--------------------------------
/* char_alpha( c )
	returns true if c is alpha
*/
D_FORCE_INLINE
bool
CString::char_alpha
(
	char c
)
{
	// test for regular ascii and western European high-ascii chars
	return ( c >= 'a' && c <= 'z' ) || ( c >= 'A' && c <= 'Z' );
}

//--------------------------------
/* char_numberic( c )
	returns true if c is a number.
*/
D_FORCE_INLINE
bool
CString::char_numeric
(
	char c
)
{
	return c >= '0' && c <= '9';
}

//--------------------------------
/* char_newline( c )
	returns true if c is a newline.
*/
D_FORCE_INLINE
bool
CString::char_newline
(
	char c
)
{
	return c == '\n' || c == '\r' || c == '\v';
}

//--------------------------------
/* char_tab( c )
	returns true if c is a tab.
*/
D_FORCE_INLINE
bool
CString::char_tab
(
	char c
)
{
	return c == '\t';
}

//================================================================================================================================
// common c string functions

//--------------------------------
/* numeric
	returns true if the string is a number.

	NOTE: allows floating point numbers. But not scientific notation.
*/
D_FORCE_INLINE
bool
CString::numeric
(
	char const* i_cStr
)
{
	assert( i_cStr != nullptr );
	size_t	i = 0;
	bool	dot = false;

	if ( i_cStr[0] == '-' || i_cStr[0] == '+' )
	{
		++i;
	}

	do
	{
		if ( char_numeric( i_cStr[i] ) == false )
		{
			if ( (i_cStr[i] == '.') && dot == false )
			{
				dot = true;
				continue;
			}
			return false;
		}
		++i;
	}
	while( i_cStr[i] != '\0' );

	return true;
}

//--------------------------------
/* contains_lower
	returns true if the string contains a lower case character.
*/
D_FORCE_INLINE
bool
CString::contains_lower
(
	char const* i_cStr
)
{
	assert( i_cStr != nullptr );

	size_t i = 0;
	do
	{
		if( char_lower( i_cStr[i] ) == true )
		{
			return true;
		}
		else
		{
			++i;
		}
	}
	while( i_cStr[i] != '\0' );

	// all upper case or not characters
	return false;
}

//--------------------------------
/* contains_upper
	returns true if the string contains a upper case character.
*/
D_FORCE_INLINE
bool
CString::contains_upper
(
	char const* i_cStr
)
{
	assert( i_cStr != nullptr );

	size_t i = 0;
	do
	{
		if( char_upper( i_cStr[i] ) == true )
		{
			return true;
		}
		else
		{
			++i;
		}
	}
	while( i_cStr[i] != '\0' );

	// all lower case or not characters
	return false;
}


//================================================================================================================================
// extra c string function

//--------------------------------
/* strtolower( cStr )
	Convert string to lower case for A-Z only
*/
D_FORCE_INLINE
void
CString::_strtolower
(
	char* io_cStr
)
{
	assert( io_cStr != nullptr );
#ifdef D_AGNER_STRTOLOWER
	A_strtolower( io_cStr );
#else
	size_t i = 0;
	do
	{
		if ( char_upper( io_cStr[i] ) == true )
		{
			io_cStr[i] += ( 'a' - 'A' );
		}
		++i;
	}
	while( io_cStr[i] != '\0' );
#endif
}

//--------------------------------
/* strtoupper( cStr )
	Convert string to upper case for a-z only
*/
D_FORCE_INLINE
void
CString::_strtoupper
(
	char* io_cStr
)
{
	assert( io_cStr != nullptr );
#ifdef D_AGNER_STRTOUPPER
	A_strtoupper( io_cStr );
#else
	// no accelerated version implemented
	size_t i = 0;
	do
	{
		if ( char_lower( io_cStr[i] ) )
		{
			io_cStr[i] -= ( 'a' - 'A' );
		}
		++i;
	}
	while( io_cStr[i] != '\0' );
#endif
}

//--------------------------------
/* insertsubstr( cStrDest, cStrSrc, pos, len )
	Copy a substring from source into dest
*/
D_FORCE_INLINE
size_t
CString::insertsubstr
(
	char* cStrDest,
	char const* cStrSrc,
	size_t pos,
	size_t len
)
{
	assert( cStrDest != nullptr );
	assert( cStrSrc != nullptr );
	assert( len > 0 );
#ifdef D_AGNER_INSERTSUBSTR
	return A_substring( cStrDest, cStrSrc, pos, len );
#else
	// no accelerated version implemented
	assert( false );
	return 0;
#endif
}

//--------------------------------
/* strcountinset( cStr, cStrSet )
	Count characters that belong to set
*/
D_FORCE_INLINE
size_t
CString::strcountinset
(
	char const* cStr,
	char const* cStrSet
)
{
	assert( cStr != nullptr );
	assert( cStrSet != nullptr );
	assert( cStr != cStrSet );
#ifdef D_AGNER_STRCOUNTINSET
	return ::strCountInSet( cStr, cStrSet );
#else
	// no accelerated version implemented
	assert( false );
	return 0;
#endif
}