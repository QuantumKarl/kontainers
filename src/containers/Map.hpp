/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_MAP_HPP )
#define D_MAP_HPP

// TODO: inspect for (even further) improvements, ie adding a occupied bool to the entry to avoid comparisons of keys (same applies to Set)

// Toggles tracking of the number of max probes taken to insert.
// Used for performance monitoring.
#if !defined( D_FINAL )
#define D_TRACK_PROBING
#endif

//----------------------------------------------------------------
/* Hash Map
	Uses linear probing and MurMur2 Hash.
	Does not support removing entries.

	NOTE: also called "Map", "HashTable", "Dictionary" etc
*/
template< typename key_t, typename value_t >
class Map
{
	#include "MapEntries.hpp"

	enum
	{
		default_capacity = 256
	};
public:
	Map( size_t const i_tableSize = default_capacity, real_t const i_resizeLoad = 0.4 );	// resizeLoad MUST be less than 0.6
	Map( Map const& i_other );

	~Map();

	// C
	void			clear( bool const i_dealloc = true );				// Removes all added elements, optionally deallocates memory

	// D
	void			delete_contents();									// Assumes the value_t of this container is a pointer and calls delete on it (will not compile if value_t is not a pointer)

	// E
	// Erase is not supported!

	// F
	bool			find( key_t const& i_key, value_t*& i_value );			// finds a value in the hash table
	bool			find( key_t const& i_key, value_t const*& i_value ) const;

	template< typename lambda > void for_all_elements( lambda i_functionToApply );	// Apply a lambda function to each element added into the table
	template <typename lambda > void for_all_elements( lambda const i_functionToApply ) const;

	// I
	bool			insert( key_t const& i_key, value_t const& i_value );	// inserts / adds a value to the hash table

	// R
	void			resize( size_t const& i_newSize );					// Resizes and re hashes the internal table, contents are preserved.
	void			reserve( size_t const& i_newSize );					// Requests a resize if necessary (to the appropriate size).

	// S
	size_t			size() const;										// Returns the number of added entries in the hash table

	// Misc functions
	size_t			_allocated() const;									// returns total size of allocated memory
	size_t			_size() const;										// returns total size of allocated memory including size of hashTable
	size_t			_memory_used() const;								// returns size of the used entries in the hashTable

	Map&	operator=( Map const& i_other );

#ifdef D_TRACK_PROBING
	size_t m_iNumProbes;
#endif

private:
	void			copy_from( Map const& i_other );	// copies other table to this
	void			Init();								// checks if m_array needs to be allocated

	typedef			MapEntry< key_t, value_t > Entry_t;	// An entry in the hash table

	real_t			m_resizeLoad;		// The percentage load at which the table resizes. Must be less than 0.6,
										//	the lower the value the better the performance will be. However, it will use up more memory / resize earlier.

	size_t			m_arraySize;		// The number of entries allocated on the end of m_array
	size_t			m_arrayMask;		// A prime number just under tableSize, used for hashing
	size_t			m_compressionMask;	// A number used to compress the hash to an index
	size_t			m_numEntries;		// The number of key and value entries in the table
	size_t			m_resizeThreshold;	// The number of entries required to force the table to resize (load factor threshold)

	Entry_t*		m_array;			// Pointer to an array of Entry_t
	
	// For comparison (to avoid temporaries)
	key_t const 	m_defaultKey;
	value_t const	m_defaultValue;
};

#include "Map.inl"

#endif // D_MAP_HPP