/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_ARRAY_HPP )
#define D_ARRAY_HPP

#include "memory\Allocation.hpp"
#include "algorithm\Sort.hpp"
#include "random\Random.hpp"

//----------------------------------------------------------------
/* Array is a replacement for a normal C array.

int		myArray[ARRAY_SIZE];

becomes:

Array<int,ARRAY_SIZE>	myArray;

Has no performance overhead in release builds, but
does index range checking in debug builds.

Unlike TempArray, the memory is allocated inline with the
object, rather than on the heap.

Unlike ListFixed, there are no fields other than the
actual raw data, and the size is fixed.

Iterating is fast since it is via index / pointer math.
Searching is slow unless binary search(on a sorted list) is used (see Sort.h)
*/
template< typename _type_, size_t m_num >
class Array
{
	compile_time_assert( D_POWER_OF_TWO( m_num ) );	// Use a power of two
public:
					Array();
					explicit Array( _type_ const& i_elementsValue );		// set all the elements to the same value on construction
					Array( _type_ const* i_start, _type_ const* i_end );	// copy from start to end into the list
					Array( Array const& other );							// copy an entire other list

					~Array();

	// B
	_type_ const&	back() const;											// returns a reference to the first element
	_type_*			begin();												// returns a pointer to the first element
	_type_ const*	begin() const;											// returns a pointer to the first element

	// 
	bool			contains( _type_ const& i_obj ) const;					// find the given element _SLOW_

	// D
	void			delete_contents( bool const i_andSetToNULL );			// delete the contents of the list

	// E
	_type_ const*	end() const;											// returns a pointer to list[m_num], do not dereference

	// F
	_type_*			find( _type_ const& i_obj ) const;							// find pointer to the given element _SLOW_
	bool			find_index( const _type_& i_obj, size_t& o_index ) const;	// find the index for the given element _SLOW_
	bool			find_null( size_t& o_index ) const;							// find the index for the first NULL pointer in the list _SLOW_
	_type_ const&	front() const;												// returns a reference to the list[0]

	// I
	size_t			index_of( _type_ const* i_obj ) const;						// returns the index for the pointer to an element in the list _RISKY_

	// M
	void			memory_set();												// calls memset on the list
	void			memory_set( size_t const i_value );							// calls memset on the list with value as their new value

	// S
	void			shuffle( Random::RNG& ran );								// apply a shuffle to the list
	size_t			size() const;												// returns number of elements in list
	void			sort( Algorithm::SortBase<_type_> const& i_sort = Algorithm::Sort_QuickDefault<_type_>() ); // sort the list

	// Misc functions
	size_t			_memory_used() const;										// returns size of the used elements in the list

	Array<_type_,m_num>&	operator=( Array<_type_,m_num>const& other ); // _SLOW_
	_type_ const&			operator[]( size_t index ) const;
	_type_&					operator[]( size_t index );

private:
	_type_		m_list[m_num];
};

#include "Array.inl"

#endif// end of D_ARRAY