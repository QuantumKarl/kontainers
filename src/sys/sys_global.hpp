/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_SYS_GLOBAL_H )
#define D_SYS_GLOBAL_H
//----------------------------------------------------------------
//					Global constants and functions
// NOTE: keep this down to simple types and defines. Do NOT add code.

//--------------------------------
//		Macro functions
#define D_CONST_ALIGN( x, a ) ( ( ( x ) + ((a)-1) ) & ~((a)-1) )
#define D_POWER_OF_TWO(n)		((n & (n-1)) == 0)

#define D_LL(x) x##ll
#define D_ULL(x) x##ull
// end of macro functions
//--------------------------------


//--------------------------------
//		Static constants

// These are to avoid using sizeof twice in one statement
const size_t WORD_SIZE = sizeof( word );
const size_t WORD_BITS = WORD_SIZE * 8;

// The alignment of heap allocations (in bytes)
const size_t c_Memory_Alignment = 16; // 64 bit

const uint64 uint64MAX = static_cast<uint64>(-1);
const uint32 uint32MAX = static_cast<uint32>(-1);
const size_t size_tMAX = static_cast<size_t>(-1);

// end of static constants
//--------------------------------


//--------------------------------
//		Static functions

//----------------
// Memory Alignment
D_INLINE
size_t
Align
(
	const size_t x,
	const size_t a = c_Memory_Alignment
)
{
	return ( ( x ) + (a-1) ) & ~(a-1);
}
//----------------

//----------------
// Min and Max
template<class T>
D_INLINE
T
Min
(
	T const& x,
	T const& y
)
{
	return x < y ? x : y;
}

template<class T>
D_INLINE
T
Max
(
	T const& x,
	T const& y
)
{
	return x > y ? x : y;
}

template<class T>
D_INLINE
T
Min
(
	T const& x,
	T const& y,
	T const& z
)
{
	return x < y ? ( x < z ? x : z ) : ( y < z ? y : z );
}

template<class T>
D_INLINE
T
Max
(
	T const& x,
	T const& y,
	T const& z
)
{
	return x > y ? ( x > z ? x : z ) : ( y > z ? y : z );
}
//----------------


//----------------
// Bits and Bytes related
template <class T>
D_INLINE T Crop(T value, size_t size)
{
	if (size < 8*sizeof(value))
		return T(value & ((T(1) << size) - 1));
	else
		return value;
}

template <class T1, class T2>
D_INLINE bool SafeConvert(T1 from, T2 &to)
{
	to = (T2)from;
	if (from != to || (from > 0) != (to > 0))
		return false;
	return true;
}

D_INLINE size_t BitsToBytes(size_t bitCount)
{
	return ((bitCount+7)/(8));
}

D_INLINE size_t BytesToWords(size_t byteCount)
{
	return ((byteCount+WORD_SIZE-1)/WORD_SIZE);
}

D_INLINE size_t BitsToWords(size_t bitCount)
{
	return ((bitCount+WORD_BITS-1)/(WORD_BITS));
}

D_INLINE size_t BitsToDwords(size_t bitCount)
{
	return ((bitCount+2*WORD_BITS-1)/(2*WORD_BITS));
}

// For shady bit twiddling
namespace
{
	class dBitField
	{
	public:
		unsigned int mantissal : 32;
		unsigned int mantissah : 20;
		unsigned int exponent : 11;
		unsigned int sign : 1;
	};

	union double_union
	{
		double d;
		dBitField f;
	};
}

// NOTE: Assuming 64-bit doubles and little-endian structure (not portable!)
// author Gerd Isenberg edited by Karl
// index = [0,63] of least significant one bit
// returns true if a bit is set
// return false if the value is zero (no ones to count)
D_FORCE_INLINE
static
bool
Bit_Scan_Forward
(
	size_t& index,
	uint64 const mask
)
{
	if( mask != 0 )
	{
		double_union ud;
		ud.d = (double)(mask & (0-mask) ); // isolated LS1B to double, ~mask
		index = ud.f.exponent - 1023;
		return true;
	}
	return false;
}

// NOTE: Assuming 64-bit doubles and little-endian structure (not portable!)
// author Gerd Isenberg edited by Karl
// index = [0,63] of most significant one bit
// returns true if a bit is set
// return false if the value is zero (no ones to count)
static
D_FORCE_INLINE
bool
Bit_Scan_Reverse
(
	size_t& index,
	uint64 const mask
)
{
	if( mask != 0 )
	{
		double_union ud;
		ud.d = (double)(mask & ~(mask >> 32)); //-V112 // avoid rounding error
		index = ud.f.exponent - 1023;
		return true;
	}
	return false;
}

// Use this if the above does not work
#if 0
const int index64[64] =
{
	0, 47,  1, 56, 48, 27,  2, 60,
	57, 49, 41, 37, 28, 16,  3, 61,
	54, 58, 35, 52, 50, 42, 21, 44,
	38, 32, 29, 23, 17, 11,  4, 62,
	46, 55, 26, 59, 40, 36, 15, 53,
	34, 51, 20, 43, 31, 22, 10, 45,
	25, 39, 14, 33, 19, 30,  9, 24,
	13, 18,  8, 12,  7,  6,  5, 63
};
 
/**
 * bitScanReverse
 * @authors Kim Walisch, Mark Dickinson
 * @param bb bitboard to scan
 * @precondition bb != 0
 * @return index (0..63) of most significant one bit
 */
int BitScanReverse(U64 bb)
{
	const U64 debruijn64 = C64(0x03f79d71b4cb0a89);
	assert (bb != 0);
	bb |= bb >> 1;
	bb |= bb >> 2;
	bb |= bb >> 4;
	bb |= bb >> 8;
	bb |= bb >> 16;
	bb |= bb >> 32;
	return index64[(bb * debruijn64) >> 58];
}
#endif
// end of Bits and Bytes related
//----------------


//----------------
// Powers and Multiples
template <class T>
D_INLINE bool IsPowerOf2(const T &n)
{
	return n > 0 && (n & (n-1)) == 0;
}

template <class T1, class T2>
D_INLINE T2 ModPowerOf2(const T1 &a, const T2 &b)
{
	assert( IsPowerOf2(b) );
	return T2(a) & (b-1);
}

template <class T1, class T2>
D_INLINE T1 RoundDownToMultipleOf(const T1 &n, const T2 &m)
{
	if (IsPowerOf2(m))
		return n - ModPowerOf2(n, m);
	else
		return n - n%m;
}

template <class T1, class T2>
D_INLINE T1 RoundUpToMultipleOf(const T1 &n, const T2 &m)
{
	if (n+m-1 > n)
	{
		return RoundDownToMultipleOf(n+m-1, m);
	}
	else // not possible
	{
		assert( false );
		//throw InvalidArgument("RoundUpToMultipleOf: integer overflow");
		return n;
	}
}

// Get the next power of two that is greater than value
D_FORCE_INLINE
uint64 PowerOf2GT( uint64 value )
{
	// Check if we need to do a bit scan
	if( IsPowerOf2( value ) == false )
	{
		size_t index = 0;
		if( Bit_Scan_Reverse( index, value ) == true )
		{
			// return next power of two (index+1)
			return static_cast<uint64>( 1 << (static_cast<uint64>(index+1)) ); //-V629
		}
		else// no bits set, next power of two is 2^0
		{
			return 1;
		}
	}
	else// already a power of two, so just times it by two
	{
		return value << 1;
	}
}

// Get the next power of two that is greater than or equal to value
D_FORCE_INLINE
uint64 PowerOf2GE( uint64 value )
{
	// Check if we need to do a bit scan
	if( IsPowerOf2( value ) == false )
	{
		size_t index = 0;
		if( Bit_Scan_Reverse( index, value ) == true )
		{
			// return next power of two (index+1)
			return static_cast<uint64>( 1 << (static_cast<uint64>(index+1)) ); //-V629
		}
		else// no bits set, next power of two is 2^0
		{
			return 1;
		}
	}
	else// already a power of two, so just keep it as it is
	{
		return value;
	}
}
// end of powers and multiples
//----------------

//----------------
// Conditional Swaps
template <class T>
D_INLINE void ConditionalSwap(bool c, T &a, T &b)
{
	T t = c * (a ^ b);
	a ^= t;
	b ^= t;
}

template <class T>
D_INLINE void ConditionalSwapPointers(bool c, T &a, T &b)
{
	ptrdiff_t t = c * (a - b);
	a -= t;
	b += t;
}

template< typename _type_ >
D_INLINE void SwapValues( _type_& a, _type_& b )
{
	_type_ c = a;
	a = b;
	b = c;
}
// end of conditional swaps
//----------------


//----------------
// Other
template< typename _type_ >
D_INLINE
bool
IsSignedType( const _type_ t )
{
	return _type_( -1 ) < 0;
}

// Returns the number of elements in between two pointers
template< typename _type_ >
D_INLINE
size_t // should return ptrdiff_t
ptr_range( _type_ ptrA, _type_ ptrB )
{
	ptrdiff_t diff = Max(ptrA,ptrB) - Min(ptrA,ptrB);
	// TODO: swap with abs

	// Some sort of validation
	assert( diff >= 0	&&
			diff < (1 << 19) ); //524,288, change it if you actually use more range. Just check its not an error

	return static_cast<size_t>(diff);
}

//----------------------------------------------------------------
// checks if two blocks of memory overlap
// Note: if the allocated objects arent aligned the compiler will
// pad them, which will cause this test to not be accurate since,
// the two objects are not overlapping in memory(but their padding could be).
static
D_INLINE
bool
IsBlocksOverlapping
(
	const void* a,
	const void* b,
	const size_t n
)
{
	const char* x = static_cast<const char*>(a);
	const char* y = static_cast<const char*>(b);
	return ( x <= y && x + n > y ) || ( y <= x && y + n > x );
}

// Convert value from one range of numbers to another range
template <typename T>
D_FORCE_INLINE
T
ConvertRange
(
	T const& originalStart,
	T const& originalEnd,
	T const& newStart,
	T const& newEnd,
	T const& value
)
{
	return static_cast<T>( newStart + ( (value - originalStart) * (static_cast<real_t>(newEnd - newStart) / static_cast<real_t>(originalEnd - originalStart))) );
}

// TODO: a logging / trace system( with different users )
//--------------------------------
// for debug, prints an array
template< typename T >
void PrintArray( char const* i_cStrName, T const* i_array, size_t const min, size_t const max )
{
	cout << i_cStrName << "\n";
	for(size_t i = min; i < max; ++i)
	{
		cout << "[" << i << "]" << " = " << i_array[i] << "\n";
	}
	cout << max << " = " << "array size";
	cout << endl;
}

//--------------------------------
// for debug, turns an array into a string
template< typename T >
string ArrayToString( T const* i_array, char i_preValue, char i_postValue, size_t min, size_t max )
{
	string toString;
	toString.reserve( static_cast<size_t>( double(max - min)*1.5 ) );
	for(size_t i = 0; i < max; ++i)
	{
		toString.push_back(i_preValue);
		toString << i_array[i];
		toString.push_back( i_postValue );
	}
	toString.shrink_to_fit();
	return toString;
}
// end of other
//----------------


// end of static functions
//--------------------------------

// end of global constants and functions
//----------------------------------------------------------------
#endif // end of D_SYS_GLOBAL_HPP