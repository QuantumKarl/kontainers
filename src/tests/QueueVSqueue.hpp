/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_QUEUE_VS_QUEUE )
#define D_QUEUE_VS_QUEUE

class QueueVSqueue
{
public:
	QueueVSqueue();

	static void RunQueueTests();
	static void RunqueueTests();

	LoopCount const& GetLoopCount() const { return m_loopCount; }
private:

	void QueueTestStrings( size_t const i_size );
	void QueueTestNumbers( size_t const i_size );

	void queueTestStrings( size_t const i_size );
	void queueTestNumbers( size_t const i_size );

	void QueueRunA();
	void queueRunA();

	LoopCount m_loopCount;
};

#include "QueueVSqueue.inl"
#endif