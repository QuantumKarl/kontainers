/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

//--------------------------------
/* split( i_token, o_output )
	splits the string into several sub strings on
	i_token.

	NOTE: this does not clear the output List. Therefore,
	you can reuse the same list to append strings from
	different split calls (could be calls to different string classes).
*/
D_FORCE_INLINE
bool
Str::split
(
	char const i_charTok,
	List<Str>& o_output
)
{
	assert( m_array != nullptr );
	assert( m_size > 0 );

	size_t startIndex	= 0;
	size_t endIndex		= 0;
	for( size_t i = 0; i < m_size; ++i )
	{
		if ( (i+1) <= m_size			&&
			m_array[i] == i_charTok		&&
			i > 0 )
		{
			startIndex	= endIndex;	// take the previous end index for our current start
			endIndex	= i;		// our end index is where the current token is
			i			= (i+1);	// move along to skip the token
			if( startIndex < endIndex )
			{
				o_output.push_back( Str( &m_array[startIndex], &m_array[endIndex] ) ); // C++11 &&
				endIndex = i;	// next time around when startIndex = endIndex we want to be after the token
			}
		}
	}

	// if we did find something, then the remaining string is to be added to the output List
	if( startIndex < endIndex )
	{
		o_output.push_back( Str( &m_array[endIndex], &m_array[m_size] ) ); // C++11 &&
		return true;
	}
	else
	{
		return false;
	}
}

//--------------------------------
/* split( i_strToken, o_output )
	splits the string into several sub strings on
	i_strToken.

	NOTE: this does not clear the output List. Therefore,
	you can reuse the same list to append strings from
	different split calls (could be calls to different string classes).
*/
D_FORCE_INLINE
bool
Str::split
(
	Str const& i_strTok,
	List<Str>& o_output
)
{
	assert( i_strTok.size() > 0 );
	assert( m_array != nullptr );
	assert( m_size > 0 );

	char const firstChar = i_strTok.front();
	size_t const tokSize = i_strTok.size();
	size_t startIndex	= 0;
	size_t endIndex		= 0;
	for( size_t i = 0; i < m_size; ++i )
	{
		if ( (i+tokSize) <= m_size		&&
			m_array[i] == firstChar		&&
			i > 0						&&
			strncmp( &m_array[i], i_strTok.begin(), tokSize ) == 0 )
		{
			startIndex	= endIndex;		// take the previous end index for our current start
			endIndex	= i;			// our end index is where the current token is
			i			= (i+tokSize);	// move along to skip the token
			if( startIndex < endIndex )
			{
				o_output.push_back( Str( &m_array[startIndex], &m_array[endIndex] ) ); // C++11 &&
				endIndex = i;	// next time around when startIndex = endIndex we want to be after the token
			}
		}
	}

	// if we did find something, then the remaining string is to be added to the output List
	if( startIndex < endIndex )
	{
		o_output.push_back( Str( &m_array[endIndex], &m_array[m_size] ) ); // C++11 &&
		return true;
	}
	else
	{
		return false;
	}
}

//--------------------------------
/* split( i_cStrTok, o_output )
	splits the string into several sub strings on
	i_cStrtoken.

	NOTE: this does not clear the output List. Therefore,
	you can reuse the same list to append strings from
	different split calls (could be calls to different string classes).
*/
D_FORCE_INLINE
bool
Str::split
(
	char const* i_cStrTok,
	List<Str>& o_output
)
{
	assert( i_cStrTok != nullptr );
	assert( m_array != nullptr );
	assert( m_size > 0 );

	char const firstChar = i_cStrTok[0];
	size_t const tokSize = strlen(i_cStrTok);
	size_t startIndex	= 0;
	size_t endIndex		= 0;
	for( size_t i = 0; i < m_size; ++i )
	{
		if ( (i+tokSize) <= m_size		&&
			m_array[i] == firstChar		&&
			i > 0						&&
			strncmp( &m_array[i], i_cStrTok, tokSize ) == 0 )
		{
			startIndex	= endIndex;		// take the previous end index for our current start
			endIndex	= i;			// our end index is where the current token is
			i			= (i+tokSize);	// move along to skip the token
			if( startIndex < endIndex )
			{
				o_output.push_back( Str( &m_array[startIndex], &m_array[endIndex] ) ); // C++11 &&
				endIndex = i;	// next time around when startIndex = endIndex we want to be after the token
			}
		}
	}

	// if we did find something, then the remaining string is to be added to the output List
	if( startIndex < endIndex )
	{
		o_output.push_back( Str( &m_array[endIndex], &m_array[m_size] ) ); // C++11 &&
		return true;
	}
	else
	{
		return false;
	}
}

//--------------------------------
/* to_lower
	converts all upper case characters to lower case.
*/
D_FORCE_INLINE
void
Str::to_lower
(
)
{
	strtolower( m_array );
}

//--------------------------------
/* to_upper
	converts all lower case characters to upper case.
*/
D_FORCE_INLINE
void
Str::to_upper
(
)
{
	strtoupper( m_array );
}

//--------------------------------
/* numeric
	returns true if the string is a number.

	NOTE: allows floating point numbers.
*/
D_FORCE_INLINE
bool
Str::numeric
(
) const
{
	return CString::numeric( m_array );
}

//--------------------------------
/* contains_lower
	returns true if the string contains a lower case character.
*/
D_FORCE_INLINE
bool
Str::contains_lower
(
) const
{
	return CString::contains_lower( m_array );
}

//--------------------------------
/* contains_upper
	returns true if the string contains a upper case character.
*/
D_FORCE_INLINE
bool
Str::contains_upper
(
) const
{
	return CString::contains_upper( m_array );
}

//--------------------------------
/* span( i_cStrSet )
	Returns the length of the initial portion of this Str which consists only of characters that are part of i_cStrSet.

	eg on the string "123abc44" if span was called with
	span( "0123456789" );
	would return 3. Since the first 3 letters are in the set (the string passed to span).
*/
D_FORCE_INLINE
size_t
Str::span
(
	char const* i_cStrSet
) const
{
	assert( m_array != nullptr );
	assert( m_size > 0 );
	return strspn( &m_array[0], i_cStrSet );
}

//--------------------------------
/* span( i_strSet )
	Returns the length of the initial portion of this Str which consists only of characters that are part of i_strSet.

	eg on the string "123abc44" if span was called with
	span( "0123456789" );
	would return 3. Since the first 3 letters are in the set (the string passed to span).
*/
D_FORCE_INLINE
size_t
Str::span
(
	Str const& i_strSet
) const
{
	assert( m_array != nullptr );
	assert( m_size > 0 );
	assert( i_strSet.empty() == false );
	return strspn( &m_array[0], i_strSet.c_str() );
}

//--------------------------------
/* hash
	Return the hash of the string
*/
D_FORCE_INLINE
size_t
Str::hash
(
	const size_t i_seed
) const
{
	return Hash::Murmur( &m_array[0], m_size, i_seed );
}

//--------------------------------
/* hash
	Return the hash of the string
*/
D_FORCE_INLINE
size_t
Str::hash
(
) const
{
	return Hash::Murmur( &m_array[0], m_size, Primes::findLE(m_size) );
}