/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */
#include "Random.hpp"
#include "..\sys\sys_asmlib.hpp"

#include <ctime>
namespace Random
{
	RNG& Random::Generator()
	{
#ifdef D_ASMLIB_HPP
		static RNG gen(static_cast<uint64>(time(nullptr)) ^ static_cast<uint64>(ReadTSC()) );
#else
		static RNG gen(time(nullptr) );
#endif
		return gen;
	}
}