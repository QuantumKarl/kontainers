/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

namespace
{
	class VecList
	{
	public:
		List<float> m_data;

		VecList operator+( VecList const& i_rhs )
		{
			size_t const count = Min( m_data.size(), i_rhs.m_data.size() );

			VecList res;
			res.m_data.reserve( count );

			for(size_t i = 0; i < count; ++i )
			{
				res.m_data.push_back( m_data[i] + i_rhs.m_data[i] );
			}
			return res;
		}
	};

	class Vecvector
	{
	public:
		vector<float> m_data;

		Vecvector operator+( Vecvector const& i_rhs )
		{
			size_t const count = Min( m_data.size(), i_rhs.m_data.size() );

			Vecvector res;
			res.m_data.reserve( count );

			for(size_t i = 0; i < count; ++i )
			{
				res.m_data.push_back( m_data[i] + i_rhs.m_data[i] );
			}
			return res;
		}
	};
}

//----------------------------------------------------------------
/* Init loop counters and other magic numbers
*/
ListVsVec::ListVsVec()
{
#if defined( D_FULL_TEST )
	m_loopCount.SetOuterLoopAtIndex(0,10); // RunA loop

	m_loopCount.SetArgumentAtIndex(0,1<<16); // Test1 arg1

	m_loopCount.SetArgumentAtIndex(1,1<<17); // Test2 arg1
	m_loopCount.SetArgumentAtIndex(2,512); // Test2 arg2
	m_loopCount.SetArgumentAtIndex(3,1024); // Test2 arg2
	m_loopCount.SetArgumentAtIndex(4,128); // Test2 arg2

	m_loopCount.SetCyclesAndRunsArgs(5,4);
	//m_loopCount.SetCyclesAndRunsArgs(1,1);
#else
	m_loopCount.SetOuterLoopAtIndex(0,1); //-V525 //-V112 // RunA loop

	m_loopCount.SetArgumentAtIndex(0,128); // Test1 arg1

	m_loopCount.SetArgumentAtIndex(1,32); //-V525 //-V112 // Test2 arg1
	m_loopCount.SetArgumentAtIndex(2,16); // Test2 arg2
	m_loopCount.SetArgumentAtIndex(3,16); //-V525 //-V112 // Test2 arg2
	m_loopCount.SetArgumentAtIndex(4,16); //-V112 Test2 arg2

	m_loopCount.SetCyclesAndRunsArgs(1,1);
#endif
}

//----------------------------------------------------------------
/* Karl List Test1
	General use, pod type
	largest single container will expand to approx 4 * elements

	NOTE: verified that it is in the final program
*/
void ListVsVec::ListTest1
(
	size_t const i_num
)
{
	assert( i_num % 2 == 0 );
	assert( i_num < Primes::NumPrimes() );

	List<int64> l1;
	List<int64> l2;
	List<int64> l3;
	List<int64> sums;
	// intensionally not reserving, to force container to resize

	// fill the lists ( will allocate memory )
	for(size_t i = 0; i < i_num ; ++i)
	{
		l1.push_back( i );
		l2.push_back( i * i / 2 + 1 );
		l3.push_back( Primes::PrimeAt( i ) );
	}

	// fill sums
	for(size_t i = 0; l1.empty() == false; ++i)
	{
		int64 value = 0;
		for( size_t j = 0, c = l1.size(); j < c; ++j )
		{
			value += l3[j] / l2[j] + l1[j];
		}
		l1.pop_back(); // contracting
		l2.pop_back();
		l3.pop_back();

		// calculated value
		sums.push_back( value ); // will allocate memory
	}

	// create more values from sums
	assert( sums.size() % 2 == 0 );
	for( size_t i = 0, c = sums.size(); i < c; i+=2 )
	{
		l1.push_back( sums[i] - sums[i+1] ); // will not allocate memory
		l2.push_back( sums[i] - l1.back() );
		l3.push_back( l1.back() + l2.back() );
	}

#if 0
	PrintArray( "sums", &sums[0], 0, sums.size() );
	PrintArray( "l1", &l1[0], 0, l1.size() );
	PrintArray( "l2", &l2[0], 0, l2.size() );
	PrintArray( "l3", &l3[0], 0, l3.size() );
#endif

	// clear l1 for next step
	l1.clear(false); // contract to zero, will not deallocate memory

	// finally collate all the values into a l1
	assert( l2.size() * 2 == sums.size() );
	for(size_t i = 0, c = l2.size(), j = 0; i < c; ++i, j +=2 )
	{
		l1.push_back( l2[i] ); // will allocate memory once l1.size more than l2.size
		l1.push_back( sums[j] );
		l1.push_back( l3[i] );
		l1.push_back( -sums[j+1] );
	}

#if 0
	size_t memUsed =
	l1._size() +
	l2._size() +
	l3._size() +
	sums._size();
	cout << "mem used: " << memUsed / 1024 << " KB" << endl;

	PrintArray( "l1", &l1[0], 0, l1.size() );
#endif
}

//----------------------------------------------------------------
/* STL vector Test1
	General use, pod type
	largest single container will expand to approx 4 * elements

	NOTE: 
*/
void ListVsVec::VecTest1
(
	size_t const i_num
)
{
	assert( i_num % 2 == 0 );
	assert( i_num < Primes::NumPrimes() );

	vector<int64> l1;
	vector<int64> l2;
	vector<int64> l3;
	vector<int64> sums;
	// intensionally not reserving, to force container to resize

	// fill the vectors ( will allocate memory )
	for(size_t i = 0; i < i_num ; ++i)
	{
		l1.push_back( i );
		l2.push_back( i * i / 2 + 1 );
		l3.push_back( Primes::PrimeAt( i ) );
	}

	// fill sums
	for(size_t i = 0; l1.empty() == false; ++i)
	{
		int64 value = 0;
		for( size_t j = 0, c = l1.size(); j < c; ++j )
		{
			value += l3[j] / l2[j] + l1[j];
		}
		l1.pop_back(); // contracting
		l2.pop_back();
		l3.pop_back();

		// calculated value
		sums.push_back( value ); // will allocate memory
	}

	// create more values from sums
	assert( sums.size() % 2 == 0 );
	for( size_t i = 0, c = sums.size(); i < c; i+=2 )
	{
		l1.push_back( sums[i] - sums[i+1] ); // will not allocate memory
		l2.push_back( sums[i] - l1.back() );
		l3.push_back( l1.back() + l2.back() );
	}

#if 0
	PrintArray( "sums", &sums[0], 0, sums.size() );
	PrintArray( "l1", &l1[0], 0, l1.size() );
	PrintArray( "l2", &l2[0], 0, l2.size() );
	PrintArray( "l3", &l3[0], 0, l3.size() );
#endif

	// clear l1 for next step
	l1.clear(); // contract to zero

	// finally collate all the values into a l1
	assert( l2.size() * 2 == sums.size() );
	for(size_t i = 0, c = l2.size(), j = 0; i < c; ++i, j +=2 )
	{
		l1.push_back( l2[i] ); // will allocate memory once l1.size more than l2.size
		l1.push_back( sums[j] );
		l1.push_back( l3[i] );
		l1.push_back( -sums[j+1] );
	}

#if 0
	PrintArray( "l1", &l1[0], 0, l1.size() );
#endif
}

//----------------------------------------------------------------
/* Karl List Test2
	General use, non pod type
	Lots of pushing and clearing, will cause alot of temps.
	Very fast, must pass a large number.

	NOTE: verified that it is in the final program
*/
void ListVsVec::ListTest2
(
	size_t const i_num,
	size_t const i_mul
)
{
	assert( i_num % 2 == 0 );
	assert( i_mul % 2 == 0 );
	assert( i_num > i_mul );
	assert( i_num < SourceData::randomFloats.size() );

	List<VecList> l1; // intensionally not pointers (would be better as pointers)
	List<VecList> l2;
	List<VecList> sums;
	// intensionally not reserving to force the container to resize

	// fill in multiples eg, 16,8,4,2
	for(size_t multipul = i_mul; multipul > 1; multipul /= 2 )
	{
		size_t ranIndex = 0;// start ran index again
		for(size_t i = 0; i < i_num; i += multipul)
		{
			l1.push_back( VecList() ); // copies by value, will cause allocations
			l2.push_back( VecList() );

			// fill with mul number of random floats
			for( size_t j = 0; j < multipul; ++j )
			{
				l1.back().m_data.push_back( SourceData::randomFloats[ranIndex++] ); // might cause allocations
				l2.back().m_data.push_back( SourceData::randomFloats[ranIndex++] );
			}
		}
	}

#if 0
	// print the sizes
	cout << l1.size() << endl;
	for( size_t i = 0, c = l1.size(); i < c ; ++i )
	{
		cout << l1[i].m_data.size() << endl;
	}
	/* you get n number of lists with size multiple
		where: i_mul * n = i_num
		ie if i_num = 8 and i_mul 8 was passed to this functions your sizes would be:
		8
		4
		4
		2
		2
		2
		2
		thus i_num / i_mul = number of lists of size i_mul then
		i_num / (i_mul / 2) = number of lists of size i_mul / 2 then
		i_num / (i_mul / 4) = number of lists of size i_mul / 4 etc
	*/
#endif

	// vector add into sums
	for( size_t i = 0, c = l1.size(); i < c ; ++i )
	{
		sums.push_back( l1[i] + l2[i] );// will cause allocations
	}

	// clear l1 and l2 in preparation to receive data
	l1.clear( false );
	l2.clear( false );

	// fold all sums into a single list
	assert( sums.size() % 2 == 0 );

	// setup l1
	for(size_t i = 0, c = sums.size(); i < c; i+=2 )
	{
		l1.push_back( sums[i] + sums[i+1] );// will cause allocations in the + operator
	}

	// dump some memory
	sums.clear( true );

	// make sure we have a power of 2, by expanding
	while( IsPowerOf2(l1.size()) == false )
	{
		size_t const count	= l1.back().m_data.size();
		size_t ranIndex		= l1.size();

		l1.push_back( VecList() );

		for( size_t i = 0; i < count; ++i )
		{
			l1.back().m_data.push_back( SourceData::randomFloats[ranIndex++] );
		}
	}

	// folding (ish)
	while( l1.size() != 1 )
	{
		// write to l2 from l1
		for(size_t i = 0, c = l1.size(); i < c; i+=2 )
		{
			l2.push_back( l1[i] + l1[i+1] );// only allocations in + operator
		}
		// clear l1 for writing to
		l1.clear( false );

		// write from l2 to l1
		for(size_t i = 0, c = l2.size(); i < c; i+=2 )
		{
			l1.push_back( l2[i] + l2[i+1] );// only allocations in + operator
		}

		// clear l2 for writing to
		l2.clear( false );
	}

#if 0
	cout << l1.front().m_data.front() << endl;
#endif
}

//----------------------------------------------------------------
/* STL vector Test2
	General use, non pod type
	Lots of pushing and clearing, will cause alot of temps.
	Very fast, must pass a large number.

	NOTE: verified that it is in the final program
*/
void ListVsVec::VecTest2
(
	size_t const i_num,
	size_t const i_mul
)
{
	assert( i_num % 2 == 0 );
	assert( i_mul % 2 == 0 );
	assert( i_num > i_mul );
	assert( i_num < SourceData::randomFloats.size() );

	vector<Vecvector> l1; // intensionally not pointers (would be better as pointers)
	vector<Vecvector> l2;
	vector<Vecvector> sums;
	// intensionally not reserving to force the container to resize

	// fill in multiples eg, 16,8,4,2
	for(size_t multipul = i_mul; multipul > 1; multipul /= 2 )
	{
		size_t ranIndex = 0;// start ran index again
		for(size_t i = 0; i < i_num; i += multipul)
		{
			l1.push_back( Vecvector() ); // copies by value, will cause allocations
			l2.push_back( Vecvector() );

			// fill with mul number of random floats
			for( size_t j = 0; j < multipul; ++j )
			{
				l1.back().m_data.push_back( SourceData::randomFloats[ranIndex++] ); // might cause allocations
				l2.back().m_data.push_back( SourceData::randomFloats[ranIndex++] );
			}
		}
	}

#if 0
	// print the sizes
	cout << l1.size() << endl;
	for( size_t i = 0, c = l1.size(); i < c ; ++i )
	{
		cout << l1[i].m_data.size() << endl;
	}
	/* you get n number of lists with size multiple
		where: i_mul * n = i_num
		ie if i_num = 8 and i_mul 8 was passed to this functions your sizes would be:
		8
		4
		4
		2
		2
		2
		2
		thus i_num / i_mul = number of lists of size i_mul then
		i_num / (i_mul / 2) = number of lists of size i_mul / 2 then
		i_num / (i_mul / 4) = number of lists of size i_mul / 4 etc
	*/
#endif

	// vector add into sums
	for( size_t i = 0, c = l1.size(); i < c ; ++i )
	{
		sums.push_back( l1[i] + l2[i] );// will cause allocations
	}

	// clear l1 and l2 in preparation to receive data
	l1.clear();
	l2.clear();

	// fold all sums into a single list
	assert( sums.size() % 2 == 0 );

	// setup l1
	for(size_t i = 0, c = sums.size(); i < c; i+=2 )
	{
		l1.push_back( sums[i] + sums[i+1] );// will cause allocations in the + operator
	}

	// dump some memory
	sums.clear();

	// make sure we have a power of 2, by expanding
	while( IsPowerOf2(l1.size()) == false )
	{
		size_t const count	= l1.back().m_data.size();
		size_t ranIndex		= l1.size();

		l1.push_back( Vecvector() );

		for( size_t i = 0; i < count; ++i )
		{
			l1.back().m_data.push_back( SourceData::randomFloats[ranIndex++] );
		}
	}

	// folding (ish)
	while( l1.size() != 1 )
	{
		// write to l2 from l1
		for(size_t i = 0, c = l1.size(); i < c; i+=2 )
		{
			l2.push_back( l1[i] + l1[i+1] );// only allocations in + operator
		}
		// clear l1 for writing to
		l1.clear();

		// write from l2 to l1
		for(size_t i = 0, c = l2.size(); i < c; i+=2 )
		{
			l1.push_back( l2[i] + l2[i+1] );// only allocations in + operator
		}

		// clear l2 for writing to
		l2.clear();
	}

#if 0
	cout << l1.front().m_data.front() << endl;
#endif
}

//----------------------------------------------------------------
/*
*/
void ListVsVec::ListRunA()
{
	ListTest1(m_loopCount.GetArgumentA()); // 8 seconds
	ListTest1(m_loopCount.GetArgumentA()); // 8 seconds
	ListTest1(m_loopCount.GetArgumentA()); // 8 seconds
	for(size_t i = 0; i < m_loopCount.GetOuterLoopA(); ++i )// 8 seconds
	{
		ListTest2(m_loopCount.GetArgumentB(),m_loopCount.GetArgumentC());
		ListTest2(m_loopCount.GetArgumentB(),m_loopCount.GetArgumentD());
		ListTest2(m_loopCount.GetArgumentB(),m_loopCount.GetArgumentE());
	}
}

//----------------------------------------------------------------
/*

*/
void ListVsVec::VecRunA()
{
	VecTest1(m_loopCount.GetArgumentA()); // 20 seconds
	VecTest1(m_loopCount.GetArgumentA()); // 20 seconds
	VecTest1(m_loopCount.GetArgumentA()); // 20 seconds
	for(size_t i = 0; i < m_loopCount.GetOuterLoopA(); ++i )// 8 seconds
	{
		VecTest2(m_loopCount.GetArgumentB(),m_loopCount.GetArgumentC());
		VecTest2(m_loopCount.GetArgumentB(),m_loopCount.GetArgumentD());
		VecTest2(m_loopCount.GetArgumentB(),m_loopCount.GetArgumentE());
	}
}

//----------------------------------------------------------------
/*
*/
void ListVsVec::RunListTests()
{
	ListVsVec listTests;
	TestUtils::testCode("Karl List Test",
						[&](){listTests.ListRunA();},
						listTests.GetLoopCount().GetRunsArgument(),
						listTests.GetLoopCount().GetCyclesArgument() );
}

//----------------------------------------------------------------
/*
*/
void ListVsVec::RunVecTests()
{
	ListVsVec vecTests;
	TestUtils::testCode("STL vector Test",
						[&](){vecTests.VecRunA();},
						vecTests.GetLoopCount().GetRunsArgument(),
						vecTests.GetLoopCount().GetCyclesArgument() );
}