/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>.

	INFO: This is the Prime Sieve from Kim Walisch see
	https://github.com/kimwalisch/primesieve
*/
#include "Primes.hpp"

#include "..\sys\sys_asmlib.hpp"
#include "..\algorithm\BinSearch.hpp"
#include <cmath>

//#define D_PRIMES_SIZE_DEBUG_OUTPUT

#if defined( D_PRIMES_SIZE_DEBUG_OUTPUT )
	#include <iostream>
#endif

namespace Primes
{
	List<int> m_primes;

	// Haskell source used to generate this magic number
	// let primes = 2 : [n | n<-[3,5..], all ((> 0).rem n) [3,5..floor.sqrt$fromIntegral n]]
	// length $ takeWhile (<value) primes
	D_FORCE_INLINE size_t Primes::EstimateNumberOfPrimesLessThan( const size_t value )
	{
		const float fPrimesAllocScala = 0.1f; // approx 10% of numbers are primes (depends on what range you are looking at, this is configured for the default init)
		return static_cast<size_t>( value * fPrimesAllocScala + 0.5f );
	}

	// Generate primes using the Segmented Sieve of Eratosthenes.
	// This algorithm uses O(n log log n) operations and O( sqrtSize(n) ) space.
	template<typename T>
	void Primes::segmented_sieve( const size_t limit, List<T>& outputPrimes )
	{
#if defined( D_ASMLIB_HPP )
		const size_t L1D_CACHE_SIZE = DataCacheSize(1);
#else
		const size_t L1D_CACHE_SIZE	= 1 << 15; //32,768
#endif

		if( limit >= 2 )
		{
			const size_t sqrtSize = static_cast<const size_t>( sqrt( static_cast<double>(limit) ) );

			// set the segment size
			const size_t segment_size = limit > L1D_CACHE_SIZE ? L1D_CACHE_SIZE : limit;

			//size_t iOutputIndex	= 0; // Needed for C style array
			size_t low			= 0;
			size_t high			= 0;
			size_t iSieveIndex	= 1;
			size_t s			= 2;
			size_t n			= 3;

			List<bool> sieve( segment_size, false );
			List<bool> is_prime( sqrtSize + 1, true );
			List<T> primes;
			List<T> next;

			// reserve some memory to help alleviate the resize cost (if any)
			primes.reserve( EstimateNumberOfPrimesLessThan( limit ) );
			next.reserve( EstimateNumberOfPrimesLessThan( limit ) );

			// make sure output is valid
			outputPrimes.clear(false); // clear but do not dealloc
			outputPrimes.push_back( static_cast<T>( s ) );

			// generate small primes <= sqrtSize
			for (size_t i = 2; i * i <= sqrtSize; ++i)
			{
				if (is_prime[i])
				{
					for (size_t j = i * i; j <= sqrtSize; j += i)
					{
						is_prime[j] = false;
					}
				}
			}

			// start sieving primes
			for (; low <= limit; low += segment_size)
			{
				// reset the sieve segment
				for(size_t k = 0, c = sieve.size(); k < c; ++k )
				{
					sieve[k] = true;
				}

				// current segment = interval [low, high]
				high = Min(low + segment_size - 1, limit);

				// store small primes needed to cross off multiples
				for (; s * s <= high; ++s)
				{
					if ( is_prime[s] == true )
					{
						primes.push_back( static_cast<T>( s ) );
						next.push_back( static_cast<T>( (s * s - low) ) );
					}
				}

				// sieve the current segment
				iSieveIndex = 1;
				for ( size_t size = primes.size(); iSieveIndex < size; ++iSieveIndex)
				{
					size_t j = next[ iSieveIndex ];
					for (size_t k = primes[ iSieveIndex ] * 2; j < segment_size; j += k)
					{
						sieve[j] = false;
					}
					next[iSieveIndex] = static_cast<T>( j - segment_size );
				}

				// output the primes generated from this loop
				for (; n <= high; n += 2)
				{
					if (sieve[n - low]) // n is a prime
					{
						//++iOutputIndex; // Needed for writing to C style array
						outputPrimes.push_back( static_cast<T>( n ) );
					}
				}
			}
		}
		else // no primes
		{
			outputPrimes.clear(true);
		}
	}

	size_t Primes::findGE( const size_t value )
	{
		return static_cast<size_t>( m_primes[Algorithm::BinSearch_GreaterEqual( m_primes.begin(), m_primes.size(), static_cast<int>(value) )] );
	}

	size_t Primes::findLE( const size_t value )
	{
		return static_cast<size_t>( m_primes[Algorithm::BinSearch_LessEqual( m_primes.begin(), m_primes.size(), static_cast<int>(value) )] );
	}

	size_t Primes::PrimeAt( const size_t index )
	{
		assert( index < m_primes.size() );
		return static_cast<size_t>( m_primes[ index ] );
	}

	size_t Primes::MaxPrime()
	{
		return static_cast<size_t>( m_primes.back() );
	}

	size_t Primes::NumPrimes()
	{
		return m_primes.size();
	}

	void Primes::InitStatic( const size_t primesLessThan /*= 1 << 16*/ )
	{
		// This function should not be called multiple times
		assert( m_primes.size() == 0 );

#if defined( PRIMES_SIZE_DEBUG_OUTPUT )
		std::cout << "Generating an excessive number of primes" << std::endl;
#endif

		m_primes.clear( false );
		m_primes.reserve( EstimateNumberOfPrimesLessThan( primesLessThan ) );
		segmented_sieve( primesLessThan, m_primes );
		m_primes.shrink_to_fit();

		// Debug output
		//--------------------------------
#if defined( PRIMES_SIZE_DEBUG_OUTPUT )
		std::cout << "Completed Generating primes" << std::endl;
		size_t approxSize = (m_primes.size() * sizeof( int )); // calculated size of the vector in memory
		const size_t range = 16; // number of primes to print per range

		std::cout << "Primes stored " << m_primes.size() << " Max Value " << primesLessThan << " Primes Ratio " << (float)m_primes.size() / (float)primesLessThan <<
					 "\nMemory Used " << approxSize << " bytes || " << (float)approxSize /1024.0f / 1024.0f << " MegaBytes" << std::endl;

		// first
		for(size_t i = 0; i < range; ++i )
		{
			std::cout << "\n" << m_primes[i];
		}
		std::cout << std::endl;

		// middle
		for(size_t i = m_primes.size() - m_primes.size() / 2; i < m_primes.size() / 2 + range; ++i )
		{
			std::cout << "\n" << m_primes[i];
		}
		std::cout << std::endl;

		// final
		for(size_t i = m_primes.size() - range; i < m_primes.size(); ++i )
		{
			std::cout << "\n" << m_primes[i];
		}
#endif
		//--------------------------------
	}

	void FiniStatic()
	{
		m_primes.clear(true);
	}

}// end of namespace