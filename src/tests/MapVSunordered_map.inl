#include <unordered_map>
using std::unordered_map;

//----------------------------------------------------------------
/* Init loop counters are other magic numbers
*/
MapVSunordered_map::MapVSunordered_map()
{
#if defined( D_FULL_TEST )
	m_loopCount.SetOuterLoopAtIndex(0,256); // RunA loop
	m_loopCount.SetArgumentAtIndex(0,SourceData::allWords.size());// StringToIndex test arg
	m_loopCount.SetArgumentAtIndex(1,Min(SourceData::allWords.size(), Primes::NumPrimes() ) ); // IndexToString test arg
	m_loopCount.SetCyclesAndRunsArgs(3, 4);
	//m_loopCount.SetCyclesAndRunsArgs(1, 1);
#else
	m_loopCount.SetOuterLoopAtIndex(0,1); // RunA loop
	m_loopCount.SetArgumentAtIndex(0,128);// StringToIndex test arg
	m_loopCount.SetArgumentAtIndex(1,128); // IndexToString test arg
	m_loopCount.SetCyclesAndRunsArgs(1, 1);
#endif
}

//----------------------------------------------------------------
/* Test Karl Map:: strings being mapped to size_t (indexes)
*/
void MapVSunordered_map::TestStringToIndex( size_t const size )
{
	assert( size <= SourceData::allWords.size() );

	size_t const count = size;
	Map<string, size_t> hashT( static_cast<size_t>(PowerOf2GE(static_cast<uint64>(count) * 2U)) );
	// intensionally do not give it the correct starting size

	// Add all the words (forwards)
	for(size_t i = 0; i < count; ++i )
	{
		if( hashT.insert( SourceData::allWords.at(i), i ) == true )
		{

		}
		else
		{
			cout << "FAILED TO INSERT KEY" << endl;
		}
	}

	// Check we have all the entries
	if( hashT.size() == count )
	{
	}
	else
	{
		cout << "FATAL ERROR MISSING WORDS " << endl;
		//return;
	}

	// Get all the words and check each value is correct (backwards)
	size_t j = 0;
	const size_t* value = nullptr;
	for(size_t i = 0; i < count; ++i )
	{
		j = count - i - 1;

		if( hashT.find( SourceData::allWords.at(j), value ) == true )
		{
			if( *value == j )
			{
				continue;
			}
			else // ERROR
			{
				cout << "ERROR MISS MATCHING VALUE FOR:: " << SourceData::allWords.at(j) << endl;
			}
		}
		else // ERROR
		{
			cout << "FATAL ERROR GETTING VALUE FOR:: " << SourceData::allWords.at(j) << endl;
			return;
		}
	}

	// Remove all the words
	hashT.clear(false);

	// Add all the words (backwards)
	for(size_t i = 0; i < count; ++i )
	{
		j = count - i - 1;
		hashT.insert( SourceData::allWords.at(j), i );
	}

	// Check we have all the entries
	if( hashT.size() == count )
	{
		//cout << hashT.m_iNumProbes << endl;
	}
	else
	{
		cout << "FATAL ERROR MISSING STRINGS" << endl;
		return;
	}
}

//----------------------------------------------------------------
/* Test STL unordered_map:: strings being mapped to size_t (indexes)
*/
void MapVSunordered_map::Test_unordered_map_StringToIndex( size_t const size )
{
	assert( size <= SourceData::allWords.size() );

	size_t const count = size;
	unordered_map<string, size_t> hashT( count / 2 );
	// intensionally do not give it the correct starting size

	// few type defs to make the code abit neater
	typedef std::pair<string, size_t> entry_t;
	typedef std::unordered_map<string,size_t>::const_iterator c_iter;

	// Add all the words (forwards)
	for(size_t i = 0; i < count; ++i )
	{
		if( hashT.insert( entry_t(SourceData::allWords.at(i), i) ).second == true )
		{

		}
		else
		{
			cout << "FAILED TO INSERT KEY" << endl;
		}
	}

	// Check we have all the entries
	if( hashT.size() == count )
	{
	}
	else
	{
		cout << "FATAL ERROR MISSING WORDS " << endl;
		//return;
	}

	// Get all the words and check each value is correct (backwards)
	size_t j = 0;
	c_iter findResult = hashT.end();
	for(size_t i = 0; i < count; ++i )
	{
		j = count - i - 1;
		findResult = hashT.find( SourceData::allWords.at(j) );
		if( findResult != hashT.end() )
		{
			if( findResult->second == j )
			{
				continue;
			}
			else // ERROR
			{
				cout << "ERROR MISS MATCHING VALUE FOR:: " << SourceData::allWords.at(j) << endl;
			}
		}
		else // ERROR
		{
			cout << "FATAL ERROR GETTING VALUE FOR:: " << SourceData::allWords.at(j) << endl;
			return;
		}
	}

	// Remove all the words
	hashT.clear();

	// Add all the words (backwards)
	for(size_t i = 0; i < count; ++i )
	{
		j = count - i - 1;
		hashT.insert( entry_t( SourceData::allWords.at(j), i) );
	}

	// Check we have all the entries
	if( hashT.size() == count )
	{
	}
	else
	{
		cout << "FATAL ERROR MISSING STRINGS" << endl;
		return;
	}
}

//----------------------------------------------------------------
/* Test Karl Map:: size_t being mapped to strings
*/
void MapVSunordered_map::TestIndexToString( size_t const size )
{
	assert( size <= Primes::NumPrimes() );
	assert( size <= SourceData::allWords.size() );

	size_t const count = size;

	Map<size_t, string> hashT( static_cast<size_t>( PowerOf2GE( static_cast<size_t>(count) * 2U) ) );
	// intensionally do not give it the correct starting size

	// Add all the primes
	for( size_t i = 0; i < count ; ++i )
	{
		// The XOR INT_MAX is to make sure that its not possible to
		// just use the key as a index( ie, to avoid defeating the point of the hash table )
		hashT.insert( (Primes::PrimeAt(i) ^ INT_MAX), SourceData::allWords.at(i) );
	}

	// Check we have all the entries
	if( hashT.size() == count )
	{
	}
	else
	{
		cout << "FATAL ERROR MISSING PRIMES" << endl;
		return;
	}

	// Get all the primes and check each value is correct (backwards)
	size_t j = 0;
	const string* value = nullptr;
	for(size_t i = 0; i < count; ++i )
	{
		j = count - i - 1;

		if( hashT.find( (Primes::PrimeAt(j) ^ INT_MAX), value ) == true )
		{
			if( *value == SourceData::allWords.at(j) )// expensive, will slow down the test
			{
				continue;
			}
			else // ERROR
			{
				cout << "ERROR MISS MATCHING VALUE FOR:: " << Primes::PrimeAt(j) << endl;
			}
		}
		else // ERROR
		{
			cout << "FATAL ERROR GETTING VALUE FOR:: " << Primes::PrimeAt(j) << endl;
			return;
		}
	}

	// Remove all the primes
	hashT.clear(false);

	// Add all the primes (backwards)
	for(size_t i = 0; i < count; ++i )
	{
		j = count - i - 1;
		hashT.insert( Primes::PrimeAt(j), SourceData::allWords.at(i) );
	}

	// Check we have all the entries
	if( hashT.size() == count )
	{
		//cout << hashT.m_iNumProbes << endl;
	}
	else
	{
		cout << "FATAL ERROR MISSING PRIMES" << endl;
		return;
	}
}

//----------------------------------------------------------------
/* Test STL unordered_map:: size_t being mapped to strings
*/
void MapVSunordered_map::Test_unordered_map_IndexToString( size_t const size )
{
	assert( size <= Primes::NumPrimes() );
	assert( size <= SourceData::allWords.size() );

	size_t const count = size;

	unordered_map<size_t, string> hashT(count / 2);
	// intensionally do not give it the correct starting size

	// few type defs to make the code abit neater
	typedef std::pair<size_t,string> entry_t;
	typedef std::unordered_map<size_t,string>::const_iterator c_iter;

	// Add all the primes
	for( size_t i = 0; i < count ; ++i )
	{
		// The XOR INT_MAX is to make sure that its not possible to
		// just use the key as a index( ie, to avoid defeating the point of the hash table )
		hashT.insert( entry_t( (Primes::PrimeAt(i) ^ INT_MAX), SourceData::allWords.at(i) ) );
	}

	// Check we have all the entries
	if( hashT.size() == count )
	{
	}
	else
	{
		cout << "FATAL ERROR MISSING PRIMES" << endl;
		return;
	}

	// Get all the primes and check each value is correct (backwards)
	size_t j = 0;
	c_iter findResult = hashT.end();
	for(size_t i = 0; i < count; ++i )
	{
		j = count - i - 1;

		findResult = hashT.find( (Primes::PrimeAt(j) ^ INT_MAX) );
		if( findResult != hashT.end() )
		{
			if( findResult->second == SourceData::allWords.at(j) )// expensive, will slow down the test
			{
				continue;
			}
			else // ERROR
			{
				cout << "ERROR MISS MATCHING VALUE FOR:: " << Primes::PrimeAt(j) << endl;
			}
		}
		else // ERROR
		{
			cout << "FATAL ERROR GETTING VALUE FOR:: " << Primes::PrimeAt(j) << endl;
			return;
		}
	}

	// Remove all the primes
	hashT.clear();

	// Add all the primes (backwards)
	for(size_t i = 0; i < count; ++i )
	{
		j = count - i - 1;
		hashT.insert( entry_t( Primes::PrimeAt(j), SourceData::allWords.at(i) ) );
	}

	// Check we have all the entries
	if( hashT.size() == count )
	{
	}
	else
	{
		cout << "FATAL ERROR MISSING PRIMES" << endl;
		return;
	}
}

//----------------------------------------------------------------
void MapVSunordered_map::MapRunA()
{
	for(size_t i = 0; i < m_loopCount.GetOuterLoopA(); ++i )
	{
		TestStringToIndex( m_loopCount.GetArgumentA() ); // nearly 1 second
		TestIndexToString( m_loopCount.GetArgumentB() ); // 0.5 seconds
	}
}

//----------------------------------------------------------------
void MapVSunordered_map::unordered_mapRunA()
{
	for(size_t i = 0; i < m_loopCount.GetOuterLoopA(); ++i )
	{
		Test_unordered_map_StringToIndex( m_loopCount.GetArgumentA() );
		Test_unordered_map_IndexToString( m_loopCount.GetArgumentB() );
	}
}

//----------------------------------------------------------------
void MapVSunordered_map::RunMapTests()
{
	MapVSunordered_map mapTest;
	TestUtils::testCode("Karl Map Test",
						[&](){mapTest.MapRunA();},
						mapTest.GetLoopCount().GetRunsArgument(),
						mapTest.GetLoopCount().GetCyclesArgument() );
}

//----------------------------------------------------------------
void MapVSunordered_map::Rununordered_mapTests()
{
	MapVSunordered_map mapTest;
	TestUtils::testCode("STL unordered_map Test",
						[&](){mapTest.unordered_mapRunA();},
						mapTest.GetLoopCount().GetRunsArgument(),
						mapTest.GetLoopCount().GetCyclesArgument() );
}