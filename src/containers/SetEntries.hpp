/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

// See Set for it in use
//--------------------------------
/* SetEntry
SetEntry is a templated class which allow the hash table to neatly call the correct functions
for any given templated key type.

As you can see this is only for keys, so you only need to create a new SetEntry if your using
key that is not currently defined.

Key values that are supported by default are as follows:

>> Any type that is 8 byte aligned (in 64 bit builds)
>> Any type that is 4 byte aligned (in 32 bit builds)
>> Strings
>> size_t
*/

//--------------------------------
// Defaulted template Hash Entry,
// Should cover most things that are not strings.
// if you key is small but uses this type consider making
// a specialization with a quicker hash to improve performance.
template< typename key_t>
class SetEntry
{
public:
	SetEntry():
		key()
	{}

	explicit SetEntry( const key_t& key ):
		key( key )
	{}

	static size_t
		GetHash( const key_t& key, const size_t tableSeed ) // tableSeed is the nearest prime number less than tableSize
	{
		// if you just got a compile time error here, you need to make a template. See others in "HashTableEntryTemplates.hpp"

#if defined( D_64BIT )
		// Assert that the key data type is a multiple of 8 bytes (64bit)
		assert_sizeof_8_byte_multiple( key )
#elif defined( D_32BIT )
		// Assert that the key data type is a multiple of 4 bytes (32bit)
		assert_sizeof_4_byte_multiple( key )
#endif

		return MurmurHash( &key, sizeof( key ), tableSeed );
	}

	D_INLINE static bool
		Equal( const key_t& key1, const key_t& key2 )
	{
		return key1 == key2;
	}

	key_t	key;
};

//--------------------------------
// string key type specialization,
// it has case insensitive compare
template<>
class SetEntry< string > // STL:: String
{
public:

	SetEntry():
		key()
	{}

	explicit SetEntry( const string& key ):
		key( key )
	{}

	static size_t
		GetHash( const string& key, const size_t tableSeed )
	{
		return Hash::Murmur(key.c_str(), key.size(), tableSeed);
	}

	D_INLINE static bool
		Equal( const string& key1, const string& key2 )
	{
		return strcmp( key1.c_str(), key2.c_str() ) == 0;
	}

public:
	string	key;
};

//--------------------------------
// Str key type specialization
template<>
class SetEntry< Str >
{
public:
	SetEntry():
		key()
	{}

	explicit SetEntry( const Str& key ):
		key( key )
	{}

	static size_t
		GetHash( const Str& key, const size_t tableSeed )
	{
		return key.hash( tableSeed );
	}

	D_INLINE static bool
		Equal( const Str& key1, const Str& key2 )
	{
		return key1 == key2;
	}

public:
	Str key;
};

//--------------------------------
// size_t key type specialization
template<>
class SetEntry< size_t >
{
public:
	SetEntry():
		key()
	{}

	explicit SetEntry( const size_t& key ):
		key( key )
	{}

	D_INLINE static size_t
		GetHash( const size_t& key, const size_t tableSeed )
	{
		return Hash::MurmurFinal( key ) & tableSeed;
	}

	D_INLINE static bool
		Equal( const size_t& key1, const size_t& key2 )
	{
		return key1 == key2;
	}

public:
	size_t	key;
};