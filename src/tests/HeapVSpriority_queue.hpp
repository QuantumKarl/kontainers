/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_HEAP_VS_PRIORITY_QUEUE_HPP )
#define D_HEAP_VS_PRIORITY_QUEUE_HPP

class HeapVSpriority_queue
{
public:
	HeapVSpriority_queue();

	static void RunHeapTests();
	static void Runpriority_queueTests();

	LoopCount const& GetLoopCount() const { return m_loopCount; }
private:

	void HeapRunA();
	void priority_queueRunA();

	// Karl containers
	void HeapNumbers( size_t const size );
	void HeapStrings( size_t const size );

	// test
	void HeapStringsPtr( size_t const size );

	// STL containers
	void priority_queueNumbers( size_t const size );
	void priority_queueStrings( size_t const size );

	LoopCount m_loopCount;
};

#include "HeapVSpriority_queue.inl"

#endif