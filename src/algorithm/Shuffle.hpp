/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_SHUFFLE_HPP )
#define D_SHUFFLE_HPP

namespace Algorithm
{
	//--------------------------------
	/* shuffle( i_array, i_size, i_ran )
		Simple Fisher Yates shuffle.
		i.e. a bit like shuffling a deck of cards
	*/
	template< typename T >
	void
	shuffle
	(
		T*				i_array,
		size_t const	i_size,
		Random::RNG&	i_ran
	)
	{
		// Swap each value with a random index,
		// each element will be swapped at least once
		for( size_t i = 0, index = 0; i < i_size; ++i )
		{
			// Get index to swap
			index = static_cast<size_t>( i_ran.Integer(0,i_size-1) );

			// Swap values
			if( index != i )
			{
				SwapValues( i_array[i], i_array[index] );
			}
		}
	}
}
#endif