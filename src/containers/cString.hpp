/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_CSTRING_HPP )
#define D_CSTRING_HPP

// if AGNER's asm lib is included
#if defined( D_ASMLIB_HPP )
//--------------------------------
// Select what AGNER functions to be used
// could be changed to pre-processor macros instead

#define D_AGNER_MEMCPY
#define D_AGNER_MEMMOVE
#define D_AGNER_MEMSET
#define D_AGNER_MEMCMP
#define D_AGNER_STRCAT
#define D_AGNER_STRCPY
#define D_AGNER_STRLEN
#define D_AGNER_STRCMP
#define D_AGNER_STRICMP
#define D_AGNER_STRSTR
#define D_AGNER_STRSPN

// extra functionality, may cause asserts when not defined
#define D_AGNER_STRCSPN
#define D_AGNER_STRTOLOWER
#define D_AGNER_STRTOUPPER
#define D_AGNER_INSERTSUBSTR
#define D_AGNER_STRCOUNTINSET

#endif

// when calling memory copy check if the two blocks of memory overlap
#if !defined( D_FINAL )
	#define D_MEMCPY_OVERLAP_CHECK
#endif

// if we are D_USING_CSTRING_CMP
// any c string implementations will be preferred over C++ ones
// eg strncmp over a loop performing the comparison
#define D_USING_CSTRING_CMP
// NOTE: this overloads AGNERS functions, so when this is NOT defined,
// AGNERS will not be used even if its available

//----------------------------------------------------------------
/*	CString::
	CString provides a way to overload the system c string functions,
	such as memcpy with Agner fogs hand coded SIMD functions.
	In addition, these overloads provide a means to try to catch errors
	by asserting before the c function is called.
*/
class CString
{
public:
	//-------------------------------- standard c string functions --------------------------------
	static void*		_memcpy(void* dest, void const* src, size_t count );		// Copy count bytes from src to dest
	static void*		_memmove(void* dest, void const* src, size_t count);		// Same as memcpy, allows overlap between src and dest
	static void*		_memset(void* dest, int c, size_t count);					// Set count bytes in dest to (char)c
	static int			_memcmp(void const* buf1, void const* buf2, size_t num);	// Compares two blocks of memory

	static char*		_strcat(char * dest, char const* src);					// Concatenate strings dest and src. Store result in dest
	static char*		_strcpy(char* dest, const char * src);					// Copy string src to dest
	static size_t		_strlen(char const* cStr);								// Get length of zero-terminated string
	static int			_strcmp(char const* a, char const* b);					// Compare strings. Case sensitive
	static int			_strncmp( char const* a, char const *b, size_t len );	// Compare string, with max len cutoff. Case sensitive
	static int			_stricmp(char const* cStr1, char const* cStr2);			// Compare strings. Case insensitive for A-Z only
	static int			_strincmp( char const* a, char const *b, size_t len );	// Compare string, with max len cutoff. Case insensitive. NOTE: no c string or AGNER version
	static char const*	_strstr(char const* cStr, char const* cSubStr);			// Search for substring in string
	static size_t		_strspn(char const* cStr, char const* cStrSet);			// Find span of characters that belong to set

	//-------------------------------- char functions --------------------------------
	static bool			char_printable( char c );
	static bool			char_lower( char c );
	static bool			char_upper( char c );
	static bool			char_alpha( char c );
	static bool			char_numeric( char c );
	static bool			char_newline( char c );
	static bool			char_tab( char c );

	//-------------------------------- common c string functions --------------------------------
	static bool			numeric( char const* i_cStr );
	static bool			contains_lower( char const* i_cStr );
	static bool			contains_upper( char const* i_cStr );

	//-------------------------------- non standard c functions provided by AGNER --------------------------------
	static size_t		_strcspn(char const* cStr, char const* cStrSet);			// Find span of characters that do NOT belong to set
	static void			_strtolower(char* cStr);									// Convert string to lower case for A-Z only
	static void			_strtoupper(char* cStr);									// Convert string to upper case for a-z only

	// These functions do not have overloads they have to be manually accessed, since they do NOT have accelerated replacements
	static size_t		insertsubstr(char* cStringDest, char const* cStrSrc, size_t pos, size_t len);	// Copy a substring from source into dest
	static size_t		strcountinset(const char * str, const char * set);		// Count characters that belong to set
};

#include "cString.inl"

//----------------------------------------------------------------
// use #defines to force users to use these functions

#define memcpy		CString::_memcpy
#define memmove		CString::_memmove
#define memset		CString::_memset
#define memcmp		CString::_memcmp
#define strcat		CString::_strcat
#define strcpy		CString::_strcpy
#define strlen		CString::_strlen
#define strcmp		CString::_strcmp
#define strncmp		CString::_strncmp
#define stricmp		CString::_stricmp
#define strincmp	CString::_strincmp
#define strstr		CString::_strstr
#define strspn		CString::_strspn
#define strcspn		CString::_strcspn
#define strtolower	CString::_strtolower
#define strtoupper	CString::_strtoupper

#endif // end of D_CSTRING_HPP