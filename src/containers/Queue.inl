/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

//--------------------------------
/* Queue
	default ctor.
	i_capacity is the initial alloc size
*/
template <typename type_t, bool overWrite >
D_FORCE_INLINE
Queue<type_t,overWrite>::Queue
(
):
	m_array(nullptr),
	m_capacity(0),
	m_size(0),
	m_min_index(1),
	m_max_index(0)
{
	Init();
}

//--------------------------------
/* Queue( i_other )
	copy ctor.
*/
template <typename type_t, bool overWrite >
D_FORCE_INLINE
Queue<type_t,overWrite>::Queue
(
	Queue const& i_other
):
	m_array(nullptr),
	m_capacity(i_other.m_capacity),
	m_size(i_other.m_size),
	m_min_index(1),
	m_max_index(0)
{
	*this = i_other;
}

//--------------------------------
/* Queue( i_start, i_end )
	iterator copy ctor.
*/
template <typename type_t, bool overWrite >
Queue<type_t,overWrite>::Queue
(
	type_t const* i_start,
	type_t const* i_end
):
	m_array(nullptr),
	m_capacity(0),
	m_size(0),
	m_min_index(1),
	m_max_index(0)
{
	assert( i_start != nullptr	&&
			i_end != nullptr );

	size_t const count = ptr_range( i_start, i_end );

	// Alloc memory
	reserve( count );

	// Set the new size
	m_size = count;

	// Add each element to our list
	for(size_t i = 0; i < count; ++i )
	{
		m_array[i] = i_start[i];
	}

	// Set min and max index
	m_min_index = 0;
	m_max_index = m_size-1;
}

//--------------------------------
/* ~Queue
	dtor.
*/
template <typename type_t, bool overWrite >
D_FORCE_INLINE
Queue<type_t,overWrite>::~Queue
(
)
{
	clear( true );
}

//--------------------------------
/* back
	accesses the last element.
*/
template <typename type_t, bool overWrite >
D_FORCE_INLINE
type_t&
Queue<type_t,overWrite>::back
(
)
{
	assert(m_array != nullptr);
	assert(m_size > 0);
	return m_array[m_max_index];
}

//--------------------------------
/* back
	accesses the last element (const version).
*/
template <typename type_t, bool overWrite >
D_FORCE_INLINE
type_t const&
Queue<type_t,overWrite>::back
(
) const
{
	assert(m_array != nullptr);
	assert(m_size > 0);
	return m_array[m_max_index];
}

//--------------------------------
/* capacity
	returns the number of elements in the container
	could accommodate without resizing.

	I.E. The number of elements it has space allocated for.
*/
template <typename type_t, bool overWrite >
D_FORCE_INLINE
size_t
Queue<type_t,overWrite>::capacity
(
) const
{
	return m_capacity;
}

//--------------------------------
/* clear
	removes all the elements in the list.
*/
template <typename type_t, bool overWrite >
D_FORCE_INLINE
void
Queue<type_t,overWrite>::clear
(
	bool const i_deallocate
)
{
	if( i_deallocate == true )
	{
		if ( m_array != nullptr )
		{
			ArrayDelete< type_t >(m_array, m_capacity);
			m_array	= nullptr;
		}
		m_capacity	= 0;
	}

	m_size = 0;
	m_min_index = 1;
	m_max_index = 0;
}

//--------------------------------
/* emplace_back
	creates an entry at the back of the queue,
	and returns a reference to it.
*/
template <typename type_t, bool overWrite >
D_INLINE
type_t&
Queue<type_t,overWrite>::emplace_back
(
)
{
	size_t const next_max = (m_max_index+1) < m_capacity ? (m_max_index+1) : 0;
	if( full() == false )
	{
		m_max_index = next_max;
		++m_size;
		return m_array[next_max];
	}
// disable warning 4127: "conditional expression is constant"
#pragma warning( disable : 4127 )
	else if (overWrite == true)
	{
		m_max_index = next_max;

		// the max has overwritten the min so increment min
		m_min_index = (m_min_index+1) < m_capacity ? (m_min_index+1) : 0;
		return m_array[next_max];
	}
#pragma warning( default : 4127 )
	else// expand
	{
		reallocate( ResizePolicy<type_t>::GetNextSize(m_capacity) );

		// we do not want the next_max since it will be 0
		++m_max_index;
		++m_size;
		return m_array[m_max_index];
	}
}

//--------------------------------
/* empty
	returns true if they are no elements in the container,
	else it returns false
*/
template <typename type_t, bool overWrite >
D_FORCE_INLINE
bool
Queue<type_t,overWrite>::empty
(
) const
{
	return m_size == 0;
}

//--------------------------------
/* front
	accesses the first element.
*/
template <typename type_t, bool overWrite >
D_FORCE_INLINE
type_t&
Queue<type_t,overWrite>::front
(
)
{
	assert(m_array != nullptr);
	assert(m_size > 0);
	return m_array[m_min_index];
}

//--------------------------------
/* front
	accesses the first element (const version).
*/
template <typename type_t, bool overWrite >
D_FORCE_INLINE
type_t const&
Queue<type_t,overWrite>::front
(
) const
{
	assert(m_array != nullptr);
	assert(m_size > 0);
	return m_array[m_min_index];
}

//--------------------------------
/* full
	returns true if the container is at max capacity
	else it returns false

	NOTE: if the container is full, pushing one more
	element will either cause a resize (overWrite == false)
	or it to overwrite the first entry (overWrite == true).
*/
template <typename type_t, bool overWrite >
D_FORCE_INLINE
bool
Queue<type_t,overWrite>::full
(
) const
{
	return (m_size+1) == m_capacity;
}

//--------------------------------
/* push_back( i_value )
	pushes i_value to the back of the container.
	this increases size by one.
*/
template <typename type_t, bool overWrite >
D_INLINE
void
Queue<type_t,overWrite>::push_back
(
	type_t const& i_value
)
{
// disable warning 4127: "conditional expression is constant"
#pragma warning( disable : 4127 )
	if (overWrite == true)
	{
		size_t const next_max = (m_max_index+1) < m_capacity ? (m_max_index+1) : 0;
		if( full() == false )
		{
			m_array[next_max]	= i_value;
			m_max_index			= next_max;
			++m_size;
		}
		else// wrap around
		{
			m_array[next_max]	= i_value;
			m_max_index			= next_max;

			// the max has overwritten the min so increment min
			m_min_index = (m_min_index+1) < m_capacity ? (m_min_index+1) : 0;
		}
	}
#pragma warning( default : 4127 )
	else// expanding
	{
		// if there is space to insert i_value some where
		if( m_size+1 < m_capacity )
		{
			size_t const next_max = (m_max_index+1) < m_capacity ? (m_max_index+1) : 0;
			m_array[next_max]	= i_value;
			m_max_index			= next_max;
			++m_size;
		}
		else // expand
		{
			// expand normally (new entries on the end)
			if( m_min_index < m_max_index )
			{
				reallocate( ResizePolicy<type_t>::GetNextSize(m_capacity) );

				m_array[m_size]	= i_value;
				m_max_index		= m_size;
				++m_size;
			}
			else// ( m_min_index > m_max_index )
			{
				// the data layout is like so:
				//	0 to max_index contains data,
				//	max_index to min_index is empty,
				//	min_index to size contains data

				// Therefore the normal reallocate will not do:
				// we could keep the data is the same pattern (with the gap in the middle)
				// but since making the data continuous again (min_index < max_index) is the same number of copies,
				// it is the better choice.

				// calc new size
				size_t const newCapacity = ResizePolicy<type_t>::GetNextSize(m_capacity);

				// create new array for data
				type_t* newArray = ArrayNew<type_t>(newCapacity);
				size_t const minToSize = m_size+1 - m_min_index;

				// assert we are copying m_sizse number of entries
				assert( minToSize + m_max_index +1== m_size );

				// copy from min_index to size
				ArrayCopy<type_t>(&newArray[0], &m_array[m_min_index], minToSize );

				// copy from 0 to max index
				ArrayCopy<type_t>(&newArray[minToSize],&m_array[0], m_max_index+1 );

				// remove the old array
				ArrayDelete<type_t>(m_array,m_size);

				// assign new array
				m_capacity	= newCapacity;
				m_array		= newArray;
				m_min_index = 0;
				m_max_index = m_size;
	
				// finally add the new data
				m_array[m_size] = i_value;
				++m_size;
			}
		}
	}
}

//--------------------------------
/* pop_front
	pops the element at the front of the container.
	this decreases size by one.
*/
template <typename type_t, bool overWrite >
D_FORCE_INLINE
void
Queue<type_t,overWrite>::pop_front
(
)
{
	assert(m_array != nullptr);
	assert(m_size > 0);

	m_min_index = (m_min_index+1) < m_capacity ? (m_min_index+1) : 0;
	--m_size;
}

//--------------------------------
/* reserve( i_newSize )
	ensures there is at least enough allocated to
	accommodate i_newSize
*/
template <typename type_t, bool overWrite >
D_INLINE
void
Queue<type_t,overWrite>::reserve
(
	size_t const i_newMinSize
)
{
	// if needs to expand
	if( i_newMinSize > m_capacity )
	{
		// get next size, nearest power of two
		size_t const nextSize = ResizePolicy<type_t>::Reserve( i_newMinSize );
		reallocate( nextSize );
	}
	// else big enough already
}

//--------------------------------
/* reallocate( i_newSize )
	Allocates memory for the amount of elements requested while keeping the contents intact.
	Contents are copied using their = operator so that data is correctly instantiated.

	eg if you know you are not going to use more that 256 elements,
	it would be efficient to call reallocate( 256 ) before adding any elements.
	However, there is the static sized container you may want to consider.

	NOTE: usually used internally in this class, use it with care.
*/
template <typename type_t, bool overWrite >
D_FORCE_INLINE
void
Queue<type_t,overWrite>::reallocate
(
	size_t const i_newCapacity
)
{
	// if expanding
	if ( i_newCapacity > m_capacity )
	{
		m_array = ArrayResize<type_t>( m_array, m_capacity, i_newCapacity );
		m_capacity = i_newCapacity;
	}
	// free up the m_array if no data is being reserved
	else if( i_newCapacity == 0 )
	{
		clear( true );
	}
	// else not changing the size
}

//--------------------------------
/* size
	returns the number of elements in the container.

	I.E. the number of successful pushes - the number of
	successful pops
*/
template <typename type_t, bool overWrite >
D_FORCE_INLINE
size_t
Queue<type_t,overWrite>::size
(
) const
{
	return m_size;
}

//--------------------------------
/* operator =
	Copies one List to another (deep copy).

	NOTE: avoid if you can
*/
template <typename type_t, bool overWrite >
Queue<type_t,overWrite>&
Queue<type_t,overWrite>::operator=
(
	Queue<type_t,overWrite> const& i_other
)
{
	clear( true );

	m_size		= i_other.m_size;
	m_capacity	= i_other.m_capacity;
	m_min_index = i_other.m_min_index;
	m_max_index = i_other.m_max_index;

	if( m_capacity > 0 )
	{
		m_array = ArrayNew< type_t >( m_capacity );

		for( size_t i = 0; i < m_size; ++i )
		{
			m_array[ i ] = i_other.m_array[ i ];
		}
	}

	return *this;
}

//--------------------------------
/* _allocated
	Returns the total memory allocated for the array in bytes,
	but doesn't take into account additional memory allocated by type_t.

	ie allocated size * element size
*/
template <typename type_t, bool overWrite >
size_t
Queue<type_t,overWrite>::_allocated
(
) const
{
	return m_capacity * sizeof( type_t );
}

//--------------------------------
/* _size
	Returns total size of the list in bytes,
	but doesn't take into account additional memory allocated by type_t

	ie class size + allocation size
*/
template <typename type_t, bool overWrite >
size_t
Queue<type_t,overWrite>::_size
(
) const
{
	return sizeof( Queue< type_t > ) + _allocated();
}

//--------------------------------
/* _memory_used
	Returns size of the used elements in the list.

	ie element size * num used elements
*/
template <typename type_t, bool overWrite >
size_t
Queue<type_t,overWrite>::_memory_used
(
) const
{
	return m_size * sizeof( type_t );
}

//--------------------------------
/* Init
	Checks if m_array needs to be allocated, and allocates it if necessary.
*/
template <typename type_t, bool overWrite >
D_FORCE_INLINE
void
Queue<type_t,overWrite>::Init
(
)
{
	if ( m_array == nullptr )
	{
		assert( m_size == 0 );
		reallocate( ResizePolicy<type_t>::GetNextSize(m_size) );
	}
}