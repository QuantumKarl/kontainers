/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

//--------------------------------
/* List()
	default ctor, minimal cost
*/
template< typename type_t>
D_FORCE_INLINE
List<type_t>::List
(
):
	m_array( nullptr ),
	m_size( 0 ),
	m_capacity( 0 )
{
	Init();
}

//--------------------------------
/* List( newListSize, elements )
	ctor, creates the list of size newListSize and sets their value to elementsValue
*/
template< typename type_t>
D_INLINE
List<type_t>::List
(
	size_t const i_newListSize,
	type_t const& i_elementsValue
):
	m_array( nullptr ),
	m_size( 0 ),
	m_capacity( 0 )
{
	assert( i_newListSize > 0 );

	// Alloc memory
	reserve( i_newListSize );

	// Set the new size
	m_size = i_newListSize;

	// Set values
	for( size_t i = 0; i < i_newListSize; ++i )
	{
		m_array[i] = i_elementsValue;
	}
}

//--------------------------------
/* List( start, end )
	ctor, iterate style init. copies elements from start to end into the list
*/
template< typename type_t>
D_INLINE
List<type_t>::List
(
	type_t const* i_start,
	type_t const* i_end
):
	m_array( nullptr ),
	m_size( 0 ),
	m_capacity( 0 )
{
	assert( i_start != nullptr	&&
			i_end != nullptr );

	size_t const count = ptr_range( i_start, i_end );

	// Alloc memory
	reserve( count );

	// Set the new size
	m_size = count;

	// Add each element to our list
	ArrayCopy(m_array,i_start,count);
}

//--------------------------------
/* List( other )
	ctor, copy constructor
*/
template< typename type_t >
D_FORCE_INLINE
List<type_t>::List
(
	List const& other
):
	m_array( nullptr ),
	m_size( 0 ),
	m_capacity( 0 )
{
	*this = other;
}

//--------------------------------
/* ~List
	dtor, clean up
*/
template< typename type_t >
D_FORCE_INLINE
List<type_t>::~List
(
)
{
	clear( true );
}

//--------------------------------
/* back
	return a const reference the to element at the back of the list,
	this is the most recently push back element or the last iteration in a loop
*/
template< typename type_t >
D_FORCE_INLINE
type_t const&
List<type_t>::back
(
) const
{
	assert(m_array != nullptr);
	return m_array[m_size-1];
}

//--------------------------------
/* back
	return a const reference the to element at the back of the list,
	this is the most recently push back element or the last iteration in a loop
*/
template< typename type_t >
D_FORCE_INLINE
type_t&
List<type_t>::back
(
)
{
	assert(m_array != nullptr);
	return m_array[m_size-1];
}

//--------------------------------
/* begin
	Returns a pointer to the begining of the array.
	It is the opposite to end()
	It is useful for iterating over a list;
*/
template< typename type_t >
D_FORCE_INLINE
type_t*
List<type_t>::begin
(
)
{
	assert( m_array != nullptr );
	return m_array;
}

//--------------------------------
/* begin
	Returns a const pointer to the begining of the array.  Useful for iterating through the m_array in loops.
	It is the opposite to end()
*/
template< typename type_t>
D_FORCE_INLINE
type_t const*
List<type_t>::begin
(
) const
{
	assert( m_array != nullptr );
	return m_array;
}

//--------------------------------
/* capacity
	Returns the number of elements currently allocated.

	NOTE: this is NOT how many elements you can use!
	this is the size of the internal array, for number of elements you can use look at
	size();
*/
template< typename type_t >
D_FORCE_INLINE
size_t
List<type_t>::capacity
(
)
const
{
	return m_capacity;
}

//--------------------------------
/* clear
	Clears the contents of the list, optionally deallocates memory

	Note: Assumes that type_t automatically handles freeing up memory.
	You may want delete_contents() if you are clear() ing pointers.
*/
template< typename type_t >
D_INLINE
void
List<type_t>::clear
(
	bool const i_andDealloc
)
{
	if( i_andDealloc == true )
	{
		if ( m_array != nullptr )
		{
			ArrayDelete< type_t >(m_array, m_capacity);
			m_array		= nullptr;
		}
		m_size		= 0;
		m_capacity	= 0;
	}
	else
	{
		m_size = 0;
	}
}

//--------------------------------
/* contains
	Just like C#'s contains function on the List class,
	returns true if i_obj element is in the list.

	NOTE: It is SLOW at O( n ) where n is the size().
	if you use this a lot, you might want to consider a HashTable
*/
template< typename type_t>
D_INLINE
bool
List<type_t>::contains
(
	type_t const& i_obj
) const
{
	for( size_t i = 0; i < m_size; ++i )
	{
		if ( m_array[i] == i_obj )
		{
			return true;
		}
	}

	// Not found
	return false;
}

//--------------------------------
/* delete_contents
	Calls the destructor (delete) of each (pointer) element in the list.
	Then sets all the pointers to nullptr
	
	CAUTION:
	NOTE: this only works on lists containing pointers to objects.
	Since the m_array was not responsible for allocating the object, it has
	no information on whether the object still exists or not, so care must be taken to ensure that
	the pointers are still valid when this function is called.
	In addition, if the elements were allocated with new[], it will cause a memory leak.
*/
template< typename type_t >
void
List<type_t>::delete_contents
(
	bool const i_andSetToNULL
)
{
	for(size_t i = 0; i < m_size; ++i )
	{
		if( m_array[i] != nullptr ) //-V809
		{
			delete m_array[i];
		}
	}

	if( i_andSetToNULL == true )
	{
		memset( m_array, 0, m_size * sizeof( type_t ) );
	}
}

//--------------------------------
/* emplace_back
	Returns a reference to a new data element at the end of the list.

	NOTE: like the C++11 version just less fancy
*/
template< typename type_t>
D_FORCE_INLINE
type_t&
List<type_t>::emplace_back()
{
	CheckResize();

	return m_array[ m_size++ ];
}

//--------------------------------
/* empty
	returns true if the container is empty (ie, size() == 0)
*/
template< typename type_t >
D_FORCE_INLINE
bool
List<type_t>::empty
(
) const
{
	return m_size == 0;
}

//--------------------------------
/* end
	returns a pointer to list[size()].
	Dereferencing is undefined behavior.
	It is the opposite to begin().
*/
template< typename type_t >
D_FORCE_INLINE
type_t const*
List<type_t>::end
(
) const
{
	assert( m_array != nullptr );
	return &m_array[m_size];
}

//--------------------------------
/* erase( index )
	Removes the element at the specified index and moves all data following the element down to fill in the gap.
	The number of elements in the m_array is reduced by one.
	Returns false if the index is outside the bounds of the list.

	NOTE: The element is not destroyed,
	so any memory used by it may not be freed until the destruction of the list.

	NOTE: This is SLOW, if you use this alot consider using a hastTable.
*/
template< typename type_t>
bool
List<type_t>::erase
(
	size_t const i_index
)
{
	assert( m_array != nullptr );

	if ( i_index < m_size )
	{
		--m_size;

		for(size_t i = i_index; i < m_size; ++i )
		{
			m_array[i] = m_array[i+1];
		}
		return true;
	}
	else
	{
		return false;
	}
}

//--------------------------------
/* erase( obj )
	Removes obj if it is found within the list and moves all data
	following the element down to fill in the gap.
	The number of elements in the m_array is reduced by one.
	Returns false if the data is not found in the list.

	NOTE: The element is not destroyed, so any memory used by it may not be freed until the destruction of the list.
	
	NOTE: SLOW, if you use it alot consider using a hashTable
*/
template< typename type_t>
bool
List<type_t>::erase
(
	type_t const& i_obj
)
{
	size_t index;
	if ( find_index( i_obj, index ) == true )
	{
		return erase( index );
	}
	
	return false;
}

//--------------------------------
/* erase_fast( index )
	Removes the element at the specified index and moves the last element into its spot, rather
	than moving the whole array down by one. Of course, this doesn't maintain the order of
	elements!
	The number of elements in the m_array is reduced by one.
	returns false if the index is out of bounds

	NOTE:	The element is not destroyed, so any memory used by it may not be freed until the 
			destruction of the list.
*/
template< typename type_t>
D_FORCE_INLINE
bool
List<type_t>::erase_fast
(
	size_t const i_index
)
{
	assert( m_array != nullptr );

	if ( i_index < m_size )
	{
		// reduce our max count
		--m_size;

		// swap the values
		if ( i_index != m_size )
		{
			m_array[i_index] = m_array[m_size];
		}
		return true;
	}
	else
	{
		return false;
	}
}

//--------------------------------
/* find( char )
	Searches for the specified data in the list and returns it's address.
	Returns nullptr if the data is not found.

	NOTE: SLOW if you use this alot consider using a hashTable
*/
template< typename type_t>
type_t*
List<type_t>::find
(
	type_t const& i_obj
) const
{
	size_t i = 0;
	if ( find_index( i_obj, i ) == true )
	{
		return &m_array[i];
	}

	return nullptr;
}

//--------------------------------
/* find_index( obj, index )
	Searches for the specified data in the m_array and returns it's index.
	Returns false if the data is not found.

	NOTE: SLOW at O( n )
*/
template< typename type_t>
bool
List<type_t>::find_index
(
	type_t const& i_obj,
	size_t& o_index
) const
{
	for( size_t i = 0; i < m_size; ++i )
	{
		if ( m_array[i] == i_obj )
		{
			o_index = i;
			return true;
		}
	}

	// Not found
	o_index = 0;
	return false;
}

//--------------------------------
/* find_null
	Finds the first nullptr in the list.
	Returns true if a nullptr was found.

	NOTE: SLOW at O( n )
*/
template< typename type_t>
bool
List<type_t>::find_null
(
	size_t& i_index
) const
{
	for( size_t i = 0; i < m_size; ++i )
	{
		if ( m_array[i] == nullptr )
		{
			i_index = i;
			return true;
		}
	}

	// Not found
	i_index = 0;
	return false;
}

//--------------------------------
/* front
	Returns a const reference to the first element.
	Opposite to back.

	NOTE: any operation on front() such as erase is the SLOWEST possible operation on a list
*/
template< typename type_t >
D_FORCE_INLINE
type_t const&
List<type_t>::front
(
) const
{
	assert( m_array != nullptr);
	return m_array[0];
}

//--------------------------------
/* insert( obj, index )
	Inserts obj at index in the list.
	Moves all the elements after it up one.
	Returns false is index is out of bounds.

	NOTE: SLOW if you are using this a lot consider using a hashTable instead
*/
template< typename type_t>
bool
List<type_t>::insert
(
	type_t const& i_obj,
	size_t const i_index
)
{
	if ( i_index < m_size )
	{
		CheckResize();

		// Move elements after m_size up one
		for ( size_t i = m_size; i > i_index; --i )
		{
			m_array[i] = m_array[i-1];
		}

		// Set the new value of i_index
		m_array[i_index] = i_obj;

		++m_size;
		return true;
	}
	else
	{
		return false;
	}
}

//--------------------------------
/* index_of
	Takes a pointer to an element in the m_array and returns the index of the element.
	This is NOT a guarantee that the object is really in the list.
	Function will assert in debug builds if pointer is outside the bounds of the list,
	but remains silent in release builds.
*/
template< typename type_t>
D_FORCE_INLINE
size_t
List<type_t>::index_of
(
	type_t const* i_ptr
) const
{
	size_t const index = ptr_range(i_ptr, static_cast<type_t const*>(m_array) );

	assert( index < m_size );
	return index;
}

//--------------------------------
/* pop_back
	Opposite to push_back(), remove the element at the back of the list

	NOTE: The element is not destroyed, so any memory used by it may not be freed until the destruction of the list.
*/
template< typename type_t>
D_FORCE_INLINE
void
List<type_t>::pop_back
(
)
{
	assert( m_array != nullptr && m_size > 0 );
	--m_size;
}

//--------------------------------
/* pop_back( i_numPops )
	Opposite to push_back(), but instead of just one element remove numPops number of elements

	NOTE: The elements are not destroyed, so any memory used by it may not be freed until the destruction of the list.
*/
template< typename type_t>
D_FORCE_INLINE
void
List<type_t>::pop_back
(
	size_t const i_numPops
)
{
	assert( m_array != nullptr && m_size > 0 );
	assert( m_size - i_numPops < m_size ); // check that we will not wrap around
	assert( i_numPops < m_size );

	m_size -= i_numPops;
}

//--------------------------------
/* push_back( i_obj )
	Appends a new element at the back of the list
*/
template< typename type_t>
D_FORCE_INLINE
void
List<type_t>::push_back
(
	type_t const& i_obj
)
{
	CheckResize();

	m_array[m_size] = i_obj;
	++m_size;
}

//--------------------------------
/* push_back( i_start, i_end )
	Append elements from start to end.
	Does not include end.
*/
template< typename type_t>
void
List<type_t>::push_back
(
	type_t const* i_start,
	type_t const* i_end
)
{
	assert( i_start != nullptr && i_end != nullptr );

	// Calc number of elements
	size_t const count = ptr_range( i_start, i_end );

	// Calc new size
	size_t const newSize = m_size + count;

	// Check we have enough space
	reserve( newSize );

	// Add each element to our list
	ArrayCopy(&m_array[m_size], &i_start[0], count);

	// Set new size
	m_size = newSize;
}

//--------------------------------
/* push_back( i_other )
	Append a whole list.
*/
template< typename type_t>
void
List<type_t>::push_back
(
	List< type_t> const& i_other
)
{

	// Calc new size
	size_t const newSize = m_size + i_other.size();

	// Check there is enough space
	reserve( newSize );

	// Add each element to our list
	for(size_t i = m_size,
		j = 0; // index for Other
		i < newSize; ++i,++j )
	{
		m_array[i] = i_other[j];
	}

	// Set new size
	m_size = newSize;
}

//--------------------------------
/* resize( newSize )
	Sets the size() of the container.
	All elements up to newSize are valid and ready to be used.
	eg
	resize( 128 ); // makes myList[127] valid

	NOTE: only allows you to set a greater size.

	NOTE: does not reconstruct the new elements which could be previously used elements (ie if the size has been decreased and then increased)
*/
template< typename type_t>
D_FORCE_INLINE
void
List<type_t>::resize
(
	size_t const newSize
)
{
	if ( newSize > m_capacity )
	{
		reallocate( newSize );
	}
	m_size = newSize;
}

//--------------------------------
/* reserve( newMinSize )
	Requests the container to resize if newMineSize is large then its current capacity,
	ie reserve capacity in advance.
	if the container is already large enough no allocations will take place.
*/
template< typename type_t>
void
List<type_t>::reserve
(
	size_t const newMinSize
)
{
	// if needs to expand
	if( newMinSize > m_capacity )
	{
		// get next size
		size_t const nextSize = ResizePolicy<type_t>::Reserve( newMinSize );
		reallocate( nextSize );
	}
	// else big enough already
}

//--------------------------------
/* reallocate( newCapacity )
	Allocates memory for the amount of elements requested while keeping the contents intact.
	Contents are copied using their = operator so that data is correctly instantiated.

	eg if you know you are not going to use more that 256 elements,
	it would be efficient to call reallocate( 256 ) before adding any elements.
	However, there is the static sized container you may want to consider.

	NOTE: usually used internally in this class, use it with care.
*/
template< typename type_t>
D_FORCE_INLINE
void
List<type_t>::reallocate
(
	size_t const newCapacity
)
{
	if( newCapacity > 0 )
	{
		if ( newCapacity == m_capacity )
		{
			// not changing the size, so just exit
			return;
		}

		m_array = ArrayResize<type_t>( m_array, m_capacity, newCapacity );
		m_capacity = newCapacity;

		// if the container just reduced in size
		if ( m_size > m_capacity)
		{
			m_size = m_capacity;
		}
	}
	else// free up the m_array if no data is being reserved
	{
		clear( true );
		return;
	}
}

//--------------------------------
/* size
	Returns the number of elements currently contained in the list.

	NOTE: that this is NOT an indication of the memory allocated.
*/
template< typename type_t >
D_FORCE_INLINE
size_t
List<type_t>::size
(
) const
{
	return m_size;
}

//--------------------------------
/* shrink_to_fit
	Resizes the array to exactly the number of elements it contains or frees up memory if empty.

	NOTE: call this when you know there are going to be no more elements added
*/
template< typename type_t>
void
List<type_t>::shrink_to_fit
(
)
{
	if ( m_array != nullptr )
	{
		if ( m_size > 0 )
		{
			reallocate( m_size );
		}
		else
		{
			clear(true);
		}
	}
}

//--------------------------------
/* shuffle( ran )
	Shuffles the list. (opposite to sort)
	Pass in the RNG you want the function to draw its random
	numbers from.

	NOTE: SLOW

	NOTE: When the container is not on the main thread,
	a reference to an instance of RNG should be given to it.
*/
template< typename type_t>
void
List<type_t>::shuffle
(
	Random::RNG& ran // = Random::Generator default is global static generator
)
{
	Algorithm::shuffle( m_array, m_size, ran );
}

//--------------------------------
/* sort( templateSort = default )
	Performs a sort on the list using the supplied sort algorithm.
	QuickSort is the default.

	NOTE: The data is moved around the list, so any pointers to data within the list may 
	no longer be valid.
*/
template< typename type_t>
void
List<type_t>::sort
(
	const Algorithm::SortBase<type_t>& sort // = idSort_QuickDefault<type_t>()
)
{
	assert( m_array != nullptr );
	sort.Sort( m_array, m_size );
}

//--------------------------------
/* _allocated
	Returns the total memory allocated for the list in bytes,
	but doesn't take into account additional memory allocated by type_t.

	ie allocated size * element size
*/
template< typename type_t >
size_t
List<type_t>::_allocated
(
) const
{
	return m_capacity * sizeof( type_t );
}

//--------------------------------
/* _size
	Returns total size of the list in bytes,
	but doesn't take into account additional memory allocated by type_t

	ie class size + allocation size
*/
template< typename type_t >
size_t
List<type_t>::_size
(
) const
{
	return sizeof( List< type_t > ) + _allocated();
}

//--------------------------------
/* _memory_used
	Returns size of the used elements in the list.

	ie element size * num used elements
*/
template< typename type_t >
size_t
List<type_t>::_memory_used
(
) const
{
	return m_size * sizeof( type_t );
}

//--------------------------------
/* operator =
	Copies one List to another (deep copy).

	NOTE: SLOW avoid if you can
*/
template< typename type_t>
List<type_t>&
List<type_t>::operator=
(
	List<type_t> const& other
)
{
	clear( true );

	m_size		= other.m_size;
	m_capacity	= other.m_capacity;

	if( m_capacity > 0 )
	{
		m_array = ArrayNew< type_t >( m_capacity );

		ArrayCopy(m_array,other.m_array, m_size);
	}

	return *this;
}

//--------------------------------
/*	operator[]
	const version.
	Performs bounds checking in debug build.
*/
template< typename type_t>
D_FORCE_INLINE
type_t const&
List<type_t>::operator[]
(
	size_t i_index
) const
{
	assert( i_index < m_size );

	return m_array[i_index];
}

//--------------------------------
/*	operator[]
	None const version.
	Performs bounds checking in debug build.
*/
template< typename type_t>
D_FORCE_INLINE
type_t&
List<type_t>::operator[]
(
	size_t i_index
)
{
	assert( i_index < m_size );

	return m_array[i_index];
}

//--------------------------------
/* Init
	Checks if m_array needs to be allocated, and allocates it if necessary.

	NOTE: This is needed because the list alloc is not on creation, thus needs to be checked in several places
*/
template< typename type_t>
D_FORCE_INLINE
void
List<type_t>::Init
(
)
{
	if ( m_array == nullptr )
	{
		assert( m_size == 0 );
		reallocate( ResizePolicy<type_t>::GetNextSize( m_size ) );
	}
}

//--------------------------------
/* CheckResize
	Checks if m_array / m_capacity needs to be increased in size.
*/
template< typename type_t>
D_FORCE_INLINE
void
List<type_t>::CheckResize
(
)
{
	if( m_size < m_capacity )
	{
		return;
	}
	else
	{
		reallocate( ResizePolicy<type_t>::GetNextSize( m_capacity ) );
	}
}