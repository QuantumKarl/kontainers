/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

//--------------------------------
/*	operator[]
	const version.
	Performs bounds checking in debug build.
*/
D_INLINE
char
Str::operator[]
(
	size_t i_index
) const
{
	assert( i_index < m_size);

	return m_array[i_index];
}

//--------------------------------
/*	operator[]
	None cost version.
	Performs bounds checking in debug build.
*/
D_INLINE
char&
Str::operator[]
(
	size_t i_index
)
{
	assert( i_index < m_size );

	return m_array[i_index];
}

//--------------------------------
/* operator =
	Copies one String to another (deep copy).

	NOTE: SLOW avoid if you can.

	NOTE: might be a proble if you have two strings of different policies
*/
D_INLINE
Str&
Str::operator=
(
	Str const& other
)
{
	if( other.m_capacity > 0 )
	{
		clear( false );
		reserve( other.m_capacity );// will also set capacity if resized
		m_size		= other.m_size;

		memcpy( static_cast<void*>(&m_array[0]),
				static_cast<void const*>(other.c_str()),
				m_size );

		// make sure to null terminate correctly
		m_array[m_size] = '\0';
	}
	else
	{
		clear(false); // assignment to nullptr will not cause memory deallocations
	}
	return *this;
}

//--------------------------------
/* operator =
	Copies one c string to a Str (deep copy).

	NOTE: SLOW avoid if you can.
*/
D_INLINE
Str&
Str::operator=
(
	char const* other
)
{
	if( other != nullptr )
	{
		size_t const len = strlen( other );
		clear( false );
		reserve( len );

		memcpy( static_cast<void*>( &m_array[0] ),
				static_cast<void const*>(other),
				len );

		m_size = len;

		// make sure to null terminate correctly
		m_array[m_size] = '\0';
	}
	else
	{
		clear(false); // assignment to nullptr will not cause memory deallocations
	}
	return *this;
}

//--------------------------------
/* operator +
	Add two Str's to make a third Str

	NOTE: very slow! try to only use for debug output.
*/
D_INLINE
Str
operator+
(
	Str const& a,
	Str const& b
)
{
	Str res(a);
	res.push_back( b );
	return res;
}

//--------------------------------
/* operator +
	Add a Str and a c string to make a third Str

	NOTE: very slow! try to only use for debug output.
*/
D_INLINE
Str
operator+
(
	Str const& a,
	char const*	b
)
{
	Str res(a);
	res.push_back( b );
	return res;
}

//--------------------------------
/* operator +
	Add a Str and a c string to make a third Str

	NOTE: very slow! try to only use for debug output.
*/
D_INLINE
Str
operator+
(
	char const*	a,
	Str const& b
)
{
	Str res(a);
	res.push_back( b );
	return res;
}

//--------------------------------
/* operator +
	Add a Str and a real_t to make a third Str

	NOTE: very slow! try to only use for debug output.
*/
D_INLINE
Str
operator+
(
	Str const& a,
	real_t const i_real
)
{
	Str res(a);
	res.push_backf( "%f", i_real );
	return res;
}

//--------------------------------
/* operator +
	Add a Str and a int64 to make a third Str

	NOTE: very slow! try to only use for debug output.
*/
D_INLINE
Str
operator+
(
	Str const& a,
	int64 const i_int64
)
{
	Str res(a);
	res.push_backf( "%lli", i_int64);
	return res;
}

//--------------------------------
/* operator +
	Add a Str and a size_t to make a third Str

	NOTE: very slow! try to only use for debug output.
*/
D_INLINE
Str
operator+
(
	Str const& a,
	size_t const i_size_t
)
{
	Str res(a);
#if defined( D_32BIT )
	res.push_backf( "%lu", i_size_t ); //-V111
#elif defined( D_64BIT )
	res.push_backf( "%llu", i_size_t ); //-V111
#else
	compile_time_assert( false );
#endif
	return res;
}

//--------------------------------
/* operator +
	Add a Str and a bool to make a third Str

	NOTE: very slow! try to only use for debug output.
*/
D_INLINE
Str
operator+
(
	Str const& a,
	bool const i_bool
)
{
	Str res(a);
	if( i_bool == true )
	{
		res.push_back( "true", 4 ); //-V112
	}
	else
	{
		res.push_back( "false", 5 ); //-V112
	}
	return res;
}

//--------------------------------
/* operator +=
	Appending a string to another.
*/
D_INLINE
Str&
Str::operator+=
(
	Str const& i_str
)
{
	push_back( i_str );
	return *this;
}

//--------------------------------
/* operator +=
	Appending a string to another.
*/
D_INLINE
Str&
Str::operator+=
(
	char const* i_cStr
)
{
	push_back( i_cStr );
	return *this;
}

//--------------------------------
/* operator +=
	Appending a char to string.
*/
D_INLINE
Str&
Str::operator+=
(
	char const i_char
)
{
	push_back( i_char );
	return *this;
}

//--------------------------------
/* operator +=
	Appending a real_t to string.
*/
D_INLINE
Str&
Str::operator+=
(
	real_t const i_real
)
{
	push_backf( "%f", i_real );
	return *this;
}

//--------------------------------
/* operator +=
	Appending a int64 to string.
*/
D_INLINE
Str&
Str::operator+=
(
	int64 const i_int64
)
{
	push_backf( "%lli", i_int64);
	return *this;
}

//--------------------------------
/* operator +=
	Appending a size_t to string.
*/
D_INLINE
Str&
Str::operator+=
(
	size_t const i_size_t
)
{
#if defined( D_32BIT )
	push_backf( "%lu", i_size_t); //-V112
#elif defined( D_64BIT )
	push_backf( "%llu", i_size_t); //-V112
#else
	compile_time_assert( false );
#endif
	return *this;
}

//--------------------------------
/* operator +=
	Appending a bool to string.
*/
D_INLINE
Str&
Str::operator+=
(
	bool const i_bool
)
{
	if( i_bool == true )
	{
		push_back( "true", 4 ); //-V112
	}
	else
	{
		push_back( "false", 5 ); //-V112
	}
	return *this;
}

//--------------------------------
/* operator ==
	Checks for equality.
*/
D_INLINE
bool
operator==
(
	Str const& i_strLeft,
	Str const& i_strRight
)
{
	if( i_strLeft.c_str() == i_strRight.c_str() )
	{
		return true;
	}
	if( i_strLeft.size() == i_strRight.size() )
	{
		return strncmp(	i_strLeft.c_str(),
						i_strRight.c_str(),
						i_strLeft.size() ) == 0;
	}
	else
	{
		return false;
	}
}

//--------------------------------
/* operator ==
	Checks for equality.
*/
D_INLINE
bool
operator==
(
	Str const& i_strLeft,
	char const* i_cStrRight
)
{
	assert( i_cStrRight != nullptr );
	if( i_strLeft.c_str() != i_cStrRight )
	{
		return strcmp( i_strLeft.c_str(), i_cStrRight ) == 0;
	}
	else
	{
		return true;
	}
}

//--------------------------------
/* operator ==
	Checks for equality.
*/
D_INLINE
bool
operator==
(
	char const* i_cStrLeft,
	Str const& i_strRight
)
{
	assert( i_cStrLeft != nullptr );
	if( i_cStrLeft != i_strRight.c_str() )
	{
		return strcmp( i_cStrLeft, i_strRight.c_str() ) == 0;
	}
	else
	{
		return true;
	}
}

//--------------------------------
/* operator !=
	Checks for inequality.
*/
D_INLINE
bool
operator!=
(
	Str const& i_strLeft,
	Str const& i_strRight
)
{
	return ( i_strLeft == i_strRight ) == false;
}

//--------------------------------
/* operator !=
	Checks for inequality.
*/
D_INLINE
bool
operator!=
(
	Str const& i_strLeft,
	char const* i_cStrRight
)
{
	assert( i_cStrRight != nullptr );
	return (i_strLeft == i_cStrRight ) == false;
}

//--------------------------------
/* operator !=
	Checks for inequality.
*/
D_INLINE
bool
operator!=
(
	char const* i_cStrLeft,
	Str const& i_strRight
)
{
	assert( i_cStrLeft != nullptr );
	return ( i_cStrLeft == i_strRight ) == false;
}

//--------------------------------
/* operator >>
	writes data out of a the string into ostream.
*/
D_INLINE
ostream&
operator<<
(
	ostream& i_ostream,
	Str&	i_str
)
{
	// stream out the c string
	i_ostream << i_str.c_str();

	// return the stream
	return i_ostream;
}

//--------------------------------
/* operator >>
	reads data out of a istream into the string.
*/
D_INLINE
istream&
operator>>
(
	istream& i_istream,
	Str&	i_str
)
{
	// create local buffer to read into
	char cache[Str::istream_buffer_size];

	// stream from istream to char[]
	i_istream >> cache;

	// add the result into the string
	i_str.push_back(cache);

	// return the stream
	return i_istream;
}