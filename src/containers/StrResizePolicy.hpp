/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

// Default resize policy for the Str container.
class StrResizePolicyNormal
{
	// remember these are bytes! so they are very small compared to the other containers / objects
	static size_t const c_granularity	= sizeof(size_t)*8*2; // 32 bit = 64, 64 bit = 128
	static size_t const c_baseSize		= sizeof(size_t)*8*4; // 32 bit = 128, 64 bit = 256
public:

	static
	D_FORCE_INLINE
	size_t
	GetNextSize
	(
		size_t const currentSize
	)
	{
		if( currentSize != 0 )
		{
			// round up to the next multiple of c_Memory_Alignment to make sure everything is aligned
			return RoundUpToMultipleOf( currentSize + c_granularity, c_Memory_Alignment );
		}
		else// initial size
		{
			return c_baseSize;
		}
	}

	static
	D_FORCE_INLINE
	size_t
	Round // Rounds up to the nearest size
	(
		size_t const currentSize
	)
	{
		if( currentSize != 0 )
		{
			return RoundUpToMultipleOf( currentSize, c_Memory_Alignment );
		}
		else// initial size
		{
			return c_baseSize;
		}
	}
};