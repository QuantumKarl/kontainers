.data
timeStart	qword 0
timeEnd		qword 0

.code
PUBLIC startTimerA
PUBLIC endTimerA

startTimerA PROC
	RDTSC
	mov DWORD PTR [timeStart], EAX
	mov DWORD PTR [timeStart+dword], EDX
	RET
startTimerA ENDP

endTimerA PROC
	RDTSC
	mov DWORD PTR [timeEnd], EAX
	mov DWORD PTR [timeEnd+dword], EDX
	mov RAX, timeEnd
	mov RBX, timeStart
	sub RAX, RBX
	RET
endTimerA ENDP
End