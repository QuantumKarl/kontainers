/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_SYS_DEFINES_H )
#define D_SYS_DEFINES_H
//----------------------------------------------------------------
//							Un defines

//#undef all the things

// end of Un defines
//----------------------------------------------------------------

//----------------------------------------------------------------
//							Platform Defines

#if defined(WIN64)
	// 64 bit windows

	#define D_PC
	#define D_PC_WIN
	#define D_WIN64
	#define D_64BIT
	#define D_LITTLE_ENDIAN
	#define	D_CPUSTRING	"x64"

#elif defined(WIN32)
	// 32 bit windows

	#define D_PC
	#define D_PC_WIN
	#define D_WIN32
	#define D_32BIT
	#define D_LITTLE_ENDIAN
	#define	D_CPUSTRING	"x86"

#else

// TODO: linux / GCC defines

#error Unknown Platform

#endif

// end of Platform Defines
//----------------------------------------------------------------

//----------------------------------------------------------------
//						Build config defines
#if defined( FINAL )
#define D_FINAL
#elif defined( RELEASE )
#define D_RELEASE
#elif defined( DEBUG )
#define D_DEBUG
#else
#error  Unknown build config
#endif
// end of build config defines
//----------------------------------------------------------------

//----------------------------------------------------------------
//						Platform specific defines

//--------------------------------
// windows defines
#ifdef D_PC_WIN

#define	D_BUILD_STRING					"win-" CPUSTRING

#define D_ALIGN16( x )					__declspec(align(16)) x
#define D_ALIGNTYPE16					__declspec(align(16))
#define D_ALIGNTYPE128					__declspec(align(128))

// Used in text / string classes
#define D_PATHSEPARATOR_STR				"\\"
#define D_PATHSEPARATOR_CHAR			'\\'
#define D_NEWLINE						"\r\n"

#define D_INLINE						inline
#define D_FORCE_INLINE					__forceinline

// lint complains that extern used with definition is a hazard, but it
// has the benefit (?) of making it illegal to take the address of the function
#ifdef _lint
#define D_INLINE_EXTERN				inline
#define D_FORCE_INLINE_EXTERN		__forceinline
#else
#define D_INLINE_EXTERN				extern inline
#define D_FORCE_INLINE_EXTERN		extern __forceinline
#endif

// checking format strings catches a LOT of errors
#include <CodeAnalysis\SourceAnnotations.h>
#define	D_VERIFY_FORMAT_STRING	[SA_FormatString(Style="printf")]

#endif
// TODO: linux / GCC platform defines

// end of windows defines
//--------------------------------

// end of Platform specific defines
//----------------------------------------------------------------
#endif // end of D_SYS_DEFINES_HPP