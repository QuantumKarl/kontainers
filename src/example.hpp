/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_EXAMPLE_HPP )
#define D_EXAMPLE_HPP

#include "algorithm\Sort.hpp"
#include "containers\List.hpp"
#include "containers\ListFixed.hpp"
#include "containers\Array.hpp"
#include "containers\Queue.hpp"
#include "containers\Heap.hpp"

#include "misc\Primes.hpp"

#include "hashes\MurmurHash2.hpp"
#include "containers\Map.hpp"
#include "containers\Str.hpp"

#define _USE_MATH_DEFINES
#include <math.h>

// ctr + f for the function are you interested in
namespace container_examples
{
	// an object to play around with, so you can see what is being called
	class obj
	{
		size_t ii;
	public:
		obj(size_t iiIn = 234): ii( iiIn ){}

		~obj()
		{
			cout << "obj dtor" << endl;
		}

		obj& operator=( const obj& other )
		{
			ii = other.ii;
			return *this;
		}
	};

	void ListExample()
	{
		// create a list which contains ints
		List<int> myList;
		myList.resize( 4 ); //-V112 // Tell the container to allocate space for 4 elements

		int const numbers[4] = {1,2,3,4} ;

		// push_back
		myList.push_back( &numbers[0], &numbers[4] );// myList now contains 1,2,3,4
		myList.push_back( 5 );						// myList now contains 1,2,3,4,5. Also just resized internally, more on that later.

		// pop_back
		myList.pop_back(); // myList now contains 1,2,3,4
		myList.pop_back(); // myList now contains 1,2,3

		// does out list contain the number 2? NOTE: SLOW
		if( myList.contains( 2 ) == true )
		{
			// is the back of the list 3?
			if( myList.back() == 3 )
			{
				// create a new entry at the back of the list and then assign its value to 4
				myList.emplace_back() = 4; //-V112
				// myList now contains 1,2,3,4
			}
			// else back() != 3
		}
		// else number 2 is not in the list

		// insert 0 at the 0th element (very SLOW)
		if( myList.insert( 0, 0 ) == true )
		{
			//myList now contains 0,1,2,3,4
		}
		// else index is out of bounds

		// iteration example via pointer
		int* current = myList.begin();
		int const* end	= myList.end();
		while( current != end )
		{
			// add one to the current element
			*current += 1;
			++current;// increment the pointer
		}
		//myList now contains 1,2,3,4,5

		//iteration example via index
		size_t size = myList.size();
		for(size_t i = 0; i < size; ++i )
		{
			// take away two from each element
			myList[i] -= 2;
		}
		// myList now contains -1,0,1,2,3

		// double check the contents is as above
		if( myList.front() == -1	&&	// look at the front
			myList.back() == 3 )		// look at the back
		{
			// find the index of the value 0
			size_t indexOfZero = 0;
			if( myList.find_index( 0, indexOfZero ) == true )// SLOW
			{
				// erase via swap not move down
				myList.erase_fast( indexOfZero ); // myList now contains -1,3,1,2
			}
		}

		// we set the size of the container to 4 by calling resize at the start.
		// then we added 5 elements. This means the container had to resize once.
		// when a container resizes it increases by 2 times its current sizes.
		// Note: you can specify otherwise, see ResizePolicy at the top of list.

		// The container's capacity should be 8 since we resized from 4 when we pushed in a 5th element.
		if( myList.capacity() == 8 )
		{
			//correct, the list has only resized once.
			size_t bytes = 0;

			// size on the end of m_list or allocated size
			bytes = myList._allocated();		// sizeof( int ) * myList.capacity() == 4 * 8 == 32

			// size the user is using / can access
			bytes = myList._memory_used();		// sizeof( int ) * myList.size() == 4 * 4 == 16

			// size of the container including its allocated size but excluding anything the contents of m_list has allocated
			bytes = myList._size();				// sizeof( List<int> ) * myList._allocated() == sizeof(*m_list) + sizeof( m_num ) + sizeof( m_capacity ) + 32 == 8 + 8 + 8 + 32 == 56

			// lets not waste any memory, tell the container to shrink to the number of elements we are using
			myList.shrink_to_fit(); // SLOW will re-alloc, call this once you know there will be no more elements added

			// we have no unused memory
			if( myList._allocated() == myList._memory_used() )
			{
				// lets make our data nice by quickly sorting it (quick sort is the default sort, ascending order)
				myList.sort();
			}

			// OK enough of myList<int> lets remove all the elements
			myList.clear( true ); // Also deallocates memory
		}

		//----------------
		// lets make a new list containing pointers
		List<int*> myPtrList(4,nullptr); //-V112 // set its size to 4 and each value to nullptr

		//actually lets have 8
		myPtrList.resize( 8 ); // expand the list, but with set_size apposed to resize, you can access these straight away.

		// However, 5 to 7 (indexes) are uninitialized!
		for( size_t i = 5; i < 8; ++i )
		{
			myPtrList[i] = nullptr;
		}

		// lets store some pointers
		myPtrList[1] = new int(1 << 1); //-V508
		myPtrList[2] = new int(1 << 2); //-V508
		myPtrList[3] = new int(1 << 3); //-V508
		myPtrList[4] = new int(1 << 4); //-V508

		// you can use push back with another list as an argument, also you can pass your own list to make it repeat
		myPtrList.push_back( myPtrList );

		// hmm that clearly wasnt a good idea, we have a list with duplicated pointers, how would we know which ones are valid?
		// lets get rid of 8 from the back
		myPtrList.pop_back( 8 );
		// alternatively, could have sorted and once we come across a none null pointer remove it then skip ahead two

		// lets check where the first nullptr is, just in case we are doing something silly
		size_t nullIndex = myPtrList.size() + 1; // just so we can see if its changed
		if( myPtrList.find_null( nullIndex ) == true )
		{
			// ops didnt initialise the first element, shame we can not count very well.
			myPtrList[nullIndex] = new int( 1 << 0 ); //-V508
		}

		// lets deallocate them properly
		myPtrList.delete_contents( true ); // The pointers had "delete" called on the and then all set to nullptr
		// IMPORTANT NOTE: anything not allocated with "new" would have been a memory leak or undefined behavior

		//----------------
		// you can use a list as a stack
		//	pop_back	== pop
		//	back		== peek
		//	push_back	== push

		List<real_t> myStackList;

		// request that we have at least space for 128 elements
		myStackList.reserve( 128 );

		myStackList.push_back( static_cast<real_t>(M_PI) );

		if( myStackList.back() > 3.0 )
		{
			myStackList.push_back( static_cast<real_t>(M_E) );
		}

		if( myStackList.back() > 2.0 )
		{
			myStackList.push_back( static_cast<real_t>((1.0+sqrt(5.0))/2) );
		}

		if( myStackList.back() > 1 )
		{
			myStackList.push_back( 0.0 );
		}

		// Sum and empty the list
		real_t sum = 0.0;
		while(myStackList.empty() == false ) // while the list is not empty
		{
			sum += myStackList.back();
			myStackList.pop_back();
		}

		//----------------
		// Misc functions missed so far
		List<int> myMiscList;
		myMiscList.push_back(&numbers[0], &numbers[4]);

		if( myMiscList.index_of( &myMiscList[2] ) == 2 ) // you can get an index of a member by passing in its pointer
		{
			if( myMiscList.find( 3 ) != nullptr ) // you can get a pointer to an element given its value
			{
				myMiscList.erase( 2 ); // you can erase by value
				myMiscList.erase( static_cast<int const&>(myMiscList[2]) ); //-V106 // you can erase by reference to object
			}
		}
		List<int> myOtherMiscList( myMiscList ); // Has a copy constructor
		myOtherMiscList.clear( false ); // does not deallocate memory
		myOtherMiscList = myMiscList; // has an assignment operator
		List<int> iteratorContructor( &numbers[0], &numbers[4] );
		int* heap_numbers = new int[4];
		heap_numbers[0] = 1;
		heap_numbers[1] = 2;
		heap_numbers[2] = 3;
		heap_numbers[3] = 4; //-V112
		List<int> iteratorContructor2( &heap_numbers[0], &heap_numbers[4] );
		delete[] heap_numbers;

		//================================================================
		// KNOWN PITFALLS
		// 1: mixing pop_back and set_size
		// push_back some elements
		// pop_back some elements
		// set_size to the previous size before popping
		// look at the "new" values, they are the old values since, they are not actually removed
		// is this desired behavior? to not remove anything until the container goes out of scope
		// seems possible on IDTec
		// short answer, yes
	}

	void ListFixedExample()
	{
		// create a list which contains ints
		ListFixed<int,8> myList;

		int const numbers[4] = {1,2,3,4} ;

		// push_back
		myList.push_back( &numbers[0], &numbers[4] );// myList now contains 1,2,3,4
		myList.push_back( 5 );						// myList now contains 1,2,3,4,5. Also just resized internally, more on that later.

		// pop_back
		myList.pop_back(); // myList now contains 1,2,3,4
		myList.pop_back(); // myList now contains 1,2,3

		// does out list contain the number 2? NOTE: SLOW
		if( myList.contains( 2 ) == true )
		{
			// is the back of the list 3?
			if( myList.back() == 3 )
			{
				// create a new entry at the back of the list and then assign its value to 4
				myList.emplace_back() = 4; //-V112
				// myList now contains 1,2,3,4
			}
			// else back() != 3
		}
		// else number 2 is not in the list

		// insert 0 at the 0th element (very SLOW)
		if( myList.insert( 0, 0 ) == true )
		{
			//myList now contains 0,1,2,3,4
		}
		// else index is out of bounds

		// iteration example via pointer
		int* current = myList.begin();
		int const* end	= myList.end();
		while( current != end )
		{
			// add one to the current element
			*current += 1;
			++current;// increment the pointer
		}
		//myList now contains 1,2,3,4,5

		//iteration example via index
		size_t size = myList.size();
		for(size_t i = 0; i < size; ++i )
		{
			// take away two from each element
			myList[i] -= 2;
		}
		// myList now contains -1,0,1,2,3

		// double check the contents is as above
		if( myList.front() == -1	&&	// look at the front
			myList.back() == 3 )		// look at the back
		{
			// find the index of the value 0
			size_t indexOfZero = 0;
			if( myList.find_index( 0, indexOfZero ) == true )// SLOW
			{
				// erase via swap not move down
				myList.erase_fast( indexOfZero ); // myList now contains -1,3,1,2
			}
		}

		// we set the size of the container to 4 by calling resize at the start.
		// then we added 5 elements. This means the container had to resize once.
		// when a container resizes it increases by 2 times its current sizes.
		// Note: you can specify otherwise, see ResizePolicy at the top of list.

		// The container's capacity should be 8 since we resized from 4 when we pushed in a 5th element.
		if( myList.capacity() == 8 )
		{
			//correct, the list has only resized once.
			size_t bytes = 0;

			// size on the end of m_list or allocated size
			bytes = myList._allocated();		// sizeof( int ) * myList.capacity() == 4 * 8 == 32

			// size the user is using / can access
			bytes = myList._memory_used();		// sizeof( int ) * myList.size() == 4 * 4 == 16

			// size of the container including its allocated size but excluding anything the contents of m_list has allocated
			bytes = myList._size();				// sizeof( ListFixed<int> ) * myList._allocated() == sizeof(*m_list) + sizeof( m_num ) + sizeof( m_capacity ) + 32 == 8 + 8 + 8 + 32 == 56

			// lets mix things up abit with a shuffle
			myList.shuffle( Random::Generator() );

			// lets make our data nice by quickly sorting it (quick sort is the default sort, ascending order)
			myList.sort();

			// OK enough of myList<int> lets remove all the elements
			myList.clear();
		}

		//----------------
		// lets make a new list containing pointers
		ListFixed<int*,16> myPtrList(8,nullptr); // set its size to 4 and each value to nullptr

		// lets store some pointers
		myPtrList[1] = new int(1 << 1); //-V508
		myPtrList[2] = new int(1 << 2); //-V508
		myPtrList[3] = new int(1 << 3); //-V508
		myPtrList[4] = new int(1 << 4); //-V508

		// you can use push back with another list as an argument, also you can pass your own list to make it repeat
		myPtrList.push_back( myPtrList );

		// hmm that clearly wasnt a good idea, we have a list with duplicated pointers, how would we know which ones are valid?
		// lets get rid of 8 from the back
		myPtrList.pop_back( 8 );
		// alternatively, could have sorted and once we come across a none null pointer remove it then skip ahead two

		// lets check where the first nullptr is, just in case we are doing something silly
		size_t nullIndex = myPtrList.size() + 1; // just so we can see if its changed
		if( myPtrList.find_null( nullIndex ) == true )
		{
			// ops didnt initialise the first element, shame we can not count very well.
			myPtrList[nullIndex] = new int( 1 << 0 ); //-V508
		}

		// lets deallocate them properly
		myPtrList.delete_contents( true ); // The pointers had "delete" called on the and then all set to nullptr
		// IMPORTANT NOTE: anything not allocated with "new" would have been a memory leak or undefined behavior

		//----------------
		// you can use a list as a stack
		//	pop_back	== pop
		//	back		== peek
		//	push_back	== push

		ListFixed<real_t,8> myStackList;

		myStackList.push_back( static_cast<real_t>(M_PI) );

		if( myStackList.back() > 3.0 )
		{
			myStackList.push_back( static_cast<real_t>(M_E) );
		}

		if( myStackList.back() > 2.0 )
		{
			myStackList.push_back( static_cast<real_t>( (1.0+sqrt(5.0))/2.0) );
		}

		if( myStackList.back() > 1 )
		{
			myStackList.push_back( 0.0 );
		}

		// Sum and empty the list
		real_t sum = 0.0;
		while(myStackList.empty() == false ) // while the list is not empty
		{
			sum += myStackList.back();
			myStackList.pop_back();
		}

		//----------------
		// Misc functions missed so far
		ListFixed<int,8> myMiscList;
		myMiscList.push_back(&numbers[0], &numbers[4]);

		if( myMiscList.index_of( &myMiscList[2] ) == 2 ) // you can get an index of a member by passing in its pointer
		{
			if( myMiscList.find( 3 ) != nullptr ) // you can get a pointer to an element given its value
			{
				myMiscList.erase( 2 ); // you can erase by value
				myMiscList.erase( static_cast<int const&>(myMiscList[2]) ); //-V106 // you can erase by reference to object
			}
		}
		ListFixed<int,8> myOtherMiscList( myMiscList ); // Has a copy constructor
		myOtherMiscList.clear();
		myOtherMiscList = myMiscList; // has an assignment operator
		ListFixed<int,8> iteratorContructor( &numbers[0], &numbers[4] );
		int* heap_numbers = new int[4];
		heap_numbers[0] = 1;
		heap_numbers[1] = 2;
		heap_numbers[2] = 3;
		heap_numbers[3] = 4; //-V112
		ListFixed<int,8> iteratorContructor2( &heap_numbers[0], &heap_numbers[4] );
		delete[] heap_numbers;

		//================================================================
		// KNOWN PITFALLS
		// 1: mixing pop_back and set_size
		// push_back some elements
		// pop_back some elements
		// set_size to the previous size before popping
		// look at the "new" values, they are the old values since, they are not actually removed
		// is this desired behavior? to not remove anything until the container goes out of scope
		// seems possible on IDTec
		// short answer, yes
	}

	void ArrayExample()
	{

		int const numbers[4] = {1,2,3,4} ;
		Array<int,128> myList(&numbers[0], &numbers[4] );

		// does out list contain the number 2? NOTE: SLOW
		if( myList.contains( 2 ) == true )
		{
			// is the back of the list 3?
			if( myList.back() == 4 ) //-V112
			{
			}
			// else back() != 3
		}
		// else number 2 is not in the list

		// iteration example via pointer
		int* current = myList.begin();
		int const* end	= myList.end();
		while( current != end )
		{
			// add one to the current element
			*current += 1;
			++current;// increment the pointer
		}

		//iteration example via index
		size_t size = myList.size();
		for(size_t i = 0; i < size; ++i )
		{
			// take away two from each element
			myList[i] -= 2;
		}

		// double check the contents is as above
		if( myList.front() == -1	&&	// look at the front
			myList.back() == 3 )		// look at the back
		{
			// find the index of the value 0
			size_t indexOfZero = 0;
			if( myList.find_index( 0, indexOfZero ) == true )// SLOW
			{
				myList[2] = 2;
			}
		}

		size_t bytes = 0;

		// size the user is using / can access
		bytes = myList._memory_used();		// sizeof( int ) * myList.size()

		//----------------
		// lets make a new list containing pointers
		Array<int*,8> myPtrList(nullptr); // set its size to 4 and each value to nullptr

		// lets store some pointers
		myPtrList[1] = new int(1 << 1); //-V508
		myPtrList[2] = new int(1 << 2); //-V508
		myPtrList[3] = new int(1 << 3); //-V508
		myPtrList[4] = new int(1 << 4); //-V508

		// lets check where the first nullptr is, just in case we are doing something silly
		size_t nullIndex = myPtrList.size() + 1; // just so we can see if its changed (not necessary)
		if( myPtrList.find_null( nullIndex ) == true )
		{
			// ops didnt initialise the first element, shame we can not count very well.
			myPtrList[nullIndex] = new int( 1 << 0 ); //-V508
		}

		// lets deallocate them properly
		myPtrList.delete_contents( true ); // The pointers had "delete" called on them and then all set to nullptr
		// IMPORTANT NOTE: anything not allocated with "new" would have been a memory leak or undefined behavior

		// Misc
		Array<int,128> copy(myList);
	}

	void MapExample()
	{
		{
			// Create hash tables via template ( Does not allocate memory )
			Map<string, size_t> hashT;
			Map<string, size_t> hashT2;

			// reserve enough memory for 64 entries (inserts) into the table
			hashT.reserve(64); // Allocates memory

			// insert elements willy nilly
			hashT.insert("one",		1);
			hashT.insert("two",		2);
			hashT.insert("three",	3); // returns true if the insert was successful
			hashT.insert("four",	4); //-V112 // will return false if the entry is already in the table
			hashT.insert("five",	5); 
			hashT.insert("six",		6);
			// the container will automatically resize when necessary (you can manipulate this via resizeLoad on the ctor)

			// size is the number of successful inserts / the number of elements in the table
			cout << hashT.size() << endl;

			// You can get info on the amount of memory allocated
			cout << "_allocated "	<< hashT._allocated() << endl;
			cout << "_size "		<< hashT._size() << endl;
			cout << "_memory_used "	<< hashT._memory_used() << endl;

			// use for_all_elements to do something on each inserted value (SLOW, and suggests you should not be using a hashTable)
			hashT.for_all_elements( []( size_t& val ) { val+=1; } );
			hashT.for_all_elements( []( const size_t& val ) { cout << val << endl; } );

			// Get values by calling find
			size_t* val = nullptr;
			if( hashT.find( "five", val ) == true )// val is a pointer to the found object
				// you should not keep these returned pointers! when the container resize they become invalid.
			{
				cout << *val << endl;
			}
			// else not found

			// You can use the assignment operator
			hashT2 = hashT;
			hashT.clear(false); // This empties the hash table but does not deallocate memory
			hashT2.clear(true); // This empties the hash table and deallocates memory
		} // Maps will automatically deallocate memory when they fall out of scope

		// The hash table works best for pointers since they are small and will fit in the cache well
		// Example of a hash table to pointers

		Map<string,real_t*> table;

		// Insert pointers via new keyword
		table.insert("Pi",new real_t( static_cast<real_t>(M_PI) ) );
		table.insert("e", new real_t(static_cast<real_t>(M_E) ) );
		table.insert("GoldenRatio",new real_t(static_cast<real_t>( (1.0+sqrt(5.0))/2.0 ) ) );

		// Remember you get a pointer to your type, and the type was real_t* so its a pointer to a pointer
		real_t** val = nullptr;
		if( table.find( "GoldenRatio", val ) == true )
		{
			// you can store nullptr values if you wanted
			if( *val != nullptr )
			{
				cout << **val << endl;
			}
		}
		// else not found

		// Calls "delete" on each of the valid entries in the Map
		table.delete_contents();
		table.clear(); // defaults to "true" ie will deallocate memory

		// if you allocated the pointers with anything other than "new" it would be a memory leak and undefined behavior.
		// instead you may use for_all_elements to deallocate none "new"ed pointers (ie, malloc, new[])
	}

	void SetExample()
	{
		// Create Sets via template
		Set<string> hashT;
		Set<string> hashT2; // Does not allocate memory

		// reserve enough memory for 64 entries (inserts) into the table
		hashT.reserve(64); // Allocates memory

		// insert elements willy nilly
		hashT.insert("one");
		hashT.insert("two");
		hashT.insert("three"); // returns true if the insert was successful
		hashT.insert("four"); // will return false if the entry is already in the table
		hashT.insert("five");
		hashT.insert("six");
		// the container will automatically resize when necessary (you can manipulate this via resizeLoad on the ctor)

		// size is the number of successful inserts / the number of elements in the table
		cout << hashT.size() << endl;

		// You can get info on the amount of memory allocated
		cout << "_allocated "	<< hashT._allocated() << endl;
		cout << "_size "		<< hashT._size() << endl;
		cout << "_memory_used "	<< hashT._memory_used() << endl;

		// Check the keys exist by calling find
		if( hashT.find( "five" ) == true )
		{
			cout << "there is such a thing as the number five" << endl;
		}
		// else not found

		if( hashT.find( "zero" ) == false )
		{
			// no such thing as zero
		}

		// You can use the assignment operator
		hashT2 = hashT;
		hashT.clear(false); // This empties the hash table but does not deallocate memory
		hashT.resize(128); // just for testing, do not take note of this
		hashT2.clear(true); // This empties the hash table and deallocates memory
		// Set will automatically deallocate memory when they fall out of scope
	}

	void QueueExample()
	{
		//----------------
		// set up a Queue
		Queue<int> q;

		//----------------
		// add some entries
		for( int i = 0; i < 128; ++i )
		{
			q.push_back( i );
		}

		//----------------
		// set up another Queue using copy ctor
		Queue<int> q2(q);

		//----------------
		// remove entries from previous Queue
		cout << "Q::" << endl;
		while( q.empty() == false )
		{
			cout << q.front() << endl;
			q.pop_front();
		}
		cout << endl;

		//----------------
		// add some more entries to previous Queue
		cout << "Q:: back" << endl;
		for( int i = 64; i < 128; ++i )
		{
			q.emplace_back() = i;
			cout << q.back() << endl;
		}
		cout << endl;

		//----------------
		// remove entries from previous Queue
		cout << "Q::" << endl;
		while( q.empty() == false )
		{
			cout << q.front() << endl;
			q.pop_front();
		}
		cout << endl;

		//----------------
		// Create yet another Queue by iterator style
		int ints[8] = { 9,8,7,6,5,4,3,2 };
		Queue<int,true> q3( &ints[0], &ints[8] );
		q3.reserve( 64 );// make sure we have enough space for 64 elements

		//----------------
		// add some entries
		for( int i = 0; i < 128; ++i )
		{
			q3.push_back( i );
		}

		//----------------
		// remove entries
		cout << "Q3::" << endl;
		while( q3.empty() == false )
		{
			cout << q3.front() << endl;
			q3.pop_front();
		}
		cout << endl;

		//----------------
		// Clean up previous Queue
		q2.clear(false);

		//----------------
		cout << "Memory sizes" << endl;
		cout << q3._allocated() << endl;
		cout << q3._size() << endl;
		cout << q3._memory_used() << endl;
	}

	void HeapExample()
	{
		// simple example
		{
			Heap<int,max_heap> h;
			h.reserve(128);
			h.push(-10);
			h.push(-3);
			h.push(4); //-V112
			h.push(124);

			while( h.empty() == false )
			{
				cout << h.top() << endl;
				h.pop();
			}
		}
#if 1
		// this is testing code
		bool coutStuff = false;
		const int	num = 2048;
		const int	intervals = 8;
		int			previous = num+1;

		Heap<int,min_heap> h;
		h.reserve(128);
		Heap<int,min_heap> h2;
		h.reserve(num);

		size_t count = num/intervals;
		for(size_t j = 0; j < intervals; ++j )
		{
			for(size_t i = 0; i < count; ++i)
			{
				if( coutStuff == true )
				{
					cout << "--------------------------------" << endl;
					cout << "--- before push ----" << endl;
					if( h.size() > 0 )
					PrintArray<int>("",&h[0],0,h.size());
				}
				if( h.size() > 0 )
				{
					previous = h.top();
				}

				// push
				h.push( (int)Random::Generator().Int(0,num) );
				h2.push( (int)Random::Generator().Int(0,num*2) );
#
				assert( h.top() <= previous );
				if( coutStuff )
				{
					cout << "--- after push ----" << endl;
					PrintArray<int>("",&h[0],0,h.size());
				}
			}

			for(size_t i = 0; i < count; ++i)
			{
				if( h.size() > 0 )
				if( coutStuff == true )
				{
					cout << "--------------------------------" << endl;
					cout << "--- before pop----" << endl;
					PrintArray<int>("",&h[0],0,h.size());
				}

				previous = h.top();

				//pop
				h.pop();

				if( h.size() > 0 )
				{
					if( coutStuff == true )
					{
						cout << "--- after pop----" << endl;
						PrintArray<int>("",&h[0],0,h.size());
					}

					assert( h.top() >= previous );
				}
			}

			// just for testing
			List<int> heaped( &h2[0], (&h2[0])+(h2.size()/2) );
			heaped.shuffle( Random::Generator() );
			Heap<int,min_heap> h3( heaped.begin(), heaped.end() );
			h3 = h2;
		}

		while( h2.empty() == false )
		{
			h2.pop();
		}
#endif
	}
}// endof namespace container_examples
#endif // end of D_EXAMPLE_HPP