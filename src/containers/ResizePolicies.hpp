/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_RESIZE_POLICIES_HPP )
#define D_RESIZE_POLICIES_HPP
//--------------------------------------------------------------------------------------------------------------------------------
/* Container resize policies
	This file allows containers to resize by different amounts based on the type they are containing.

	Specialize on the type you want to behave differently, define your own behavior.

	NOTE: default behavior is increase size by *2 then round to nearest multiple of memory alignment

	Used by:
	List,
	Heap
*/

// Allows you to setup the default allocation sizes per type.
// Add you own specializations.
template< typename _type_ >
class ResizePolicy
{
	enum { default_capacity = 64 };
public:

	static
	D_FORCE_INLINE
	size_t
	GetNextSize
	(
		size_t const i_currentSize
	)
	{
		if( i_currentSize != 0 )
		{
			// round up to the next multiple of c_Memory_Alignment to make sure everything is aligned
			return RoundUpToMultipleOf( i_currentSize * 2, c_Memory_Alignment ) ;
		}
		else// initial size
		{
			return static_cast<size_t>(default_capacity);
		}
	}

	static
	D_FORCE_INLINE
	size_t
	Reserve // Rounds up to nearest binary number, to be used with reserve
	(
		size_t const i_requiredNum
	)
	{
		if( i_requiredNum > static_cast<size_t>(default_capacity) )
		{
			return static_cast<size_t>( PowerOf2GE( i_requiredNum ) );
		}
		else
		{
			return static_cast<size_t>(default_capacity);
		}
	}

private:
};

#endif // end of D_RESIZE_POLICIES_HPP