/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_ALLOC_HPP )
#define D_ALLOC_HPP

#if !defined( D_FINAL )
#define D_ALLOC_COUNT
#endif

#include "..\sys\sys_defines.hpp"
#include "..\sys\sys_includes.hpp"
#include "..\sys\sys_assert.hpp"
#include "..\sys\sys_types.hpp"
#include "..\sys\sys_global.hpp"

//----------------------------------------------------------------
/* Memory Allocations
	overloaded definitions of new and delete etc
*/

#if defined( D_ALLOC_COUNT )
namespace
{
	static int64 AllocCount = 0;
}

D_INLINE uint64	GetAllocCount()
{
	return AllocCount;
}
#endif

D_FORCE_INLINE
void*
Mem_Alloc
(
	const size_t size
)
{
	if( size > 0 )
	{

#if defined( D_ALLOC_COUNT )
		++AllocCount;
#endif

		return _aligned_malloc( Align( size ), c_Memory_Alignment );
	}
	return nullptr;
}

D_FORCE_INLINE
void
Mem_Free
(
	void *ptr
)
{
	if( ptr != nullptr )
	{
		_aligned_free( ptr );

#if defined( D_ALLOC_COUNT )
		--AllocCount;
		if( AllocCount == 0 )
		{
			printf( "\nSuccess! no leaks \n" );
		}
#endif
	}
}

D_FORCE_INLINE
void*
Mem_ClearedAlloc
(
	const size_t size
)
{
	void* mem = Mem_Alloc( size );
	memset( mem, 0, size );
	return mem;
}

D_FORCE_INLINE
void*
operator new
(
	size_t s
)
{
	return Mem_Alloc( s );
}

D_FORCE_INLINE
void
operator delete
(
	void *p
)
{
	Mem_Free( p );
}

D_FORCE_INLINE
void* operator new[]
(
	size_t s
)
{
	return Mem_Alloc( s );
}

D_FORCE_INLINE
void operator delete[]
(
	void *p
)
{
	Mem_Free( p );
}

#endif // D_ALLOC_HPP