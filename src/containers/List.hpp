/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_LIST_HPP )
#define D_LIST_HPP

#include "..\algorithm\Sort.hpp"
#include "..\memory\Allocation.hpp"
#include "..\memory\ArrayAllocaters.hpp"
#include "..\random\Random.hpp"
#include "ResizePolicies.hpp"

//----------------------------------------------------------------
/* List
	Dynamically resizing c style array with additional useful functions.

	Does not allocate memory until the first item is added.

	Iterating is fast since it is via index / pointer math.
	Adding is fast, as long as no order is needed.
	Removing is slow (if its not from the back).
	Searching is slow(on large data) unless binary search(on a sorted list) is used (see Sort.h)

	NOTE: also known as "List(C#)", "Vector(C++)"

	NOTE: When an entry is "popped off" its dtor will NOT be called, it will be called when the
	container is destroyed or clear(true) has been called (this dramatically improves performance).
		However, this can lead to unwanted effects eg, if you pop_back many entries then resize to more than
	the current size; the expanded entries will be the old entries (that you popped off earlier). However, use of
	push_back would alleviate this problem, since the pushed entries will be assigned the value you pass into the function.
*/
template< typename type_t >
class List
{
public:
	List();
	List( size_t const i_newListSize, type_t const& i_elementsValue );
	List( type_t const* i_start, type_t const* i_end );
	List( List const& i_other );

	~List();

	// B
	type_t const&	back() const;										// returns a reference to the back
	type_t&			back();												// returns a reference to the back
	type_t*			begin();											// returns a pointer to the first element
	type_t const*	begin() const;										// returns a pointer to the first element

	// C
	size_t			capacity() const;									// returns number of elements allocated for
	void			clear( bool const i_andDealloc );					// clear the list
	bool			contains( type_t const& i_obj ) const;				// find the given element _SLOW_

	// D
	void			delete_contents( bool const i_andSetToNULL );		// delete the contents pointed to by the entries in the list

	// E
	type_t&			emplace_back();										// returns reference to a new data element at the end of the list
	bool			empty() const;										// return true if the list is empty
	type_t const*	end() const;										// returns a pointer to the back of the list
	bool			erase( size_t const i_index );						// remove the element at the given index, and move every element after it down one
	bool			erase( type_t const& i_obj );						// remove the element
	bool			erase_fast( size_t const i_index );					// removes the element at the given index and places the last element into its spot - DOES NOT PRESERVE LIST ORDER

	// F
	type_t*			find( type_t const& i_obj ) const;							// find pointer to the given element _SLOW_
	bool			find_index( const type_t& i_obj, size_t& o_index ) const;	// find the index for the given element _SLOW_
	bool			find_null( size_t& o_index ) const;							// find the index for the first NULL pointer in the list _SLOW_
	type_t const&	front() const;

	// I
	bool			insert( type_t const& i_obj, size_t const i_index );	// insert the element at the given index _SLOW_
	size_t			index_of( type_t const* i_obj ) const;					// returns the index for the pointer to an element in the list _RISKY_

	// P
	void			pop_back();												// removes the last element from the container
	void			pop_back( size_t const i_numPops );						// removes the n last elements from the container
	void			push_back( type_t const& i_obj );						// append element
	void			push_back( List const& i_other );						// append list
	void			push_back( type_t const* i_start, type_t const* i_end );// append range

	// R
	void			resize( size_t const i_newsize );						// resizes list to the given number of elements
	void			reserve( size_t const i_numNewElements );				// makes sure there is enough memory for a number of elements
	void			reallocate( size_t const i_newnum );					// set number of elements in list and resize to exactly this number if needed

	// S
	void			shrink_to_fit();									// resizes list to exactly the number of elements it contains
	void			shuffle( Random::RNG& ran );						// apply shuffle to list
	size_t			size() const;										// returns number of elements in list
	void			sort( const Algorithm::SortBase<type_t>& i_sort = Algorithm::Sort_QuickDefault<type_t>() ); // sort the list

	// Misc functions
	size_t			_allocated() const;									// returns total size of allocated memory
	size_t			_size() const;										// returns total size of allocated memory including size of list type_t
	size_t			_memory_used() const;								// returns size of the used elements in the list

	List<type_t>&		operator=( List<type_t>const& i_other ); // _SLOW_
	type_t const&		operator[]( size_t i_index ) const;
	type_t&				operator[]( size_t i_index );

private:
	void	Init();
	void	CheckResize();

	type_t*		m_array;
	size_t		m_size;
	size_t		m_capacity;
};

#include "List.inl"

#endif // D_LIST_HPP