/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_QUEUE_HPP )
#define D_QUEUE_HPP

//----------------------------------------------------------------
/* Queue
	A queue, internally it is a cyclical / ring buffer.
	Using the template arg you can decide if pushing a new entry
	should A; resize the container, or B; overwrite a previous entry

	NOTE: also know as a FIFO container.
*/
template <typename type_t, bool overWrite = false >
class Queue
{
public:
	Queue();

	Queue( Queue const& i_other);
	Queue( type_t const* i_start, type_t const* i_end );
	~Queue();

	// B
	type_t&			back();
	type_t const&	back() const;

	// C
	size_t	capacity() const;
	void	clear( bool const i_deallocate );

	// E
	type_t&	emplace_back();
	bool	empty() const;

	// F
	type_t&			front();
	type_t const&	front() const;
	bool			full() const;

	// P
	void push_back( type_t const& i_item );
	void pop_front();

	// R
	void reserve(size_t const i_newMinSize);
	void reallocate(size_t const i_capacity);

	// S
	size_t	size() const;

	// Misc functions
	size_t			_allocated() const;			// returns total size of allocated memory
	size_t			_size() const;				// returns total size of allocated memory including size of list type_t
	size_t			_memory_used() const;		// returns size of the used elements in the list

	Queue &operator=(Queue<type_t, overWrite> const& i_other);

private:
	void	Init();

	type_t*		m_array;
	size_t		m_capacity;
	size_t		m_size;
	size_t		m_max_index;
	size_t		m_min_index;
};

#include "Queue.inl"

#endif // end of D_QUEUE_HPP