/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_HEAP_HPP )
#define D_HEAP_HPP

#if !defined( D_FINAL )
	#define D_CHECK_VAILD_HEAP
#endif

#include "HeapTypes.hpp"

//----------------------------------------------------------------
/* Heap
	Like a binary tree but sorted. Also it doesn't have to be
	binary.

	NOTE: Also known as "priority_queue"

	NOTES: n is the number of child nodes per node.
	the greater n is the wider the heap will be (apposed to smaller the deeper)
	Width is good to avoid swaps in the pop functions. However, this will incur more comparisons. (use when swaps are costly)
	Depth is good to avoid comparisons. However, this will incur more swaps. (use when swaps are not costly).
	In addition keep in mine that n is uses as a loop counter, so I'd recommend having it small enough to be unrolled by the compiler.
*/
template< typename type, typename cmp = max_heap >
class Heap
{
	typedef Heap<type,cmp> HeapType;
	enum { default_capacity = 64 };

public:
	Heap();
	~Heap();

	Heap( HeapType const& i_other );
	Heap( type const* i_start, type const* i_end ); // Batch insert, O(n)

	// C
	size_t capacity() const;
	void clear( bool const i_deallocate );

	// E
	bool empty() const;

	// P
	void push( type const& i_value );	// O(log n)
	void pop();							// O(log n)

	// R
	void reserve( size_t const i_minSize );
	void reallocate( size_t const i_newCapactiy );

	// T
	type const& top() const;

	// S
	size_t size() const;

	// Misc
	type&				operator[]( size_t i_index ); // be careful to not damage the heap property, it is only checked in debug
	type const&			operator[]( size_t i_index ) const;
	HeapType&			operator=( HeapType const& i_other );

private:
	void Init();
	void CheckResize();

#ifdef D_CHECK_VAILD_HEAP
	void CheckHeap();
#endif

	size_t FirstChildIndex( size_t const i_index ) const;
	size_t ParentIndex( size_t const i_nodeIndex ) const;
	
	type*	m_array;
	size_t	m_size;
	size_t	m_capacity;
};

#include "Heap.inl"

#endif // D_HEAP_HPP