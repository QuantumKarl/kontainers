/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#include <queue>
using std::priority_queue;

HeapVSpriority_queue::HeapVSpriority_queue()
{
#if defined( D_FULL_TEST )
	m_loopCount.SetOuterLoopAtIndex(0,10); // RunA first loop
	m_loopCount.SetInnerLoopAtIndex(0,128); // RunA second (first inner) loop

	m_loopCount.SetArgumentAtIndex(0,1<<17); // 65K stringsTest arg in RunA
	m_loopCount.SetArgumentAtIndex(1,Min( Primes::NumPrimes(), SourceData::randomFloats.size() ) );

	m_loopCount.SetCyclesAndRunsArgs(5,4);
	//m_loopCount.SetCyclesAndRunsArgs(1,1);
#else
	m_loopCount.SetOuterLoopAtIndex(0,1); // RunA first loop
	m_loopCount.SetInnerLoopAtIndex(0,64); // RunA second (first inner) loop

	m_loopCount.SetArgumentAtIndex(0,1<<8); // 65K stringsTest arg in RunA
	m_loopCount.SetArgumentAtIndex(1,Min( Primes::NumPrimes(), SourceData::randomFloats.size() ) );

	m_loopCount.SetCyclesAndRunsArgs(1,1);
#endif
}

#pragma region Heap
//----------------------------------------------------------------
void HeapVSpriority_queue::HeapNumbers( size_t const size )
{
	assert( size <= Primes::NumPrimes() );
	assert( size <= SourceData::randomFloats.size() );

	// assumes primes and random floats are sorted in ascending order
	assert( Primes::PrimeAt(0) < Primes::PrimeAt(1) );
	assert( SourceData::randomFloats.at(0) < SourceData::randomFloats.at(1) );

	Heap<real_t,max_heap> h;// max heap, insert smallest numbers first (to force it to do most work)

	// push floats (between 0 and 1, all less then the lowest prime)
	for(size_t i = 0; i < size; ++i)
	{
		h.push( static_cast<real_t>( SourceData::randomFloats.at( static_cast<size_t>(i) ) ) );
	}

	// push primes
	for(size_t i = 0; i < size; ++i)
	{
		h.push( static_cast<real_t>( Primes::PrimeAt( static_cast<size_t>(i) ) ) );
	}

	real_t sum = 0.0;

	// take out the values and check they are in descending order (opposite order to primes list)
	for(int64 i = size-1; i >= 0; --i)
	{
		// check we have the correct value on the top
		if( h.top() == static_cast<real_t>( Primes::PrimeAt( static_cast<size_t>(i) ) ) ) //-V550
		{
			sum += h.top() / (static_cast<real_t>(size));
			// correct! take it off the heap
			h.pop();
		}
		else
		{
			cout << "FAILURE! INCORRECT ORDER IN HEAP (PRIMES)" << endl;
			return;
		}
	}

	// take out the values and check they are in descending order (opposite order to floats list)
	for(int64 i = size-1; i >= 0; --i)
	{
		// check we have the correct value on the top
		if( h.top() == static_cast<real_t>( SourceData::randomFloats.at( static_cast<size_t>(i) ) ) ) //-V550
		{
			sum += h.top() / (static_cast<real_t>(size));
			// correct! take it off the heap
			h.pop();
		}
		else
		{
			cout << "FAILURE! INCORRECT ORDER IN HEAP (FLOATS)" << endl;
			return;
		}
	}

	if( h.empty() == true )
	{
		// success
		//cout << sum << endl;
	}
}

//----------------------------------------------------------------
void HeapVSpriority_queue::HeapStrings( size_t const size )
{
	assert( size <= SourceData::allWords.size() );

	Heap<string> h;

	// insert all words from the front to back(to force container to do most work)
	for(size_t i = 0; i < size; ++i )
	{
		h.push( SourceData::allWords[i] );
	}

	// take out the entries while checking they are in the correct order.
	for(int64 i = size-1; i >= 0; --i)
	{
		// check we have the matching value on the top of the heap
		if( h.top() == SourceData::allWords[static_cast<size_t>(i)] )
		{
			// correct! remove it from the heap
			h.pop();
		}
		else
		{
			cout << "FAILURE! INCORRECT ORDER IN HEAP (STRINGS)" << endl;
			return;
		}
	}
}

//----------------------------------------------------------------
// some what slower, since the data can not be moved into L1 cache
// but uses less memory
void HeapVSpriority_queue::HeapStringsPtr( size_t const size )
{
	assert( size <= SourceData::allWords.size() );

	Heap<string*,max_heap_ptr> h;

	// insert all words from the front to back(to force container to do most work)
	for(size_t i = 0; i < size; ++i )
	{
		h.push( &SourceData::allWords[i] );
	}

	// take out the entries while checking they are in the correct order.
	for(int64 i = size-1; i >= 0; --i)
	{
		// check we have the matching value on the top of the heap
		if( *h.top() == SourceData::allWords[static_cast<size_t>(i)] )
		{
			// correct! remove it from the heap
			h.pop();
		}
		else
		{
			cout << "FAILURE! INCORRECT ORDER IN HEAP (STRINGS)" << endl;
			return;
		}
	}
}
#pragma endregion

#pragma region priority_queue
//----------------------------------------------------------------
void HeapVSpriority_queue::priority_queueNumbers( size_t const size )
{
	assert( size <= Primes::NumPrimes() );
	assert( size <= SourceData::randomFloats.size() );

	// assumes primes and random floats are sorted in ascending order
	assert( Primes::PrimeAt(0) < Primes::PrimeAt(1) );
	assert( SourceData::randomFloats.at(0) < SourceData::randomFloats.at(1) );

	priority_queue<real_t> h;// max heap by default, insert smallest numbers first (to force it to do most work)

	// push floats (between 0 and 1, all less then the lowest prime)
	for(size_t i = 0; i < size; ++i)
	{
		h.push( static_cast<real_t>( SourceData::randomFloats.at( static_cast<size_t>(i) ) ) );
	}

	// push primes
	for(size_t i = 0; i < size; ++i)
	{
		h.push( static_cast<real_t>( Primes::PrimeAt( static_cast<size_t>(i) ) ) );
	}

	real_t sum = 0.0;

	// take out the values and check they are in descending order (opposite order to primes list)
	for(int64 i = size-1; i >= 0; --i)
	{
		// check we have the correct value on the top
		if( h.top() == static_cast<real_t>( Primes::PrimeAt( static_cast<size_t>(i) ) ) )
		{
			sum += h.top() / (static_cast<real_t>(size));
			// correct! take it off the heap
			h.pop();
		}
		else
		{
			cout << "FAILURE! INCORRECT ORDER IN HEAP (PRIMES)" << endl;
			return;
		}
	}

	// take out the values and check they are in descending order (opposite order to floats list)
	for(int64 i = size-1; i >= 0; --i)
	{
		// check we have the correct value on the top
		if( h.top() == static_cast<real_t>( SourceData::randomFloats.at( static_cast<size_t>(i) ) ) )
		{
			sum += h.top() / (static_cast<real_t>(size));
			// correct! take it off the heap
			h.pop();
		}
		else
		{
			cout << "FAILURE! INCORRECT ORDER IN HEAP (FLOATS)" << endl;
			return;
		}
	}

	if( h.empty() == true )
	{
		// success
		// cout << sum << endl;
	}
}

//----------------------------------------------------------------
void HeapVSpriority_queue::priority_queueStrings( size_t const size )
{
	assert( size <= SourceData::allWords.size() );

	priority_queue<string> h;

	// insert all words from the front to back(to force container to do most work)
	for(size_t i = 0; i < size; ++i )
	{
		h.push( SourceData::allWords[i] );
	}

	// take out the entries while checking they are in the correct order.
	for(int64 i = size-1; i >= 0; --i)
	{
		// check we have the matching value on the top of the heap
		if( h.top() == SourceData::allWords[static_cast<size_t>(i)] )
		{
			// correct! remove it from the heap
			h.pop();
		}
		else
		{
			cout << "FAILURE! INCORRECT ORDER IN HEAP (STRINGS)" << endl;
			return;
		}
	}
}
#pragma endregion

//----------------------------------------------------------------
void HeapVSpriority_queue::HeapRunA()
{
	for(size_t i = 0; i < m_loopCount.GetOuterLoopA(); ++i )
	{
		HeapStrings( m_loopCount.GetArgumentA() );
		for(size_t j = 0; j < m_loopCount.GetInnerLoopA(); ++j )
		{
			HeapNumbers( m_loopCount.GetArgumentB() );
		}
	}
}

//----------------------------------------------------------------
void HeapVSpriority_queue::priority_queueRunA()
{
	for(size_t i = 0; i < m_loopCount.GetOuterLoopA(); ++i )
	{
		priority_queueStrings( m_loopCount.GetArgumentA() );
		for(size_t j = 0; j < m_loopCount.GetInnerLoopA(); ++j )
		{
			priority_queueNumbers( m_loopCount.GetArgumentB() );
		}
	}
}

//----------------------------------------------------------------
int float_cmp( void const* i_a, void const * i_b )
{
	float const a = *static_cast<float const*>(i_a);
	float const b = *static_cast<float const*>(i_b);
	if( a < b )
	{
		return -1;
	}
	else if( a > b )
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

//----------------------------------------------------------------
void HeapVSpriority_queue::RunHeapTests()
{
	HeapVSpriority_queue heapTest;
	TestUtils::testCode("Karl Heap Test",
						[&](){heapTest.HeapRunA();},
						heapTest.GetLoopCount().GetRunsArgument(),
						heapTest.GetLoopCount().GetCyclesArgument() );
}

//----------------------------------------------------------------
void HeapVSpriority_queue::Runpriority_queueTests()
{
	HeapVSpriority_queue heapTest;
	TestUtils::testCode("STL priority_queue Test",
						[&](){heapTest.priority_queueRunA();},
						heapTest.GetLoopCount().GetRunsArgument(),
						heapTest.GetLoopCount().GetCyclesArgument() );
}