/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_SYS_TYPES_H )
#define D_SYS_TYPES_H
//----------------------------------------------------------------
//			Contains types used throughout the engine.
// NOTE: keep this down to simple types and defines. Do NOT add code.

typedef unsigned char		byte;		// 8 bits
typedef unsigned short		word;		// 16 bits
typedef unsigned int		dword;		// 32 bits
typedef unsigned long long	qword;		// 64 bits

typedef unsigned int		uint;
typedef unsigned long		ulong;

typedef signed char			int8;
typedef unsigned char		uint8;

typedef short int			int16;
typedef unsigned short int	uint16;

typedef int					int32;
typedef unsigned int		uint32;

typedef long long			int64;
typedef unsigned long long	uint64;

// The C/C++ standard guarantees the size of an unsigned type is the same as the signed type.
// The exact size in bytes of several types is guaranteed here.
assert_sizeof( bool,	1 );
assert_sizeof( char,	1 );
assert_sizeof( short,	2 );
assert_sizeof( int,		4 );
assert_sizeof( float,	4 );
assert_sizeof( double,	8 );

assert_sizeof( byte,	1 );
assert_sizeof( word,	2 );
assert_sizeof( dword,	4 );
assert_sizeof( qword,	8 );

assert_sizeof( int8,	1 );
assert_sizeof( uint8,	1 );

assert_sizeof( int16,	2 );
assert_sizeof( uint16,	2 );

assert_sizeof( int32,	4 );
assert_sizeof( uint32,	4 );

assert_sizeof( int64,	8 );
assert_sizeof( uint64,	8 );

#if defined( D_64BIT )
	assert_sizeof( size_t,	8 );
	typedef double real_t;
#elif defined( D_32BIT )
	assert_sizeof( size_t,	4 );
	typedef float real_t;
#else
#error Uknown platform
#endif

#endif // end of D_SYS_TYPES