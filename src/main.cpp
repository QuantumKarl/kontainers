/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */
#if 0
	#define D_WRITE_TO_FILE
#endif

#if 0
	// toggle full test, or practice run
	#define D_FULL_TEST
#endif

#if 1
	// testing Karl's Kontainers
	#define D_KARL_TEST
#else
	// testing STL containers
	#define D_STL_TEST
#endif

#include "sys\sys_defines.hpp"
#include "sys\sys_includes.hpp"
#include "sys\sys_assert.hpp"
#include "sys\sys_types.hpp"

#if defined(D_KARL_TEST)
	#include "sys\sys_asmlib.hpp"
#endif
#include "sys\sys_global.hpp"

#if defined(D_KARL_TEST)
	// only use overridden allocators for KARL test
	#include "memory\Allocation.hpp"
#endif
#include "memory\ArrayAllocaters.hpp"

#include <Windows.h>
#pragma comment(lib,"winmm.lib")
#include <iostream>
#include <sstream>
#include <fstream>
#include <stack>
#include <cmath>
#include <cstdlib>
#include <vector>
using namespace std;

#include "containers\cString.hpp"
#include "random\Random.hpp"

#include "algorithm\Shuffle.hpp"
#include "algorithm\Sort.hpp"
#include "misc\Primes.hpp"
#include "hashes\MurmurHash2.hpp"

#include "containers\Array.hpp"
#include "containers\List.hpp"
#include "containers\ListFixed.hpp"
#include "containers\Queue.hpp"
#include "containers\Heap.hpp"

#include "containers\Str.hpp"
#include "containers\Map.hpp"
#include "containers\Set.hpp"

#include "tests\Testbed_Utils.hpp"
#include "tests\SourceData.hpp"

#include "example.hpp"
#include "tests\loopCount.hpp"
#include "tests\stringTests.hpp"
#include "tests\AgnerTests.hpp"

#include "tests\ListVsVec.hpp"
#include "tests\MapVSunordered_map.hpp"
#include "tests\HeapVSpriority_queue.hpp"
#include "tests\QueueVSqueue.hpp"
#include "tests\StrVSstring.hpp"

int main()
{
	TestUtils::writeInfo();

	Primes::InitStatic( 3000000 ); // 216,816 primes
	SourceData::InitStatic();

// if running examples (functionality tests)
#if 0
	container_examples::MapExample();
	container_examples::SetExample();
	container_examples::ListExample();
	container_examples::ListFixedExample();
	container_examples::ArrayExample();
	container_examples::HeapExample();
	container_examples::QueueExample();
	stringTests::StringTest();
#else
// running benchmarks
	#if defined(D_KARL_TEST)
		StrVSstring::RunStrTests();
		ListVsVec::RunListTests();
		MapVSunordered_map::RunMapTests();
		QueueVSqueue::RunQueueTests();

		// needs to be last since the data needs to be sorted
		qsort( &SourceData::randomFloats[0], SourceData::randomFloats.size(), sizeof(float), float_cmp );
		HeapVSpriority_queue::RunHeapTests();
	#else
		StrVSstring::RunstringTests();
		ListVsVec::RunVecTests();
		MapVSunordered_map::Rununordered_mapTests();
		QueueVSqueue::RunqueueTests();

		// needs to be last since the data needs to be sorted
		qsort( &SourceData::randomFloats[0], SourceData::randomFloats.size(), sizeof(float), float_cmp );
		HeapVSpriority_queue::Runpriority_queueTests();
	#endif
#endif

	SourceData::FiniStatic();
	Primes::FiniStatic();
	TestUtils::writeEndtime();
	cout << "\nTests Completed" << endl;
#if defined( D_ALLOC_COUNT )
	cout << "AllocCount: " << GetAllocCount() << endl;
#endif

	//testAgner();

#if !defined( D_FINAL )
	char key(0); cin >> key;
#endif
	return 0;
}