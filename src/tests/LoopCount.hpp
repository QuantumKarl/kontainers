/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_LOOP_COUNT )
#define D_LOOP_COUNT

//----------------------------------------------------------------
// Loop Count is a container to store loop counters
const size_t loopCountCapacity	= 8;
const size_t argCapacity		= 8;

static_assert(loopCountCapacity >= 4, "must have enough space for four or more entries" );
static_assert(argCapacity >= 4, "must have enough space for four or more entries" );

class LoopCount
{
public:
	LoopCount()
	{
		size_t const loopCountInBytes	= sizeof(size_t)*loopCountCapacity;
		size_t const argsInBytes		= sizeof(size_t)*argCapacity;
		memset(&m_outerLoop[0],	0,	loopCountInBytes);
		memset(&m_innerLoop[0],	0,	loopCountInBytes);
		memset(&m_args[0],		0,	argsInBytes);
		m_cyclesArg	= 0;
		m_runsArg	= 0;
	}

	size_t GetOuterLoopA() const { return m_outerLoop[0]; }
	size_t GetOuterLoopB() const { return m_outerLoop[1]; }
	size_t GetOuterLoopC() const { return m_outerLoop[2]; }
	size_t GetOuterLoopD() const { return m_outerLoop[3]; }
	size_t GetOuterLoopAtIndex( size_t const i_index ) const { assert( i_index < loopCountCapacity); return m_outerLoop[i_index]; }
	void SetOuterLoopAtIndex( size_t const i_index, size_t const i_value ) { assert( i_index < loopCountCapacity); m_outerLoop[i_index] = i_value; }

	size_t GetInnerLoopA() const { return m_innerLoop[0]; }
	size_t GetInnerLoopB() const { return m_innerLoop[1]; }
	size_t GetInnerLoopC() const { return m_innerLoop[2]; }
	size_t GetInnerLoopD() const { return m_innerLoop[3]; }
	size_t GetInnerLoopAtIndex( size_t const i_index ) const { assert( i_index < loopCountCapacity); return m_innerLoop[i_index]; }
	void SetInnerLoopAtIndex( size_t const i_index, size_t const i_value ) { assert( i_index < loopCountCapacity); m_innerLoop[i_index] = i_value; }

	size_t GetArgumentA() const { return m_args[0]; }
	size_t GetArgumentB() const { return m_args[1]; }
	size_t GetArgumentC() const { return m_args[2]; }
	size_t GetArgumentD() const { return m_args[3]; }
	size_t GetArgumentE() const { return m_args[4]; }
	size_t GetArgumentAtIndex( size_t const i_index ) const {assert( i_index < argCapacity ); return m_args[i_index]; }
	void SetArgumentAtIndex( size_t const i_index, size_t const i_value ) { assert( i_index < argCapacity ); m_args[i_index] = i_value; }

	size_t GetCyclesArgument()	const { return m_cyclesArg; }
	size_t GetRunsArgument()	const { return m_runsArg; }
	void SetCyclesAndRunsArgs( size_t const i_cycles, size_t const i_runs ) { m_cyclesArg = i_cycles; m_runsArg = i_runs; }

private:
	size_t m_outerLoop[loopCountCapacity];
	size_t m_innerLoop[loopCountCapacity];
	size_t m_args[argCapacity];
	size_t m_cyclesArg;
	size_t m_runsArg;
};

#endif