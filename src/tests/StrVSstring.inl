/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

//----------------------------------------------------------------
// Init our magic numbers for the loops and args for the tests
StrVSstring::StrVSstring()
{
#if defined( D_FULL_TEST )
	m_loopCount.SetOuterLoopAtIndex(0,2);	// RunA outer loop (expanding)
	m_loopCount.SetArgumentAtIndex(0, SourceData::allWordsStr.size() ); // Test1 and Test2 arg

	m_loopCount.SetOuterLoopAtIndex(1,4);	// RunB outer loop (replacing)
	m_loopCount.SetArgumentAtIndex(1,4000);	// Test2 and Test3 arg

	m_loopCount.SetCyclesAndRunsArgs(2,3);
	//m_loopCount.SetCyclesAndRunsArgs(1,1);
#else
	m_loopCount.SetOuterLoopAtIndex(0,1);	// RunA outer loop (expanding)
	m_loopCount.SetArgumentAtIndex(0,10);	// Test1 arg

	m_loopCount.SetOuterLoopAtIndex(1,1);	// RunB outer loop (replacing)
	m_loopCount.SetArgumentAtIndex(1,10);	// Test 2 and Test3 arg

	m_loopCount.SetCyclesAndRunsArgs(1,1);
#endif
}

//----------------------------------------------------------------
// Karl Str Tests
//--------------------------------
/*
*/
void StrVSstring::StrTest1( size_t const i_size )
{
	assert( i_size <= SourceData::allWords.size() );

	Str str1;

	// build up a single string containing all the strings out of "allWords"
	for( size_t i = 0; i < i_size; ++i )
	{
		str1.push_back( SourceData::allWordsStr[i] );
		str1.push_back( ' ' );
	}

	// ensure we are only using as much memory as we actually need
	str1.shrink_to_fit();

	// now check each string out of "allWords" exists in str1
	for( size_t i = 0; i < i_size; ++i )
	{
		if( str1.contains( SourceData::allWordsStr[i] ) == false )
		{
			cout << "ERROR IN STR CONTAINS TEST" << endl;
			return;
		}
	}

	// erase the first word for all the words
	for( size_t i = 0; i < i_size; ++i )
	{
		str1.erase(0,SourceData::allWordsStr[i].size()+1);
	}
}

//--------------------------------
/*
*/
void StrVSstring::StrTest2( size_t const i_size )
{
	assert( i_size <= SourceData::allWords.size() );

	Str str1;

	// build up a single string containing all the strings out of "allWords"
	for( size_t i = 0; i < i_size; ++i )
	{
		str1.push_back( SourceData::allWordsStr[i] );
		str1.push_back( ' ' );
	}

	// ensure we are only using as much memory as we actually need
	str1.shrink_to_fit();

	// find 'A' and replace with 'B',find 'B' and replace with 'C',find 'D' and replace with 'C'
	// this Test has high number of matches but the length of the string remains the same
	for( char i = 'A'; i < 'Z'; ++i )
	{
		str1.replace(i, i+1);
	}

	// replace every Z with an underscore
	str1.replace( 'Z', '_' );
}

//--------------------------------
/*
*/
void StrVSstring::StrTest3( size_t const i_size )
{
	assert( i_size <= SourceData::allWords.size() );

	Str str1;

	// build up a single string containing all the strings out of "allWords"
	for( size_t i = 0; i < i_size; ++i )
	{
		str1.push_back( SourceData::allWordsStr[i] );
		str1.push_back( ' ' );
	}

	// ensure we are only using as much memory as we actually need
	str1.shrink_to_fit();

	// in the string "jim bob fred" find "jim" and replace with "bob"
	// giving you "bob bob fred", now find "bob" and replace with "fred"
	// giving you "fred fred fred", this is the find and replace pattern used below.
	// this has a high number of matches (quantity and length) and total length of the string
	// will change with every replace, also note: this will match with sub words making it even
	// more expensive
	size_t const max = i_size-1;
	for( size_t i = 0; i < max; ++i )
	{
		str1.replace( SourceData::allWordsStr[i], SourceData::allWordsStr[i+1] );
	}
}
//----------------------------------------------------------------

//----------------------------------------------------------------
// STL string test

//--------------------------------
/*	STL_replace
	STL string does not have a find and replace therefore this has been added,
	it has the same algorithm as Str replace, just using STL string function
*/
void STL_replace(string& str, string const& oldStr, string const& newStr)
{
	size_t pos = 0;
	while( (pos = str.find(oldStr, pos)) != std::string::npos)
	{
		str.replace(pos, oldStr.length(), newStr);
		pos += newStr.length();
	}
}

//--------------------------------
/*
*/
void StrVSstring::stringTest1( size_t const i_size )
{
	assert( i_size <= SourceData::allWords.size() );

	string str1;

	// build up a single string containing all the strings out of "allWords"
	for( size_t i = 0; i < i_size; ++i )
	{
		str1.append( SourceData::allWords[i] );
		str1.push_back( ' ' );
	}

	// ensure we are only using as much memory as we actually need
	str1.shrink_to_fit();

	// now check each string out of "allWords" exists in str1
	for( size_t i = 0; i < i_size; ++i )
	{
		if( str1.find( SourceData::allWords[i] ) == string::npos )
		{
			cout << "ERROR IN STR CONTAINS TEST" << endl;
			return;
		}
	}

	// erase the first word for all the words
	for( size_t i = 0; i < i_size; ++i )
	{
		str1.erase(0,SourceData::allWordsStr[i].size()+1);
	}
}

//--------------------------------
/*
*/
void StrVSstring::stringTest2( size_t const i_size )
{
	assert( i_size <= SourceData::allWords.size() );

	string str1;

	// build up a single string containing all the strings out of "allWords"
	for( size_t i = 0; i < i_size; ++i )
	{
		str1.append( SourceData::allWords[i] );
		str1.push_back( ' ' );
	}

	// ensure we are only using as much memory as we actually need
	str1.shrink_to_fit();

	// find 'A' and replace with 'B',find 'B' and replace with 'C',find 'D' and replace with 'C'
	// this Test has high number of matches but the length of the string remains the same
	string a,b;
	for( char i = 'A'; i < 'Z'; ++i )
	{
		a.clear();
		b.clear();
		a.push_back(i);
		b.push_back(i+1);
		STL_replace(str1, a,b );
	}

	// replace every Z with an underscore
	STL_replace( str1, "Z","_" );
}

//--------------------------------
/*
*/
void StrVSstring::stringTest3( size_t const i_size )
{
	assert( i_size <= SourceData::allWords.size() );

	string str1;

	// build up a single string containing all the strings out of "allWords"
	for( size_t i = 0; i < i_size; ++i )
	{
		str1.append( SourceData::allWords[i] );
		str1.push_back( ' ' );
	}

	// ensure we are only using as much memory as we actually need
	str1.shrink_to_fit();

	// in the string "jim bob fred" find "jim" and replace with "bob"
	// giving you "bob bob fred", now find "bob" and replace with "fred"
	// giving you "fred fred fred", this is the find and replace pattern used below.
	// this has a high number of matches (quantity and length) and total length of the string
	// will change with every replace, also note: this will match with sub words making it even
	// more expensive
	size_t const max = i_size-1;
	for( size_t i = 0; i < max; ++i )
	{
		STL_replace( str1, SourceData::allWords[i], SourceData::allWords[i+1] );
	}
}
//----------------------------------------------------------------

//--------------------------------
/*
*/
void StrVSstring::StrRunA()
{
	for(size_t i = 0; i < m_loopCount.GetOuterLoopA(); ++i )
	{
		StrTest1( m_loopCount.GetArgumentA() );// approx 70 seconds
	}
}

//--------------------------------
/*
*/
void StrVSstring::StrRunB()
{
	for(size_t i = 0; i < m_loopCount.GetOuterLoopB(); ++i )
	{
		StrTest2( m_loopCount.GetArgumentB() );
		StrTest3( m_loopCount.GetArgumentB() );
	}
}

//--------------------------------
/*
*/
void StrVSstring::stringRunA()
{
	for(size_t i = 0; i < m_loopCount.GetOuterLoopA(); ++i )
	{
		stringTest1( m_loopCount.GetArgumentA() );
	}
}

//--------------------------------
/*
*/
void StrVSstring::stringRunB()
{
	for(size_t i = 0; i < m_loopCount.GetOuterLoopB(); ++i )
	{
		stringTest2( m_loopCount.GetArgumentB() );
		stringTest3( m_loopCount.GetArgumentB() );
	}
}

//--------------------------------
/*
*/
void StrVSstring::RunStrTests()
{
	StrVSstring strTests;
	TestUtils::testCode("Karl Str Expanding test",
						[&](){strTests.StrRunA();},
						strTests.GetLoopCount().GetRunsArgument(), //-V807
						strTests.GetLoopCount().GetCyclesArgument() );

	TestUtils::testCode("Karl Str Replacing test",
						[&](){strTests.StrRunB();},
						strTests.GetLoopCount().GetRunsArgument(),
						strTests.GetLoopCount().GetCyclesArgument());
}

//--------------------------------
/*
*/
void StrVSstring::RunstringTests()
{
	StrVSstring stringTests;
	TestUtils::testCode("STL string Expanding test",
						[&](){stringTests.stringRunA();},
						stringTests.GetLoopCount().GetRunsArgument(), //-V807
						stringTests.GetLoopCount().GetCyclesArgument() );

	TestUtils::testCode("STL string Replacing test",
						[&](){stringTests.stringRunB();},
						stringTests.GetLoopCount().GetRunsArgument(),
						stringTests.GetLoopCount().GetCyclesArgument() );
}