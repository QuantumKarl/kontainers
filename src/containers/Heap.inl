/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

//--------------------------------
/* default constructor

*/
template< typename type, typename cmp >
D_FORCE_INLINE
Heap<type,cmp>::Heap
(
):
	m_array(nullptr),
	m_size(0),
	m_capacity(0)
{
	Init();
}

//--------------------------------
/* default destructor

*/
template< typename type, typename cmp >
Heap<type,cmp>::~Heap
(
)
{
	clear( true );
}

//--------------------------------
/* copy constructor
	Creates a heap by copying another heap
*/
template< typename type, typename cmp >
D_FORCE_INLINE
Heap<type,cmp>::Heap
(
	HeapType const& i_other
):
	m_array(nullptr),
	m_size(0),
	m_capacity(0)
{
	*this = i_other;
}

//--------------------------------
/* interator style constructor
	Similar to copy but takes a range,
	i_start and i_end

	NOTE: i_end must equal array[size] not
	array[size-1].

	NOTE: inserting elements this way is faster
	than calling push many times.

	NOTE: assumes you are copying not from a heap
*/
template< typename type, typename cmp >
Heap<type,cmp>::Heap
(
	type const* i_start,
	type const* i_end
):
	m_array(nullptr),
	m_size(0),
	m_capacity(0)
{
	assert( i_start	!= nullptr &&
			i_end	!= nullptr );

	size_t const count = ptr_range( i_start, i_end );

	// Alloc memory
	reserve( count );

	// Add each element to our array
	ArrayCopy(m_array,i_start,count);

	// update the size
	m_size = count;

	// heapify O(n)
	for ( size_t i = count / 2; i > 0; --i )
	{
		// Sift down
		size_t parent = i - 1;
		size_t child = FirstChildIndex(parent);

		while( child < count )
		{
			// look at all children and find smallest
			if( (child+1) < m_size	&&
				cmp::valid( m_array[child+1], m_array[child] ) )
			{
				++child;
			}

			if ( cmp::valid( m_array[child], m_array[parent] ) )// use provided compare
			{
				SwapValues( m_array[parent], m_array[child] );
				parent = child;
				child = FirstChildIndex(parent);
			}
			else
			{
				break;
			}
		};
	}

#ifdef D_CHECK_VAILD_HEAP
	CheckHeap();
#endif
}

//--------------------------------
/* capacity
	Returns the size of the array allocated.

	This is the number of elements the heap
	could accommodate before resizing.
*/
template< typename type, typename cmp >
D_FORCE_INLINE
size_t
Heap<type,cmp>::capacity
(
) const
{
	return m_capacity;
}

//--------------------------------
/* clear( i_deallocate )
	Clears the container.

	If i_deallocate is true,
	it will also deallocate memory
	else it will not deallocate memory

	NOTE: the entries do not get their
	dtors called unless the memory is deallocated.
*/
template< typename type, typename cmp >
D_INLINE
void
Heap<type,cmp>::clear
(
	bool const i_deallocate
)
{
	if( i_deallocate == true )
	{
		if ( m_array != nullptr )
		{
			ArrayDelete< type >(m_array, m_capacity);
			m_array	= nullptr;
		}
		m_size		= 0;
		m_capacity	= 0;
	}
	else
	{
		m_size		= 0;
	}
}

//--------------------------------
/* empty
	Returns true if the container is empty,
	else it returns false.
*/
template< typename type, typename cmp >
D_FORCE_INLINE
bool
Heap<type,cmp>::empty
(
) const
{
	return m_size == 0;
}

//--------------------------------
/* push
	Adds a value to the heap.

	NOTE: It makes sure its a valid heap.
	No further functions need to be called.

	NOTE: O( log n)
*/
template< typename type, typename cmp >
D_INLINE
void
Heap<type,cmp>::push
(
	type const& i_value
)
{

	//Add the new value to the back of the heap
	m_array[m_size] = i_value;
	// delay the incrementation of m_size so we can avoid doing m_size-1 below

	if( m_size != 0 )
	{
		// Fix heap property via sifting up
		size_t child = m_size;
		++m_size;// can now inc m_size
		CheckResize();
		size_t parent = 0;
		do
		{
			parent = ParentIndex(child);

			// check if the heap property is broken for this child / parent
			if ( cmp::invalid( m_array[parent], m_array[child] ) )// use provided compare
			{
				SwapValues( m_array[child], m_array[parent] );
				child = parent;
			}
			else// heap property not broken, completed sifting
			{
				break;
			}
		}
		while( child != 0 );
	}
	else
	{
		++m_size;
	}

#ifdef D_CHECK_VAILD_HEAP
	CheckHeap();
#endif
}

//--------------------------------
/* pop
	Removes the top value.
*/
template< typename type, typename cmp >
D_INLINE
void
Heap<type,cmp>::pop
(
)
{
	assert( m_size > 0 );

	--m_size;
	if( m_size > 1 )
	{
		// Sift down
		SwapValues( m_array[0], m_array[m_size] );
		size_t parent = 0;
		size_t child = FirstChildIndex(parent);
		do
		{
			// look at the children and find smallest
			if( (child+1) < m_size	&&
				cmp::valid( m_array[child+1], m_array[child] ) )
			{
				++child;
			}

			if ( cmp::valid( m_array[child], m_array[parent] ) )// use provided compare
			{
				SwapValues( m_array[parent], m_array[child] );
				parent = child;
				child = FirstChildIndex(parent);
			}
			else
			{
				break;
			}
		}
		while( child < m_size );
	}
	else if( m_size == 1 )
	{
		SwapValues( m_array[0], m_array[m_size] );
	}
	// else( size == 0 ) so do nothing

#ifdef D_CHECK_VAILD_HEAP
	CheckHeap();
#endif
}

//--------------------------------
/* reserve( i_minSize )
	Make sure the container can accommodate at least,
	i_minSize number of elements.

	NOTE: will allocate memory first call
*/
template< typename type, typename cmp >
D_FORCE_INLINE
void
Heap<type,cmp>::reserve
(
	size_t const i_minSize
)
{
	// if needs to expand
	if( i_minSize > m_capacity )
	{
		// get next size
		size_t const nextSize = ResizePolicy<type>::Reserve( i_minSize );
		reallocate( nextSize );
	}
	// else big enough already
}

//--------------------------------
/* reallocate( i_newCapacity )
	Allocates memory for the amount of elements requested while keeping the contents intact.
	Contents are copied using their = operator so that data is correctly instantiated.

	eg if you know you are not going to use more that 256 elements,
	it would be efficient to call reallocate( 256 ) before adding any elements.

	NOTE: usually used internally in this class, use with care.
*/
template< typename type, typename cmp >
D_FORCE_INLINE
void
Heap<type,cmp>::reallocate
(
	size_t const i_newCapacity
)
{
	// if expanding
	if ( i_newCapacity > m_capacity )
	{
		m_array = ArrayResize<type>( m_array, m_capacity, i_newCapacity );
		m_capacity = i_newCapacity;
	}
	// free up the m_array if no data is being reserved
	else if( i_newCapacity == 0 )
	{
		clear( true );
	}
	// else not changing the size
}

//--------------------------------
/*	top
	Returns a const reference to the top most element.
	Top is the element that would be removed if pop was called.

	NOTE: the top will be the minimal or maximal element in the whole list,
	depending on the comparison you have defined.
*/
template< typename type, typename cmp >
D_FORCE_INLINE
type const&
Heap<type,cmp>::top
(
) const
{
	assert( m_size > 0 );
	return m_array[0];
}

//--------------------------------
/* size
	Returns the number of elements in the container.

	i.e. the number of times push has been called minus
	the number of times pop has been called.
*/
template< typename type, typename cmp >
D_FORCE_INLINE
size_t
Heap<type,cmp>::size
(
) const
{
	return m_size;
}

//--------------------------------
/* operator[]
	Random access operator.
	performs bounds checking in debug.

	NOTE: take caution to not damage the heap property.
	i.e. do not change what the value of the thing that
	the heap is sorted on.
*/
template< typename type, typename cmp >
D_FORCE_INLINE
type&
Heap<type,cmp>::operator[]
(
	size_t i_index
)
{
	assert( i_index < m_size);

	return m_array[ i_index ];
}

//--------------------------------
/* operator[] const
	Random access operator.
	performs bounds checking in debug.
*/
template< typename type, typename cmp >
D_FORCE_INLINE
type const&
Heap<type,cmp>::operator[]
(
	size_t i_index
) const
{
	assert( i_index < m_size);

	return m_array[ i_index ];
}

//--------------------------------
/* operator =
	Copies one Heap to another (deep copy).

	NOTE: SLOW avoid if you can
*/
template< typename type, typename cmp >
Heap<type,cmp>&
Heap<type,cmp>::operator=
(
	HeapType const& i_other
)
{
	clear( true );

	m_size		= i_other.m_size;
	m_capacity	= i_other.m_capacity;

	if( m_capacity > 0 )
	{
		m_array = ArrayNew< type >( m_capacity );

		ArrayCopy(m_array, i_other.m_array, m_size);
	}

	return *this;
}

//--------------------------------
/* Init
	Checks if m_array needs to be allocated, and allocates it if necessary.
*/
template< typename type, typename cmp >
D_FORCE_INLINE
void
Heap<type,cmp>::Init
(
)
{
	if ( m_array == nullptr )
	{
		assert( m_size == 0 );
		reallocate( static_cast<size_t>( default_capacity ) );
	}
}

//--------------------------------
/* CheckResize
	Checks if m_list / m_capacity needs to be increased in size.
*/
template< typename type, typename cmp >
D_FORCE_INLINE
void
Heap<type,cmp>::CheckResize
(
)
{
	if( m_size < m_capacity )
	{
		return;
	}
	else
	{
		reallocate( ResizePolicy<type>::GetNextSize( m_capacity ) );
	}
}

//--------------------------------
/* CheckHeap
	Checks if its a valid heap

	NOTE: only enabled if D_CHECK_VALID_HEAP is defined
*/
#ifdef D_CHECK_VAILD_HEAP
template< typename type, typename cmp >
void
Heap<type,cmp>::CheckHeap
(
)
{
	// for each node
	for( size_t i = 0; i < m_size; ++i)
	{
		size_t const index = FirstChildIndex(i);
		size_t const count = index+2-1;
		for( size_t j = index; j < count; ++j )
		{
			if( j < m_size	&&
				cmp::invalid( m_array[i] ,m_array[j] ) )// use provided compare
			{
				// heap property broken!
				assert( false );
				return;// place break point here and comment out assert for inspection
			}
		}
	}
}
#endif

//--------------------------------
/* LeftChildIndex( i_nodeIndex )
	Calculates the left child index from a index.
*/
template< typename type, typename cmp >
D_FORCE_INLINE
size_t
Heap<type,cmp>::FirstChildIndex
(
	size_t const i_index
) const
{
	return (i_index<<1)+1;
}

//--------------------------------
/* ParentIndex( i_nodeIndex )
	Calculates the parent index from a index
*/
template< typename type, typename cmp >
D_FORCE_INLINE
size_t
Heap<type,cmp>::ParentIndex
(
	size_t const i_index
) const
{
	return i_index!=0?(i_index-1)>>1:0;
}