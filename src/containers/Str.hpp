/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_STR_HPP )
#define D_STR_HPP

//----------------------------------------------------------------
/* Str
	A String class used to hold arrays of characters.
	Use of char const* is frowned upon.

	NOTE: also known as "string", "string" and "String"
	NOTE: is essentially a wrapper for char const*, it does
	all the nasty c things so you don't have to.
	NOTE: remember to aways pass it by reference.
*/
class Str
{
	#include "StrResizePolicy.hpp"
public:
	//-------------------------------- Standard Functions --------------------------------
	// defined in Str.inl
	Str();
	Str( Str const& other );

	explicit Str( char const* i_cStr );
	Str( char const* i_cStr, size_t const i_len );
	Str( char const* i_start, char const* i_end );

	~Str();

	// B
	char const		back() const;										// returns a reference to the back
	char*			begin();											// returns a pointer to the first char
	char const*		begin() const;										// returns a pointer to the first char

	// C
	char const*		c_str() const;
	size_t			capacity() const;									// returns number of chars allocated for
	void			clear( bool const i_dealloc );						// clear the string
	bool			contains( char const i_char ) const;				// find the given char
	bool			contains( char const* i_cstr, size_t const i_len ) const;	// find the given char
	bool			contains( Str const& i_str ) const;					// find the given substring

	// E
	bool			empty() const;										// return true if the string is empty
	char const*		end() const;										// returns a pointer to the back of the string
	bool			erase( size_t const i_index );						// remove the element at the given index, and move every element after it down one
	bool			erase( char const i_obj );							// remove the char string
	bool			erase( Str const& i_obj );							// remove the sub string
	bool			erase( size_t const i_start, size_t const i_end );	// remove the sub string

	// F
	bool			find( char const i_char, char*& o_char ) const;			// find pointer to the given char _SLOW_
	bool			find( Str const& i_str, char*& o_char ) const;			// find pointer to the given sub string _SLOW_
	bool			find_index( char const i_char, size_t& o_index ) const;	// find the index for the given element _SLOW_
	bool			find_index( Str const& i_str , size_t& o_index ) const;	// find the index for the given element _SLOW_
	char const&		front() const;

	// I
	bool			insert( char const i_char, size_t const i_index );		// insert the element at the given index _SLOW_
	bool			insert( Str const& i_Str, size_t const i_index );		// insert string at the given index _SLOW_
	size_t			index_of( char const* i_ptr ) const;					// returns the index for the pointer to a char in the string _RISKY_

	// P
	void			pop_back();												// removes the last element from the container
	void			pop_back( size_t const i_numPops );						// removes the n last elements from the container
	void			push_back( char const i_char );							// append char
	void			push_back( Str const& i_str );							// append string
	void			push_back( char const* i_cStr );						// append c string
	void			push_back( char const* i_cStr, size_t const i_len );	// append c string of set length
	void			push_back( char const* i_start, char const* i_end );	// append range
	void			push_backf( D_VERIFY_FORMAT_STRING char const* formatCStr, ... ); // append a printf formatted string

	// R
	void			reserve( size_t const i_numNewElements );							// makes sure there is enough memory for a number of elements
	void			reallocate( size_t const i_newnum );								// set number of elements in string and resize to exactly this number if needed

	// S
	Str				sub_str( size_t const i_start, size_t const i_end );				// creates a new string from a given range
	void			shrink_to_fit();													// resizes string to exactly the number of elements it contains
	void			shuffle( Random::RNG& ran );										// apply shuffle to string
	size_t			size() const;														// returns number of chars in string
	void			sort( const Algorithm::SortBase<char>& i_sort = Algorithm::Sort_QuickDefault<char>() );	// sort the string

	// Memory functions
	size_t			_allocated() const;													// returns total size of allocated memory
	size_t			_size() const;														// returns total size of allocated memory including size of Str
	size_t			_memory_used() const;												// returns size of the used elements in the string

	//-------------------------------- Additional Functions --------------------------------
	// implemented in StrAdditional.inl
	void			to_lower(); // converts all upper case characters to lower case.
	void			to_upper(); // converts all lower case characters to upper case.

	bool			numeric() const; // checks if the string contains a number

	bool			contains_lower() const;
	bool			contains_upper() const;

	bool			split( char const i_charTok, List<Str>& o_output );
	bool			split( Str const& i_strTok, List<Str>& o_output );
	bool			split( char const* i_cStrTok, List<Str>& o_output );

	size_t			span( char const* i_cStrSet ) const;
	size_t			span( Str const& i_StrSet ) const;

	size_t			hash( size_t const i_seed ) const;
	size_t			hash() const;

	//-------------------------------- Format related functions --------------------------------
	// implemented in StrFormat.inl
	void			strip_leading( char const i_char );				// strip i_char from front as many times as that i_char occurs
	void			strip_leading( Str const& i_str );				// strip string from front as many times as the string occurs
	void			strip_leading( char const* i_cStr );			// strip c string from front as many times as the string occurs

	void			strip_trailing( char const i_char );			// strip i_char from end as many times as the char occurs
	void			strip_trailing( Str const& i_str );				// strip i_str from end as many times as the string occurs
	void			strip_trailing( char const* i_cStr );			// strip c string from end as many times as the string occurs

	void			strip_trailing_whitespace();					// strip trailing white space characters
	void			strip_quotes();									// strip quotes around string

	bool			replace( char const i_findChar, char const i_replacedChar );
	bool			replace( Str const& i_findStr, Str const& i_replaceStr );
	bool			replace( char const* i_findCStr, char const* i_replacedCStr );

	size_t			format( D_VERIFY_FORMAT_STRING char const* formatCStr, ... );

	//-------------------------------- Comparison Functions --------------------------------
	// implemented in StrCompare.inl
	// these functions are for comparing a Str with a char const*
					// case sensitive compare
	int				cmp( Str const& i_str ) const;
	int				cmp( char const* i_cStr ) const;
	int				cmpn( Str const& i_str, size_t const i_len ) const;
	int				cmpn( char const* i_cStr, size_t const i_len ) const;

					// case insensitive compare, SLOW avoid if you can
	int				icmp( Str const& i_str ) const;
	int				icmp( char const* i_cStr ) const;
	int				icmpn( Str const& i_str, size_t const i_len ) const;
	int				icmpn( char const* i_cStr, size_t const i_len ) const;

	//-------------------------------- operator Functions --------------------------------
	// implemented in StrOperators.inl
#if 0
	// intensionally not implemented, use of char const*s are not recommended
	operator		const char *() const;
	operator		const char *();
#endif

	char			operator[]( size_t i_index ) const;
	char&			operator[]( size_t i_index );

	Str&			operator=( Str const& i_other ); // _SLOW_
	Str&			operator=( char const* i_other ); // _SLOW_

	friend Str		operator+( Str const& a, Str const& b );
	friend Str		operator+( Str const& a, char const* b );
	friend Str		operator+( char const* a, Str const& b );

	friend Str		operator+( Str const& a, real_t const i_real );
	friend Str		operator+( Str const& a, int64 const i_int64 );
	friend Str		operator+( Str const& a, size_t const i_size_t );
	friend Str		operator+( Str const& a, bool const i_bool );

	Str&			operator+=( Str const& i_str );
	Str&			operator+=( char const* i_cStr );
	Str&			operator+=( char const i_char );

	Str&			operator+=( real_t const i_real );
	Str&			operator+=( int64 const i_int64 );
	Str&			operator+=( size_t const i_size_t );

	Str&			operator+=( bool const i_bool );

						// case sensitive compare
	friend bool		operator==( Str const& i_strLeft, Str const& i_strRight );
	friend bool		operator==( Str const& i_strLeft, char const* i_cStrRight );
	friend bool		operator==( char const* i_cStrLeft, Str const& i_StrRight );

						// case sensitive compare
	friend bool		operator!=( Str const& i_strLeft, Str const& i_strRight );
	friend bool		operator!=( Str const& i_strLeft, char const* i_cStrRight );
	friend bool		operator!=( char const* i_cStrLeft, Str const& i_StrRight );

	friend ostream& operator <<(ostream& i_ostream, Str const& i_str);
	friend istream& operator >>(istream& i_istream, Str& i_str);

	enum
	{
		istream_buffer_size = (1<<9) // 512 bytes
	};
private:

	//size of the char buffer used in the format function
	enum
	{
		format_buffer_size = (1<<10), // 1,024 bytes
	};

	void	Init();
	void	CheckResize();

	char*		m_array;
	size_t		m_size;
	size_t		m_capacity;
};

#include "Str.inl"
#include "StrAdditional.inl"
#include "StrFormat.inl"
#include "StrCompare.inl"
#include "StrOperators.inl"

#endif // end of D_STR_HPP