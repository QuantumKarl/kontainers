/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

//--------------------------------
/* strip_leading( i_char )
	removes i_char from the beginning of the Str,
	as many times as it occurs (at the beginning of the string)

	NOTE: case sensitive
*/
D_FORCE_INLINE
void
Str::strip_leading
(
	char const i_char
)
{
	assert( m_array != nullptr );
	assert( m_size > 0 );
	assert( i_char != '\0' );
#
	// Count how many occurrences of i_char they are at the front of the string
	size_t i = 0;
	while( i < m_size && m_array[i] == i_char )
	{
		++i;
	}

	assert( i <= m_size );
	if( i == m_size )
	{
		m_size = 0;
		m_array[m_size] = '\0';
	}
	else if( i > 0 )
	{
		// Now move down the contents to over write the occurrences
		memmove( static_cast<void*>( &m_array[0] ),
			static_cast<void const*>( &m_array[i] ),
			m_size - i );

		// update the size
		m_size -= i;

		// Make sure the string is terminated correctly
		m_array[m_size] = '\0';
	}
}

//--------------------------------
/* strip_leading( i_str )
	removes i_str from the beginning of the Str,
	as many times as it occurs (at the beginning of the string)

	NOTE: case sensitive
*/
D_FORCE_INLINE
void
Str::strip_leading
(
	Str const& i_str
)
{
	assert( m_array != nullptr );
	assert( m_size > 0 );
	assert( i_str.size() > 0 );
#
	// Count how many occurrences of i_str they are at the front of the string
	size_t const strSize = i_str.size();
	size_t i = 0;
	while( strncmp( &m_array[i], i_str.c_str(), strSize ) == 0	&& i < m_size )
	{
		i += strSize;
	}

	assert( i <= m_size );
	if( i == m_size )
	{
		m_size = 0;
		m_array[m_size] = '\0';
	}
	else if( i > 0 )
	{
		// Now move down the contents to over write the occurrences
		memmove( static_cast<void*>( &m_array[0] ),
			static_cast<void const*>( &m_array[i] ),
			m_size - i );

		// update the size
		m_size -= i;

		// Make sure the string is terminated correctly
		m_array[m_size] = '\0';
	}
}

//--------------------------------
/* strip_leading( i_cStr )
	removes i_cStr from the beginning of the Str,
	as many times as it occurs (at the beginning of the string)

	NOTE: case sensitive
*/
D_FORCE_INLINE
void
Str::strip_leading
(
	char const* i_cStr
)
{
	assert( m_array != nullptr );
	assert( m_size > 0 );
	assert( i_cStr != nullptr );
#
	// Count how many occurrences of i_str they are at the front of the string
	size_t const strSize = strlen( i_cStr );
	size_t i = 0;
	while( strncmp( &m_array[i], i_cStr, strSize ) == 0	&& i < m_size )
	{
		i += strSize;
	}

	assert( i <= m_size );
	if( i == m_size )
	{
		m_size = 0;
		m_array[m_size] = '\0';
	}
	else if( i > 0 )
	{
		// Now move down the contents to over write the occurrences
		memmove( static_cast<void*>( &m_array[0] ),
			static_cast<void const*>( &m_array[i] ),
			m_size - i );

		// update the size
		m_size -= i;

		// Make sure the string is terminated correctly
		m_array[m_size] = '\0';
	}
}

//--------------------------------
/* strip_trailing( i_char )
	removes i_char from the ending of the Str,
	as many times as it occurs (at the ending of the string)

	NOTE: case sensitive
*/
D_FORCE_INLINE
void
Str::strip_trailing
(
	char const i_char
)
{
	assert( m_array != nullptr );
	assert( m_size > 0 );
	assert( i_char != '\0' );

	// Count the number of occurrences of i_char at the end of the string
	size_t i = m_size-1;
	while( i < m_size && m_array[i] == i_char )
	{
		--i;
	}

	// update the size
	m_size = i < m_size ? i+1 : 0; // if i has not wrapped around then i+1 else 0

	// Make sure the string is terminated correctly
	m_array[m_size] = '\0';
}

//--------------------------------
/* strip_trailing( i_str )
	removes i_str from the ending of the Str,
	as many times as it occurs (at the ending of the string)

	NOTE: case sensitive
*/
D_FORCE_INLINE
void
Str::strip_trailing
(
	Str const& i_str
)
{
	assert( m_array != nullptr );
	assert( m_size > 0 );
	assert( i_str.size() > 0 );
	assert( i_str.size() <= m_size );

	// if the string's length is less than our string
	if( i_str.size() < m_size )
	{
		// Count the number of occurrences of i_str at the end of the string
		size_t const strSize = i_str.size();
		size_t i = m_size-1-strSize;

		while( strncmp( &m_array[i], i_str.c_str(), strSize ) == 0	&& i < m_size )
		{
			i -= strSize;
		}

		// update the size
		m_size = i < m_size ? i+1 : 0; // if i has not wrapped around then i+1 else 0

		// Make sure the string is terminated correctly
		m_array[m_size] = '\0';
	}
	// if the string to be stripped is our string
	else if( strncmp( &m_array[0], i_str.c_str(), m_size ) == 0)
	{
		m_size = 0;
		m_array[m_size] = '\0';
	}
}

//--------------------------------
/* strip_trailing( i_cStr )
	removes i_cStr from the ending of the Str,
	as many times as it occurs (at the ending of the string)

	NOTE: case sensitive
*/
D_FORCE_INLINE
void
Str::strip_trailing
(
	char const* i_cStr
)
{
	assert( m_array != nullptr );
	assert( m_size > 0 );
	assert( i_cStr != nullptr );

	size_t const strSize = strlen( i_cStr );

	assert( strSize <= m_size );

	// if the string's length is less than our string
	if( strSize < m_size )
	{
		// Count the number of occurrences of i_cStr at the end of the string
		size_t i = m_size-1-strSize;

		assert( i < m_size );
		while( strncmp( &m_array[i], i_cStr, strSize ) == 0 && i < m_size )
		{
			i -= strSize;
		}

		// update the size
		m_size = i < m_size ? i+1 : 0; // if i has not wrapped around then i+1 else 0

		// Make sure the string is terminated correctly
		m_array[m_size] = '\0';
	}
	// if the string to be stripped is our string
	else if( strncmp( &m_array[0], i_cStr, m_size ) == 0)
	{
		m_size = 0;
		m_array[m_size] = '\0';
	}
}

//--------------------------------
/* strip_trailing_whitespace()
	removes all white space at the end of the string.

	NOTE: similar to FORTRAN's trim
*/
D_FORCE_INLINE
void
Str::strip_trailing_whitespace
(
)
{
	assert( m_array != nullptr );
	assert( m_size > 0 );

	// count the number of occurrences of whitespace at the end of the string
	size_t i = m_size-1;
	while( i < m_size && static_cast<unsigned char>( m_array[i] ) <= ' ' )
	{
		--i;
	}

	// update the size
	m_size = i < m_size ? i+1 : 0; // if i has not wrapped around then i+1 else 0

	// Make sure the string is terminated correctly
	m_array[m_size] = '\0';
}

//--------------------------------
/* strip_quotes
	removes " from the start and the end of the string
*/
D_FORCE_INLINE
void
Str::strip_quotes
(
)
{
	assert( m_array != nullptr );
	assert( m_size > 1 );

	// check if the string has quotes to be removed
	if( m_array[0] == '\"' && m_array[m_size-1] == '\"' )
	{
		// remove the first quote
		memmove( static_cast<void*>( &m_array[0] ),
				static_cast<void const*>( &m_array[1] ),
				m_size-1 );

		// update the size
		m_size -= 2;

		// make sure to correctly null terminate (remove the last quote)
		m_array[m_size] = '\0';
	}
}

//--------------------------------
/* replace
	finds and replaces a sub string.
	returns true if any of the contents were changed.
*/
bool
Str::replace
(
	char const i_findChar,
	char const i_replaceChar
)
{
	bool replaced = false;
	for ( size_t i = 0; i < m_size; ++i )
	{
		if ( m_array[i] == i_findChar )
		{
			m_array[i] = i_replaceChar;
			replaced = true;
		}
	}
	return replaced;
}

//--------------------------------
/* replace( i_findStr, i_replaceStr )
	finds and replaces a sub string.
	returns true if any of the contents were changed.
*/
bool
Str::replace
(
	Str const& i_findStr,
	Str const& i_replaceStr
)
{
	assert( i_findStr != i_replaceStr );
	assert( i_findStr.size() > 0 );
	assert( i_replaceStr.size() > 0);// Use the erase function instead

	size_t const findSize = i_findStr.size();
	size_t const replaceSize = i_replaceStr.size();

	// Count number of matches (this is so we can avoid creating a temp string)
	size_t count	= 0;
	size_t i		= 0;
	while( (i+findSize) <= m_size )
	{
		if( strncmp(&m_array[i], i_findStr.c_str(), findSize) == 0 )
		{
			i+=findSize;
			++count;
		}
		else
		{
			++i;
		}
	}

	if( count > 0 )
	{
		// calculate the size difference between the find and replace strings
		int64 const sizeDiff = static_cast<int64>(replaceSize) - static_cast<int64>(findSize);

		// calculate new size (without casting m_size + sizeDiff * count)
		size_t const newSize = static_cast<size_t>( static_cast<int64>(m_size) + sizeDiff * static_cast<int64>(count) );

		// Ensure we have enough space
		reserve( newSize );

		i = 0;
		do
		{
			if( strncmp(&m_array[i], i_findStr.c_str(), findSize) == 0 )
			{
				if( sizeDiff != 0 ) // constant within this loop
				{
					// Move the rest of the array to accommodate the replaced string

					// without casting &m_array[i-sizeDiff]
					char const* srcToMove		= &m_array[static_cast<size_t>(static_cast<int64>(i)-sizeDiff)];
					// without casting (m_size-i)+sizeDiff
					size_t const amountToMove	= static_cast<size_t>( (static_cast<int64>(m_size)-static_cast<int64>(i) )+sizeDiff);

					memmove(static_cast<void*>(&m_array[i]),
							static_cast<void const*>( srcToMove ),
							amountToMove );
				}

				// Copy the data into our array
				memcpy(	static_cast<void*>( &m_array[i] ),
						static_cast<void const*>( i_replaceStr.c_str() ),
						replaceSize );

				assert( m_size - abs(sizeDiff) <= m_size ); // check we are not wrapping around

				// Update the size (without casting m_size += sizeDiff)
				m_size = static_cast<size_t>(static_cast<int64>(m_size) + sizeDiff);

				// Update our index
				i+=replaceSize;
			}
			else
			{
				++i;
			}
		}
		while( (i+findSize) <= newSize );
		
		// make sure to correctly null terminate
		m_array[m_size] = '\0';

		return true;
	}
	return false;
}


//--------------------------------
/* replace( i_findCStr, i_replaceCStr )
	finds and replaces a sub string.
	returns true if any of the contents were changed.
*/
bool
Str::replace
(
	char const* i_findCStr,
	char const* i_replaceCStr
)
{
	assert( strcmp( i_findCStr, i_replaceCStr ) != 0 );

	size_t const findSize = strlen( i_findCStr );
	size_t const replaceSize = strlen( i_replaceCStr );

	assert( findSize > 0 );
	assert( replaceSize > 0);// Use the erase function instead

	// Count number of matches
	size_t count	= 0;
	size_t i		= 0;
	while( (i+findSize) <= m_size )
	{
		if( strncmp(&m_array[i], i_findCStr, findSize) == 0 )
		{
			i+=findSize;
			++count;
		}
		else
		{
			++i;
		}
	}

	if( count > 0 )
	{
		// calculate the size difference between the find and replace strings
		int64 const sizeDiff = static_cast<int64>(replaceSize) - static_cast<int64>(findSize);

		// calculate new size (without casting m_size + sizeDiff * count)
		size_t const newSize = static_cast<size_t>( static_cast<int64>(m_size) + sizeDiff * static_cast<int64>(count) );

		// Ensure we have enough space
		reserve( newSize );

		i = 0;
		do
		{
			if( strncmp(&m_array[i], i_findCStr, findSize) == 0 )
			{
				if( sizeDiff != 0 ) // constant within this loop
				{
					// Move the rest of the array to accommodate the replaced string

					// without casting &m_array[i-sizeDiff]
					char const* srcToMove		= &m_array[static_cast<size_t>(static_cast<int64>(i)-sizeDiff)];
					// without casting (m_size-i)+sizeDiff
					size_t const amountToMove	= static_cast<size_t>( (static_cast<int64>(m_size)-static_cast<int64>(i) )+sizeDiff);

					memmove(static_cast<void*>(&m_array[i]),
							static_cast<void const*>( srcToMove ),
							amountToMove );
				}

				// Copy the data into our array
				memcpy(	static_cast<void*>( &m_array[i] ),
						static_cast<void const*>( i_replaceCStr ),
						replaceSize );

				assert( m_size - abs(sizeDiff) <= m_size ); // check we are not wrapping around

				// Update the size (without casting m_size += sizeDiff)
				m_size = static_cast<size_t>(static_cast<int64>(m_size) + sizeDiff);

				// Update our index
				i+=replaceSize;
			}
			else
			{
				++i;
			}
		}
		while( (i+findSize) <= newSize );
		
		// make sure to correctly null terminate
		m_array[m_size] = '\0';

		return true;
	}
	return false;
}

//--------------------------------
/* format( i_append, formatCStr, va_args )
	An attempt at a safe snprintf.

	if i_append is true, the resulting formatted
	string will be appended to the String else,
	it will replace it.

	since this uses vsnprintf, it still does not validate the
	number of va args vs number of format tokens in the format string.

	However, it use MS sourceAnnotations to attempt to check that the format C String is correct

	vsnprintf portability:

	C99 standard: vsnprintf returns the number of characters (excluding the trailing
	'\0') which would have been written to the final string if enough space had been available
	snprintf and vsnprintf do not write more than size bytes (including the trailing '\0')

	win32: _vsnprintf returns the number of characters written, not including the terminating null character,
	or a negative value if an output error occurs. If the number of characters to write exceeds count, then count 
	characters are written and -1 is returned and no trailing '\0' is added.

	Str::vsnPrintf: always appends a trailing '\0', returns number of characters written (not including terminal \0)
	or returns -1 on failure or if the buffer would overflow.
*/
size_t
Str::format
(
	D_VERIFY_FORMAT_STRING char const* formatCStr,
	...
)
{
	// Set up a buffer to write into
	size_t const bufferSize = static_cast<size_t>( format_buffer_size ); // See header for size
	char buffer[bufferSize];
	buffer[bufferSize-1] = '\0';
	
	// Setup va list
	va_list argptr;
	va_start( argptr, formatCStr ); //-V111

	// Write into buffer
	int l = _vsnprintf_s( buffer, bufferSize-1, formatCStr, argptr );

	// clean up va list
	va_end( argptr );

	assert( l != bufferSize-1 );
	assert( l != 0 );

	// copy or append into the string
	if( l > 0 )
	{
		// make sure to null terminate the c string
		buffer[l] = '\0';

		clear( false );
		push_back(buffer, static_cast<size_t>(l) );
	}

	// return the number of characters written
	return l > 0 ? static_cast<size_t>(l):0;
}