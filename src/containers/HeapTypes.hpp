/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_HEAP_TYPES_HPP )
#define D_HEAP_TYPES_HPP

// use these classes as template args (to heap) to change the heap from min heap to a max heap,
// or define your own for advanced comparison eg, a heap of pointers where the comparisons
// dereference the pointer and compare a value(instead of just comparing pointers)
class min_heap
{
	// NOTE: we have to use two functions since
	// (a < b) != (a > b)
	// is NOT true.
public:
	template< typename T >
	D_FORCE_INLINE
	static bool valid( T const& l, T const& r)
	{
		return l < r;
	}

	template< typename T >
	D_FORCE_INLINE
	static bool invalid(T const& l, T const& r)
	{
		return l > r;
	}
};

class max_heap
{
	// NOTE: we have to use two functions since
	// (a < b) != (a > b)
	// is NOT true.
public:

	template< typename T >
	D_FORCE_INLINE
	static bool valid(T const& l, T const& r)
	{
		return l > r;
	}

	template< typename T >
	D_FORCE_INLINE
	static bool invalid(T const& l, T const& r)
	{
		return l < r;
	}
};

// used when you want to contain pointers to the data, but
// order based on the objects value (not the pointer's value)
// ie, separate the data from its representation
// NOTE: appears slower unless a single data entry is too large for the L1 cache
class min_heap_ptr
{
public:
	template< typename T >
	D_FORCE_INLINE
	static bool valid( T const& l, T const& r)
	{
		return (*l) < (*r);
	}

	template< typename T >
	D_FORCE_INLINE
	static bool invalid(T const& l, T const& r)
	{
		return (*l) > (*r);
	}
};

class max_heap_ptr
{
public:

	template< typename T >
	D_FORCE_INLINE
	static bool valid(T const& l, T const& r)
	{
		return (*l) > (*r);
	}

	template< typename T >
	D_FORCE_INLINE
	static bool invalid(T const& l, T const& r)
	{
		return (*l) < (*r);
	}
};

#endif // end of D_HEAP_TYPES