/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>.

	INFO: MurmurHash2 was written by Austin Appleby, and is placed in the public
	domain. The author hereby disclaims copyright to this source code.

	It has been modified by Karl Wesley Hutchinson
	to match the style of the rest of Kontainers.
*/
#if !defined( D_MURMURHASH2_HPP )
#define D_MURMURHASH2_HPP
/* Note - This code makes a few assumptions about how your machine behaves -

	1. We can read a 4-byte value from any address without crashing
	2. sizeof(int) == 4

	And it has a few limitations -

	1. It will not work incrementally.
	2. It will not produce the same results on little-endian and big-endian machines.
*/
namespace Hash
{
#if defined( D_64BIT )
	//-----------------------------------------------------------------------------
	// MurmurHash2, 64-bit versions, by Austin Appleby

	// The same caveats as 32-bit MurmurHash2 apply here - beware of alignment
	// and endian-ness issues if used across multiple platforms.

	// 64-bit hash for 64-bit platforms

	D_INLINE
	uint64
	Murmur
	(
		const void* key,
		size_t len,
		uint64 seed
	)
	{
		const uint64 m = D_ULL(0xc6a4a7935bd1e995);
		const int r = 47;

		uint64 h = seed ^ (len * m);

		const uint64 * data = (const uint64 *)key;
		const uint64 * end = data + (len/8);

		while(data != end)
		{
			uint64 k = *data++;

			k *= m;
			k ^= k >> r;
			k *= m;

			h ^= k;
			h *= m;
		}

		const unsigned char * data2 = (const unsigned char*)data;

		switch(len & 7)
		{
		case 7: h ^= uint64(data2[6]) << 48;
		case 6: h ^= uint64(data2[5]) << 40;
		case 5: h ^= uint64(data2[4]) << 32; //-V112
		case 4: h ^= uint64(data2[3]) << 24;
		case 3: h ^= uint64(data2[2]) << 16;
		case 2: h ^= uint64(data2[1]) << 8;
		case 1: h ^= uint64(data2[0]);
			h *= m;
		};

		h ^= h >> r;
		h *= m;
		h ^= h >> r;

		return h;
	}

	// Murmur3 hash final step, sufficient for hashing small data
	uint64
	MurmurFinal
	(
		const uint64& key
	)
	{
		uint64 h = key;
		h ^= h >> 33;
		h *= 0xff51afd7ed558ccd;
		h ^= h >> 33;
		h *= 0xc4ceb9fe1a85ec53;
		h ^= h >> 33;
		return h;
	}

#elif defined( D_32BIT )
	//-----------------------------------------------------------------------------
	// MurmurHash2A, by Austin Appleby

	// This is a variant of MurmurHash2 modified to use the Merkle-Damgard 
	// construction. Bulk speed should be identical to Murmur2, small-key speed 
	// will be 10%-20% slower due to the added overhead at the end of the hash.

	// This variant fixes a minor issue where null keys were more likely to
	// collide with each other than expected, and also makes the function
	// more amenable to incremental implementations.

	uint32
	Murmur
	(
		const void * key,
		size_t len, uint32 seed
	)
	{
		#define mmix(h,k) { k *= m; k ^= k >> r; k *= m; h *= m; h ^= k; }

		const uint32 m = 0x5bd1e995;
		const int r = 24;
		uint32 l = len;

		const unsigned char * data = (const unsigned char *)key;

		uint32 h = seed;

		while(len >= 4)
		{
			uint32 k = *(uint32*)data;

			mmix(h,k);

			data += 4;
			len -= 4;
		}

		uint32 t = 0;

		switch(len)
		{
		case 3: t ^= data[2] << 16;
		case 2: t ^= data[1] << 8;
		case 1: t ^= data[0];
		};

		mmix(h,t);
		mmix(h,l);

		h ^= h >> 13;
		h *= m;
		h ^= h >> 15;

		return h;
	}

	// Murmur3 hash final step, sufficient for hashing small data
	uint32
	MurmurFinal
	(
		const uint32& key
	)
	{
		uint32 h = key;
		h ^= h >> 16;
		h *= 0x85ebca6b;
		h ^= h >> 13;
		h *= 0xc2b2ae35;
		h ^= h >> 16;
		return h;
	}
#endif
}
#endif // _MURMURHASH2_H_

#if 0
// 64-bit hash for 32-bit platforms
D_INLINE uint64 MurmurHash64( const void * key, int len, uint64 seed )
{
	const uint32 m = 0x5bd1e995;
	const int r = 24;

	uint32 h1 = uint32(seed) ^ len;
	uint32 h2 = uint32(seed >> 32);

	const uint32 * data = (const uint32 *)key;

	while(len >= 8)
	{
		uint32 k1 = *data++;
		k1 *= m; k1 ^= k1 >> r; k1 *= m;
		h1 *= m; h1 ^= k1;
		len -= 4;

		uint32 k2 = *data++;
		k2 *= m; k2 ^= k2 >> r; k2 *= m;
		h2 *= m; h2 ^= k2;
		len -= 4;
	}

	if(len >= 4)
	{
		uint32 k1 = *data++;
		k1 *= m; k1 ^= k1 >> r; k1 *= m;
		h1 *= m; h1 ^= k1;
		len -= 4;
	}

	switch(len)
	{
	case 3: h2 ^= ((unsigned char*)data)[2] << 16;
	case 2: h2 ^= ((unsigned char*)data)[1] << 8;
	case 1: h2 ^= ((unsigned char*)data)[0];
		h2 *= m;
	};

	h1 ^= h2 >> 18; h1 *= m;
	h2 ^= h1 >> 22; h2 *= m;
	h1 ^= h2 >> 17; h1 *= m;
	h2 ^= h1 >> 19; h2 *= m;

	uint64 h = h1;

	h = (h << 32) | h2;

	return h;
}
//-----------------------------------------------------------------------------
// MurmurHashNeutral2, by Austin Appleby (Safe version)

// Same as MurmurHash2, but endian- and alignment-neutral.
// Half the speed though, alas.

uint32 MurmurHashNeutral2 ( const void * key, int len, uint32 seed )
{
	const uint32 m = 0x5bd1e995;
	const int r = 24;

	uint32 h = seed ^ len;

	const unsigned char * data = (const unsigned char *)key;

	while(len >= 4)
	{
		uint32 k;

		k  = data[0];
		k |= data[1] << 8;
		k |= data[2] << 16;
		k |= data[3] << 24;

		k *= m; 
		k ^= k >> r; 
		k *= m;

		h *= m;
		h ^= k;

		data += 4;
		len -= 4;
	}

	switch(len)
	{
	case 3: h ^= data[2] << 16;
	case 2: h ^= data[1] << 8;
	case 1: h ^= data[0];
		h *= m;
	};

	h ^= h >> 13;
	h *= m;
	h ^= h >> 15;

	return h;
}

#endif // end of D_MURMURHASH2_HPP