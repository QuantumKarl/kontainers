/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

#if !defined( D_SOURCE_DATA )
#define D_SOURCE_DATA
#include <vector>
#include <string>
using namespace std;

#include "random\Random.hpp"

class SourceData
{
public:
	static void InitStatic()
	{
		SetupWords();
		SetupFloats();
	}

	static void FiniStatic()
	{
		allWords.clear();
		allWordsStr.clear();
		randomFloats.clear();
		allWords.shrink_to_fit();
		allWordsStr.shrink_to_fit();
		randomFloats.shrink_to_fit();
	}

	static vector<string>	allWords;
	static vector<Str>		allWordsStr;
	static vector<float>	randomFloats;
	// Primes are in Primes namespace

private:
	static void SetupWords()
	{
#if defined( D_FULL_TEST )
		allWords.reserve( 1 << 18 ); // there are 216k words in the file, so reserve 262K
		allWordsStr.reserve( 1 << 18 );
#else
		allWords.reserve( 1 << 16 );
		allWordsStr.reserve( 1 << 16 );
#endif
		//-------- Get all the words ------------------
		ifstream inputFile("AllEngWords.txt");

		string strTemp;
		if( inputFile.is_open() == true )
		{
			cout << "Reading all the words. Please wait..." << endl;

#if defined( D_FULL_TEST )
			while( inputFile >> strTemp )
#else
			// just do 65k words
			size_t const max = 1<<16;
			size_t i = 0;
			while( i++ < max && inputFile >> strTemp )
#endif
			{
				allWords.push_back( strTemp );
				allWordsStr.push_back( Str(strTemp.c_str() ) );
				strTemp.clear();
			}

			inputFile.close();
			inputFile.clear(); // only clears our buffer not the file

#if defined( D_FULL_TEST )
			allWords.shrink_to_fit();
			allWordsStr.shrink_to_fit();
#endif
			cout << "Completed reading all the words" << endl;
		}
		else // ERROR
		{
			cout << "ERROR UNABLE TO FIND ALLWORDS FILE" << endl;
		}
		//----------- End of all words ----------------
	}

	static void SetupFloats()
	{
		size_t const numFloats = 1 << 18; // 262K
		randomFloats.reserve( numFloats );

		for(size_t i = 0; i < numFloats; ++i )
		{
			randomFloats.push_back( (float)Random::Generator().Real() );
		}

		randomFloats.shrink_to_fit();
	}

};

vector<string>	SourceData::allWords;
vector<Str>		SourceData::allWordsStr;
vector<float>	SourceData::randomFloats;
#endif