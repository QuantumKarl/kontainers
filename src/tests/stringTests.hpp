/*	This file is part of Kontainers.
	Copyright(C) 2015 by Karl Wesley Hutchinson

	Kontainers is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kontainers is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kontainers. If not, see <http://www.gnu.org/licenses/>. */

// Tests the Str class for correct functionality
namespace stringTests
{
	void stringTest1()
	{
		Str k;
		Str k2;

		for(size_t i = 0; i < 26; ++i )
		{
			k.push_back((char)('a'+i));
		}

		for( const char* c = k.begin(), *e = k.end(); c != e; ++c)
		{
			cout << *c;
		}
		cout << "\n"<< endl;

		for(size_t i = 0; i < k.size(); i+=2 )
		{
			k.erase( i );
		}

		k2 = k;

		k.erase( 'z' );

		cout << k.begin() << endl;

		while( k.empty() == false )
		{
			cout << k.front() << ',' << k.back() << endl;
			k.pop_back();
		}

		k2.insert( 'k', k2.size()-1 );
		k2.insert( '2', k2.size()-1 );

		cout << k2.begin() << endl;

		k.clear( false );
		k2.clear( true );
	}

	void stringTest2()
	{
		const char cstr[] = { 'h','e', 'l','l','o', '\0' };
		Str k( &cstr[0], &cstr[5] );
		Str k2( "world" );
		Str k3( k );

		cout << k.begin() << endl;
		cout << k2.begin() << endl;

		k3.push_back( ' ' );
		k3.push_back( k2 );

		cout << k3.begin() << endl;

		if( k3.contains( k ) && k3.contains( k2 ) )
			//if( k.contains( "hello" ) == true )
		{
			k3.insert( k2, 0 );
			k3.insert( k, k3.size()-k.size() );
			k3.erase( k );
			k3.erase( k2 );
		}
	}

	bool stringTestContains()
	{
		cout << endl;
		size_t const num = 64;
		Str src;
		Str ser;
		for(size_t i = 0; i < num; ++i )
		{
			src.push_back( (char)('a'+Random::Generator().Int(0,25)) );
		}

		size_t div = 8; // length of sub str to search for
		while( div != 0 )
		{
			for(size_t i = 0; i < (num/div)-1; ++i )
			{
				ser.clear(false);
				ser.push_back( &src[i], &src[i+div] );

				if( src.contains( src[i] ) == true )
				if( src.contains( &src[i], div ) == 0 )
				if( src.contains( ser ) )
				{

				}
				else
				{
					// error!
					return false;
				}
			}

			--div;
		} // end of while
		cout << src.begin() << endl;

		return true;
	}
	
	bool stringTestErase()
	{
		cout << endl;
		size_t const num = 64;
		Str src;
		Str ser;

		size_t div = 8; // length of sub str to search for
		while( div != 1 )
		{
			// put characters in string to be removed
			for(size_t i = 0; i < num*2; ++i )
			{
				src.push_back( (char)('a'+Random::Generator().Int(0,25)) );
			}

			while( src.empty() == false )
			{
				ser.clear(false);
				if( src.size() > div )
				{
					ser.push_back( &src[div/3], &src[div] ); // seems to be the most dangerous function since, hardly any validation can be done on it
				}
				else
				{
					break;
				}

				//if( src.erase(div/2,div) == false )
				if( src.erase( ser ) == false )
				{
					break;
				}
			}
			--div;
		} // end of while
		cout << src.begin() << endl;

		return true;
	}
	
	bool stringTestOperators()
	{
		cout << endl;
		size_t const num = 64;
		Str str1;
		Str str2;
		Str str3;

		size_t div = 8; // length of sub str to search for
		while( div != 1 )
		{
			// put characters in string to be removed
			for(size_t i = 0; i < num*2; ++i )
			{
				str1.push_back( (char)('a'+Random::Generator().Int(0,25)) );
			}
			str2.push_back( str1 );
			str3.push_back( str2.c_str() );
			str3.push_back( (char)('a'+Random::Generator().Int(0,25)) );

			str2 = str1;

			if( str1 == str2 && str1 != str3 )
			{
				str3 = (str2+" "+str1).c_str();
				if( str3.size() == num*4+1 ) //-V112
				{
					str3.clear( false );
					str3 += str1.sub_str(0,num/2);
					str3 += str3.sub_str(0,num/4).c_str(); //-V112
					if( str3 == str3.c_str() && str1.c_str() != str3.c_str() )
					{

					}
					else
					{
						// ERROR
						return false;
					}
				}
			}
			else
			{
				//ERROR
				return false;
			}
			--div;
		}

		return true;
	}

	bool stringTestcmpAndCaseChanges()
	{
		size_t const num = 64;
		Str str1;
		Str str2;
		Str str3;
		Str str4;

		size_t div = 8; // length of sub str to search for
		while( div != 0 )
		{
			// put characters in string to be compared
			for(size_t i = 0; i < num*2; ++i )
			{
				str1.push_back( (char)('a'+Random::Generator().Int(0,25)) );
				str2.push_back( str1.back() );
				str3.push_back( (char)('a'+Random::Generator().Int(0,25)) );
				str4.push_back( (char)('0'+Random::Generator().Int(0,9)) );
			}

			// sensitive
			if( str1.cmp( str2 )						== 0	&&
				str2.cmp( str1.c_str() )				== 0	&&
				str1.cmpn( str2, str2.size()/div )		== 0	&&
				str2.cmpn( str1.c_str(), str1.size() )	== 0	&&

				// not equal
				str1.cmp( str3 )						!= 0	&&
				str3.cmp( str1.c_str() )				!= 0	&&
				str1.cmpn( str3, str3.size()/div )		!= 0	&&
				str3.cmpn( str1.c_str(), str1.size() )	!= 0 )
			{

			}
			else
			{
				//ERROR!
				return false;
			}

			// change case and comparing again
			if( str1.contains_lower() )
			{
				str1.to_upper();
			}

			// insensitive
			if( str1.icmp( str2 )						== 0	&&
				str2.icmp( str1.c_str() )				== 0	&&
				str1.icmpn( str2, str2.size()/div )		== 0	&&
				str2.icmpn( str1.c_str(), str1.size() )	== 0	&&

				// not equal
				str1.icmp( str3 )						!= 0	&&
				str3.icmp( str1.c_str() )				!= 0	&&
				str1.icmpn( str3, str3.size()/div )		!= 0	&&
				str3.icmpn( str1.c_str(), str1.size() )	!= 0 )
			{

			}
			else
			{
				//ERROR!
				return false;
			}

			// swap case and compare one last time
			if( str1.contains_upper() )
			{
				str1.to_lower();
			}
			if( str2.contains_lower() )
			{
				str2.to_upper();
			}

			// insensitive
			if( str1.icmp( str2 )						== 0	&&
				str2.icmp( str1.c_str() )				== 0	&&
				str1.icmpn( str2, str2.size()/div )		== 0	&&
				str2.icmpn( str1.c_str(), str1.size() )	== 0	&&

				// not equal
				str1.icmp( str3 )						!= 0	&&
				str3.icmp( str1.c_str() )				!= 0	&&
				str1.icmpn( str3, str3.size()/div )		!= 0	&&
				str3.icmpn( str1.c_str(), str1.size() )	!= 0 )
			{

			}
			else
			{
				//ERROR!
				return false;
			}

			// test is numeric function
			if( str1.numeric() == false &&
				str2.numeric() == false &&
				str3.numeric() == false &&
				str4.numeric() == true )
			{

			}
			else
			{
				//ERROR!
				return false;
			}

			str1.clear(false);
			str2.clear(false);
			str3.clear(false);
			--div;
		}

		return true;
	}

	bool stringTestStripping()
	{
		size_t const num = 23; // Must be less than 26!
		Str str1;// has repeated chars like so "aaaooommmwwwuuufffbbb"
		Str str2;// has str3 and str4 repeating in it like so "abcGHIabcGHIabcGHI"
		Str str3;// has random strings in it of length div like so "abc"
		Str str4;// has random strings in it of length div like so "GHI"
		Str str5;// has the same char repeated like so "aaaaa"
		Str str6;// has str3 repeated like so "abcabcabcabcabcabc"
		Set<size_t> previousRandomChars;
		previousRandomChars.reserve(32); //-V112

		size_t div = 16; // length of sub str to search for
		while( div != 0 )
		{
			//----------------------------------------------------------------
			// put characters in strings
			for( size_t j = div; j > 0; --j )
			{
				str3.push_back( (char)('a'+Random::Generator().Int(0,25)) );
				str4.push_back( (char)('a'+Random::Generator().Int(0,25)) );
			}

			for(size_t i = 0; i < num; ++i )// loopI
			{
				char charA = (char)('a'+Random::Generator().Int(0,25));

				while( previousRandomChars.insert( static_cast<size_t>(charA) ) == false )
				{
					charA = (char)('a'+Random::Generator().Int(0,25));
				}

				for( size_t j = div; j > 0; --j )
				{
					str1.push_back( charA ); // does change per loopI
					str5.push_back( str4.back() ); // doesnt change per loopI
				}
				
				str2.push_back( str3 );
				str2.push_back( str4 );
				str6.push_back( str3 );
			}

			//----------------------------------------------------------------
			// will cause a softlock if strip leading or strip trailing doesnt work!
			// test trip leading and strip trailing where they do not have a chance to strip all the chars in the string
			do
			{
				if( str1.size() >= div*2 )
				{
					size_t currentSize = str1.size();
					str1.strip_leading(str1.front()); // should remove div number of chars
					str1.strip_trailing(str1.back()); // should remove div number of chars
					if( str1.size() == currentSize - div*2 )
					{

					}
					else
					{
						// ERROR!
						return false;
					}
				}
				else
				{
					size_t currentSize = str1.size();
					str1.strip_leading(str1.front());
					if( str1.size() == 0 || str1.size() == currentSize - div )
					{

					}
					else
					{
						//ERROR
						return false;
					}
				}

				if( str2.size() >= (str3.size()+str4.size()) *2 )
				{
					size_t currentSize = str2.size();
					str2.strip_leading( str3 );
					str2.strip_leading( str4.c_str() );
					str2.strip_trailing( str4.c_str() );
					str2.strip_trailing( str3 );

					if( str2.size() == currentSize - (str3.size() * 2 + str4.size() * 2) )
					{

					}
					else
					{
						//ERROR!
						return false;
					}
				}
				else
				{
					size_t currentSize = str2.size();
					str2.strip_trailing( str4 );
					str2.strip_trailing( str3 );
					if( str2.size() == 0 || str2.size() == currentSize - (str3.size()+str4.size()) )
					{

					}
					else
					{
						//ERROR
						return false;
					}
				}
			}
			while( str1.empty() == false );
			// end of strip leading and strip trailing test of striping only parts of the string

			//----------------------------------------------------------------
			// Test stripping of strings and chars that will cause the string to become empty
			Str temp = str5;
			Str temp2 = str6;
			str5.strip_leading( temp[0] ); // char
			// can not do char version on str6
			if( str5.size() == 0 )
			{
				str5 = temp;
			}
			else
			{
				// ERROR
				return false;
			}

			size_t multiple = str5.size()%div == 0 ? 1 : str5.size()%div;
			str5.strip_leading( str5.sub_str(0,multiple) ); // str
			str6.strip_leading( str3 ); // str
			if( str5.size() == 0 )
			{
				str5 = temp;
				str6 = temp2;
			}
			else
			{
				// ERROR
				return false;
			}

			str5.strip_leading( str5.sub_str(0,multiple).c_str() ); // c str
			str6.strip_leading( str3.c_str() );
			if( str5.size() == 0 )
			{
				str5 = temp;
				str6 = temp2;
			}
			else
			{
				// ERROR
				return false;
			}

			//----------------------------------------------------------------
			// now the same for trailing
			str5.strip_trailing( temp[0] ); // char
			// can not do char version on str6
			if( str5.size() == 0 )
			{
				str5 = temp;
			}
			else
			{
				// ERROR
				return false;
			}

			str5.strip_trailing( str5.sub_str(0,multiple) ); // str
			str6.strip_trailing( str3 ); // str
			if( str5.size() == 0 )
			{
				str5 = temp;
				str6 = temp2;
			}
			else
			{
				// ERROR
				return false;
			}

			str5.strip_trailing( str5.sub_str(0,multiple).c_str() ); // c str
			str6.strip_trailing( str3.c_str() );
			if( str5.size() == 0 )
			{
				str5 = temp;
				str6 = temp2;
			}
			else
			{
				// ERROR
				return false;
			}

			str5.strip_quotes();
			str5.strip_trailing_whitespace();
			str6.strip_quotes();
			str6.strip_trailing_whitespace();

			for(size_t j = div; j > 0; --j)
			{
				str5.push_back(' ');	//-V525
				str5.push_back( '\t' );	//-V525
				str5.push_back(' ');	//-V525
				str5.push_back(' ');	//-V525
				str5.push_back(' ');	//-V525
			}

			size_t currentSize = str5.size();
			str5.strip_trailing_whitespace();
			if( str5.size() == currentSize - div*5 )
			{

			}
			else
			{
				// ERROR
				return false;
			}

			str5.insert('\"',0);
			str5.push_back( '\"' );
			currentSize = str5.size();
			str5.strip_quotes();
			if( str5.size() == currentSize -2 )
			{

			}
			else
			{
				// ERROR
				return false;
			}

			//----------------------------------------------------------------
			// setup for next loop
			str1.clear( false );
			str2.clear( false );
			//str3.clear( false ); // intensionally not cleared to make it more intense
			//str4.clear( false ); // intensionally not cleared to make it more intense
			str5.clear( false );
			str6.clear( false );
			previousRandomChars.clear( false );
			--div;
		}

		return true;
	}

	bool stringTestHash()
	{
		Str str1;
		Str str2;
		Str temp;

		size_t div = 16; // length of sub str
		while( div != 0 )
		{
			// add some chars
			for(size_t i = 0; i < div*2; ++i )
			{
				str1.push_back( (char)('a'+Random::Generator().Int(0,25)) );
				str2.push_back( (char)('0'+Random::Generator().Int(0,9)) );
			}

			// add some other chars
			for(size_t i = 0; i < 4; ++i ) //-V112
			{
				str1.push_back(str2[i]);
				str2.push_back(str1[i]);
			}

			// compare hashes
			if( str1.hash() != str2.hash() )
			{
			}
			else
			{
				// ERROR
				return false;
			}

			temp = str1;
			if( str1.hash() == temp.hash() )
			{
			}
			else
			{
				// ERROR
				return false;
			}

			temp = str2;
			if( str2.hash( div ) == temp.hash( div ) )
			{
			}
			else
			{
				// ERROR
				return false;
			}

			if( str1.hash( div-1) != temp.hash( div ) )
			{
			}
			else
			{
				// ERROR
				return false;
			}

			str1.clear(false);
			str2.clear(false);
			temp.clear(false);
			--div;
		}

		{
			Str str1;

			// get a size that is not nice
			size_t const p = Primes::PrimeAt( 15 );
			for( size_t i = 0; i < p; ++i )
			{
				str1.push_back( (char)('a'+Random::Generator().Int(0,25)) );
			}

			// copy the string (this is what will be compared)
			Str str2( str1 );

			size_t const nearest = (RoundUpToMultipleOf( p, 16 ));

			// add more entires up to the nearest
			for( size_t i = p; i < nearest; ++i )
			{
				str1.push_back( (char)('0'+Random::Generator().Int(0,9)) );
			}

			// reduce the size back to p
			str1.pop_back( nearest - p );

			// now compare
			if( str1.hash() == str2.hash() )
			{
			}
			else
			{
				// ERROR
				return false;
			}
		}

		return true;
	}

	bool stringTestFindAndReplace()
	{
		{ // middle
			Str str1("bobjimbobfred");
			Str str2("jim");//find
			Str str3("eve");//replace

			if( str1.replace( str2, str3 ) == false )
			{
				return false;
			}
		}
		{ // start + middle
			Str str1("bobujimbobujim");
			Str str2("jim");//find
			Str str3("jen");//replace

			if( str1.replace( str2, str3 ) == false )
			{
				return false;
			}
		}
		{ // start + middle + end
			Str str1("bobu");
			Str str2("bob");//find
			Str str3("jennny");//replace

			if( str1.replace( str2, str3 ) == false )
			{
				return false;
			}

			size_t hash = str1.hash();
			if( str1.replace( str3, str2 ) == false )
			{
				return false;
			}

			if( str1.replace( str2, str3 ) == false )
			{
				return false;
			}

			if( hash != str1.hash() )
			{
				return false;
			}
		}

		return true;
	}

	bool stringTestPrintf()
	{
		Str str1;
		Str str2;
		Str str3;

		str1.push_backf( "%f", 3.14159265358979323846f );
		str1.push_backf( " %d", 1024 );
		str1.push_backf( " %s", "wheels of confusion" );

		str2 = str1 + (6.0f);
		str3 = str1 + (2.0f*3.0f*4.0f*5.0f*6.0f);
		str1 = str3 + static_cast<int64>(999);
		str1 = str3 + static_cast<size_t>( -1 );
		str1 = str2 + true;

		str1.clear(false);

		str1 += 1.0f;
		str1 += static_cast<int64>(-111);
		str1 += static_cast<size_t>( -2 );

		// false become a object
		str1 += false; //-V601

		str2.format( "%s :%lli :%f", "output vales =" , static_cast<int64>( 666666 ), sqrt(2.0) );
		str3.format( "hello %s,", "world" );

		return true;
	}

	bool stringTestSplit()
	{
		// quick initial test, will do for example
#if 0
		Str a("(hello),(world),(karl),(here)");
		Str tok("),(");

		List<Str> strings;
		a.split( tok, strings );

		while( strings.empty() == false )
		{
			cout << strings.back().c_str() << endl;
			strings.pop_back();
		}
#endif

		Str str1;
		Str str2;
		char const strTokA = ',';
		Str const strTokB ("),(");

		size_t div = 16; // length of sub str
		while( div != 0 )
		{
			str2.push_back( '(' );
			for(size_t i = 0; i < div*2; ++i )
			{
				// put characters in string
				for(size_t j = 0; j < 4; ++j ) //-V112
				{
					str1.push_back( (char)('a'+Random::Generator().Int(0,25)) );
					str1.push_back( (char)('0'+Random::Generator().Int(0,9)) );
					str2.push_back( (char)('a'+Random::Generator().Int(0,25)) );
					str2.push_back( (char)('0'+Random::Generator().Int(0,9)) );
				}

				str1.push_back( strTokA );
				str2.push_back( strTokB );
			}
			str2.push_back( ')' );

			List<Str> elements;
			if( str1.split( strTokA, elements ) == true && elements.size() == div*2+1 )// +1 because we have intensionally created an empty element
			{

			}
			else
			{
				// ERROR
				return false;
			}

			elements.clear(false);
			if( str2.split( strTokB, elements ) == true && elements.size() == div*2+1 )// +1 because we have intensionally created an empty element
			{

			}
			else
			{
				// ERROR
				return false;
			}

			elements.clear(false);
			// str1 does not contain any "),(" tokens
			if( str1.split( strTokB.c_str(), elements ) == false && elements.size() == 0 )
			{

			}
			else
			{
				// ERROR
				return false;
			}

			// however str2 does contain ',' tokens
			if( str2.split( strTokA, elements ) == true && elements.size() == div*2+1 )
			{

			}
			else
			{
				// ERROR
				return false;
			}

			str1.clear(false);
			str2.clear(false);
			--div;
		}
		return true;
	}

	bool stringTestSpan()
	{
		Str str1;
		Str str2;
		Str const spanA("qwertyuiopasdfghjklzxcvbnm");

		size_t div = 16; // length of sub str
		while( div != 0 )
		{
			// push chars that match their sets
			for(size_t i = 0; i < div*2; ++i )
			{
				str1.push_back( (char)('a'+Random::Generator().Int(0,25)) );
				str2.push_back( (char)('0'+Random::Generator().Int(0,9)) );
			}

			// push chars that do not match their sets
			for(size_t i = 0; i < 4; ++i ) //-V112
			{
				str1.push_back(str2[i]);
				str2.push_back(str1[i]);
			}

			// count the span of the set
			if( str1.span( spanA ) == div*2 )
			{

			}
			else
			{
				// ERROR
				return false;
			}

			if( str2.span( "0912345678" ) == div*2 )
			{

			}
			else
			{
				// ERROR
				return false;
			}

			str1.clear(false);
			str2.clear(false);
			--div;
		}

		return true;
	}

	bool stringTestAGNER()
	{
		// some of AGNERs functions can read upto 15 bytes more then the string.
		// this will happen if the string length is not 16 byte aligned
		// however all string allocations are 16 byte aligned
		// so there should be no problems
		{
			Str str1;

			// get a size that is not nice
			size_t const p = Primes::PrimeAt( 15 );
			for( size_t i = 0; i < p; ++i )
			{
				str1.push_back( (char)('a'+Random::Generator().Int(0,25)) );
			}

			// copy the string (this is what will be compared)
			Str str2( str1 );

			size_t const nearest = (RoundUpToMultipleOf( p, 16 ));

			// add more entires up to the nearest
			for( size_t i = p; i < nearest; ++i )
			{
				str1.push_back( (char)('0'+Random::Generator().Int(0,9)) );
			}

			// reduce the size back to p
			str1.pop_back( nearest - p );

			// now compare
			if( strcmp( str1.c_str(), str2.c_str() ) == 0 )
			{
				// it seems that even though it will read past the first null in the string
				// the values after it do not go towards the outcome of the comparison
				// i.e. this compare will compare nulls with unitied memory at str1[p].
				// but the outcome is still correct
			}
			else
			{
				// ERROR
				return false;
			}
		}

		return true;
	}

	bool stringTestStreamOperators()
	{
		ifstream inputFile("AllEngWords.txt");

		if( inputFile.is_open() == true )
		{
			Str file;
			Str strTemp;

			size_t const max = 10;
			size_t count = 0;

			while( inputFile >> strTemp )// istream
			{
				file.push_back( strTemp );
				strTemp.clear( false );

				if( ++count > max )
					break;
			}

			cout << file << endl; // ostream

			inputFile.close();
		}

		return true;
	}

	void StringTest()
	{
		stringTest1();
		stringTest2();

		if( stringTestContains() == false )
		{
			cout << "FAILED! contains" << endl;
			return;
		}
		if( stringTestErase() == false )
		{
			cout << "FAILED! erase" << endl;
			return;
		}
		if( stringTestOperators() == false )
		{
			cout << "FAILED! operators" << endl;
			return;
		}

		if( stringTestcmpAndCaseChanges() == false )
		{
			cout << "FAILED! cmp or case changing" << endl;
			return;
		}

		if( stringTestStripping() == false )
		{
			cout << "FAILED! stripping test" << endl;
			return;
		}

		if( stringTestHash() == false )
		{
			cout << "FAILED! hash test" << endl;
			return;
		}

		if( stringTestFindAndReplace() == false )
		{
			cout << "FAILED! find and replace" << endl;
			return;
		}

		if( stringTestPrintf() == false )
		{
			cout << "FAILED! printf" << endl;
			return;
		}

		if( stringTestSplit() == false )
		{
			cout << "FAILED! split" << endl;
			return;
		}

		if( stringTestSpan() == false )
		{
			cout << "FAILED! span" << endl;
			return;
		}

		if( stringTestAGNER() == false )
		{
			cout << "FAILED! anger" << endl;
			return;
		}

		//stringTestStreamOperators();

		cout << "string tests succeeded!" << endl;
	}
}